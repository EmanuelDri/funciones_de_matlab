function [tpStatistics,osStatistics,initialStateStatistics,finalStateStatistics,peakVoltageStatistics]=windowedCampaignStatistics()

global tpMatrix;
global filterNames;
global osMatrix;

filePath=uigetdir();
tic
if filePath~=0
    oldFolder=cd(filePath);
    matFiles=dir('*.mat');
    [filesNumber,~]=size(matFiles);
    
    disp(filesNumber);
    
    headers={'Promedio','Mediana','M�nimo','M�ximo','Desviaci�n Est�ndar','Coeficiente de variaci�n %','Error estandar','Rango intercuartilico','Filtro','Valor p (test x2 de ajuste a la distribuci�n normal'};
    tpStatistics=cell(filesNumber,length(headers));
    tpStatistics(1,:)=headers;
    osStatistics=cell(filesNumber,length(headers));
    osStatistics(1,:)=headers;
    initialStateStatistics=cell(filesNumber,length(headers));
    initialStateStatistics(1,:)=headers;
    finalStateStatistics=cell(filesNumber,length(headers));
    finalStateStatistics(1,:)=headers;
    peakVoltageStatistics=cell(filesNumber,length(headers));
    peakVoltageStatistics(1,:)=headers;
    tpMatrix=[];
    intermediateTpMatrix=[];
    osMatrix=[];
    
    filterNames=cell(1,filesNumber);
    
    for fileIndex=1:filesNumber
        disp(fileIndex);
        %% Load data
        fileName=matFiles(fileIndex).name;
        waveFormsArray=loadDataAndConfigureStorage(filePath,fileName);
        
        %% Generate statistics tables
        statisticsTable=getWindowedCampaignStatistics(waveFormsArray);
        osValues=cell2mat(statisticsTable(2:end,4));
        tpValues=cell2mat(statisticsTable(2:end,end));
        iniSValues=cell2mat(statisticsTable(2:end,1));
        finalSValues=cell2mat(statisticsTable(2:end,2));
        peakVValues=cell2mat(statisticsTable(2:end,3));
        tpIntermediateValues=(cell2mat(statisticsTable(2:end,5))+ cell2mat(statisticsTable(2:end,6)))/2;
        
        osMean=mean(osValues); osMax=max(osValues); osMin=min(osValues);...
            osIQE=iqr(osValues); osSD=std(osValues);osVC=osSD/osMean*100;... 
            osMedian=median(osValues);osSE=osSD/sqrt(length(osValues));...
            [~,osP]=chi2gof(osValues);
        
        tpMean=mean(tpValues); tpMax=max(tpValues); tpMin=min(tpValues);...
            tpIQE=iqr(tpValues); tpSD=std(tpValues);tpVC=tpSD/tpMean*100;... 
            tpMedian=median(tpValues); tpSE=tpSD/sqrt(length(tpValues));...
            [~,tpP]=chi2gof(tpValues);
        
        iniSMean=mean(iniSValues); iniSMax=max(iniSValues); iniSMin=min(iniSValues);...
            iniSIQE=iqr(iniSValues); iniSSD=std(iniSValues);iniSVC=iniSSD/iniSMean*100;... 
            iniSMedian=median(iniSValues);iniSSE=iniSSD/sqrt(length(iniSValues));...
            [~,iniSP]=chi2gof(iniSValues);
        
        finalSMean=mean(finalSValues); finalSMax=max(finalSValues); finalSMin=min(finalSValues);...
            finalSIQE=iqr(finalSValues); finalSSD=std(finalSValues);finalSVC=finalSSD/finalSMean*100;... 
            finalSMedian=median(finalSValues);finalSSE=finalSSD/sqrt(length(finalSValues));...
            [~,finalSP]=chi2gof(finalSValues);
        
        peakVMean=mean(peakVValues); peakVMax=max(peakVValues); peakVMin=min(peakVValues);...
            peakVIQE=iqr(peakVValues); peakVSD=std(peakVValues);peakVVC=peakVSD/peakVMean*100;... 
            peakVMedian=median(peakVValues);peakVSE=peakVSD/sqrt(length(peakVValues));...
            [~,peakVP]=chi2gof(peakVValues);
            
        currentTpStatistics={tpMean*1e6,tpMedian*1e6, tpMin*1e6, tpMax*1e6, tpSD*1e6, tpVC,...
            tpSE*1e6,tpIQE*1e6,fileName,tpP};
        currentOSStatistics={osMean,osMedian, osMin, osMax, osSD, osVC,...
            osSE,osIQE,fileName,osP};
        currentIniSStatistics={iniSMean,iniSMedian, iniSMin, iniSMax, iniSSD, iniSVC,...
            iniSSE,iniSIQE,fileName,iniSP};
        currentFinalSStatistics={finalSMean,finalSMedian, finalSMin, finalSMax, finalSSD, finalSVC,...
            finalSSE,finalSIQE,fileName,finalSP};
        currentPeakVStatistics={peakVMean,peakVMedian, peakVMin, peakVMax, peakVSD, peakVVC,...
            peakVSE,peakVIQE,fileName,peakVP};
                
        tpStatistics(fileIndex+1,:)=currentTpStatistics;
        osStatistics(fileIndex+1,:)=currentOSStatistics;
        initialStateStatistics(fileIndex+1,:)=currentIniSStatistics;
        finalStateStatistics(fileIndex+1,:)=currentFinalSStatistics;
        peakVoltageStatistics(fileIndex+1,:)=currentPeakVStatistics;
        
        tpMatrix(end+1,:)=tpValues(1:99)';
        osMatrix(end+1,:)=osValues(1:99)';
        intermediateTpMatrix(end+1,:)=tpIntermediateValues(1:99)';
        filterNames{fileIndex}=fileName;
                
    end
    
    mkdir(getCurrentFolderName);
    save(fullfile(pwd,getCurrentFolderName,getCurrentFolderName),...
        'tpStatistics','osStatistics','initialStateStatistics','finalStateStatistics','peakVoltageStatistics');
    cd(oldFolder);
    figure; boxplot(tpMatrix',filterNames,'Whisker',100);title('tp');
    figure; boxplot(osMatrix',filterNames,'Whisker',100);title('os');
%     figure; boxplot(intermediateTpMatrix',filterNames,'Whisker',100);title('tp int');
%     
%     figure; boxplot(tpMatrix',filterNames,'Whisker',100);title('tp'); hold on;
%     boxplot(intermediateTpMatrix',filterNames,'Whisker',100,'Colors','g');title('tp int');
    
    

end
toc

    function waveFormsArray=loadDataAndConfigureStorage(filePath,fileName)
        
        %% Variables definition
        waveFormsArray=[];
        estimationWaveForm=[];
        %% Get campaign data from file
        
        load(fullfile(filePath,fileName));
        newFolderName=fileName(1:end-4);
        mkdir(fullfile(filePath,newFolderName));
    end

    function currentFolderName=getCurrentFolderName()
        [~, currentFolderName, ~] = fileparts(pwd);
    end

end