orcadWaveForm=0;
filePath=uigetdir();
tic
if filePath~=0
    mkdir('Results');
    oldFolder=cd(filePath);
    matFiles=dir('*.mat');
    [filesNumber,~]=size(matFiles);
    
    disp(filesNumber);
    
    headers={'Tp';'OS%';'K';'Tp+OS';'OS+K';'Tp+K';'Tp+OS+K';'fileName'};
    results=cell(1,length(headers));
    results(1,:)=headers;
    tolTp=     3.784; refTp=277.5;
    tolOS=     4.176; refOS=15.538769;
    tolK=     2.709; refGain=0.988704834;
%     results=[];save('Results\Results.mat','results');

    for fileIndex=1:filesNumber
        particularFileResults=cell(1,length(headers));
        particularFileResults(1,:)=headers;
        figure(1);
        close(1);
        disp(fileIndex);
        %% Load data
        %         clear;
        fileName=matFiles(fileIndex).name;
        s=load(fullfile(filePath,fileName));
        
        mkdir(fileName(1:end-3));
        variables=fieldnames(s);
        variablesNamesLength=max(cell2mat(cellfun(@length,variables,'UniformOutput',false)));
        waveFormsArray=s.waveFormsArray;
        
        %         Time=s.Time;
        tpFails=0;osFails=0;gainFails=0;tpOsFails=0;osKFails=0;osTpKFails=0;tpKFails=0;
        for index=1:100
            timeValues=waveFormsArray{index,1};voltageValues=waveFormsArray{index,2};
%             waveformName=variables{index+1};
            %         if (length(waveformName)==variablesNamesLength)
            %             waveformIndex=str2num(waveformName(end-1:end));
            %         else
            %             waveformIndex=str2num(waveformName(end));
            %         end
            disp(index);
            %         eval(['voltages=s.' waveformName ';']);%voltages(1)=[];
            %         voltageValues=voltages;
            voltageValues(voltageValues>5)=5;
            voltageValues(voltageValues<0)=0;
            %             figure(1);hold on;plot(Time(1001:end),voltageValues(1001:end));
            gain=[];
            wn=[];
            qp=[];
            %%
            [xData, yData] = prepareCurveData( timeValues, voltageValues );
            % Set up fittype and options.
            ft = fittype( 'smoothingspline' );
            opts = fitoptions( 'Method', 'SmoothingSpline' );
            opts.Normalize = 'on';
            opts.SmoothingParam =0.9999536367214412; %este era, por cierto
            [fitModel, ~] = fit( xData, yData, ft, opts );
            fittedVoltages=feval(fitModel,timeValues);
%             figure(1);hold on;plot(timeValues,fittedVoltages);
            voltageValues=fittedVoltages;
            %%
            %             if orcadWaveForm
            %                 endStateVoltage=voltageValues(end);
            %                 initialStateVoltage=voltageValues(1);
            %                 Time=Time-3e-3;
            %
            %             else
            %                 filterCoefficents=ones(1,1000)/1000;
            %                 voltageValues=filter(filterCoefficents,1,voltageValues);
            %                 voltageValues(1:1000)=[]; Time(1:1000)=[];
            [initialStateVoltage,endStateVoltage]=getBistateLevels(voltageValues,100);%,1000,'Histogram mean');
            %             end
            %             figure(1);hold on;plot(Time,voltageValues);
            overshootVoltage=max(voltageValues);
            voltageRange=overshootVoltage-initialStateVoltage;
            overshootTimeArray=timeValues(voltageValues==max(voltageValues));
            overshootPercent=(overshootVoltage-endStateVoltage)/(endStateVoltage-initialStateVoltage)*100;
            if isnan(overshootPercent)||overshootPercent==0
                meanPeakTime=inf;
            else
                meanPeakTime=mean(overshootTimeArray);%-timeDisplacement;
            end
            if (~isempty(find(voltageValues>=5-.01, 1))||~isempty(find(voltageValues<=0.1, 1)))
                meanPeakTime='satura';
                overshootPercent='satura';
                gain='satura';
                tpFails=tpFails+1;osFails=osFails+1;gainFails=gainFails+1;tpOsFails=tpOsFails+1;osKFails=osKFails+1;osTpKFails=osTpKFails+1;tpKFails=tpKFails+1;
            else
%                 if ~isempty(findpeaks(voltageValues))
                    stepVoltage=endStateVoltage-initialStateVoltage;
                    gain=stepVoltage/1.3
                    gainFlag=0;osFlag=0;tpFlag=0;
                    if (abs(gain-refGain)/refGain*100>=tolK)
                        gain='falla';gainFlag=1;
                        gainFails=gainFails+1; 
                    end
                    if (abs(overshootPercent-refOS)/refOS*100>=tolOS)
                        overshootPercent='falla';osFlag=1;
                        osFails=osFails+1;
                    end
%                     tau=1/sqrt((pi/log(overshootPercent/100))^2+1);
%                     qp=1/2/tau;
%                     wn=pi/meanPeakTime/sqrt(1-tau^2);
                    meanPeakTime=meanPeakTime*1e6;
                    if (abs(meanPeakTime-refTp)/refTp*100>=tolTp)
                        meanPeakTime='falla';tpFlag=1;
                        tpFails=tpFails+1;
                    end
                    tpOsFails=(osFlag|tpFlag)+tpOsFails;
                    osKFails=(osFlag|gainFlag)+osKFails;
                    osTpKFails=(osFlag|tpFlag|gainFlag)+osTpKFails;
                    tpKFails=tpKFails+(tpFlag|gainFlag);
%                 else
%                     gain=(voltageValues(end)-voltageValues(1))/1.3;
%                     qp='No OS';
%                     wn='No OS';
%                     overshootPercent=0;
%                     meanPeakTime=inf;
%                 end
            end
%             particularFileResults(end+1,:)={meanPeakTime;overshootPercent;gain;wn;qp;[num2str(10*(waveformIndex)) '%'];fileName}';
%             save(fullfile(fileName(1:end-3),[fileName(1:end-3) 'Results.mat']),'results');
        end
        results(end+1,:)={tpFails;osFails;gainFails;tpOsFails;osKFails;tpKFails;osTpKFails;fileName}';
        save('Results\Results.mat','results');
    end
end