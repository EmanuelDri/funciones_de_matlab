classdef measurementCampaignStatistics
    properties
        statisticsArray % cellarray conteniendo las estad�sticas 
        %correspondientes a la campa�a evaluada
        statisticsMatrix
    end
    methods (Access= private)
        
    end
    methods (Access=public)
        function getPeakTimeHistogram(thisClassObj,beansNumber)
            hist(thisClassObj.statisticsArray,beansNumber);
        end
    end
end