function p=seriesResonancePower(e,r,l,c,w)
p=e^2*r/(r^2+(((w.^2)*l*c-1)/w/c).^2);
end