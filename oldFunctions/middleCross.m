function [middleCrossTime,middleCrossVoltage]=middleCross(x,y)
x=reshape(x,[],1);
y=reshape(y,[],1);
transitionMetricsObject = dsp.TransitionMetrics('TimeInputPort',true,'StateLevelsSource','Auto');
[transition] = step(transitionMetricsObject,y,x);
middleCrossTime=transition.MiddleCross(1);
[~,middleCrossTimeIndex]=min(abs(x-middleCrossTime));
middleCrossVoltage=y(middleCrossTimeIndex);
end