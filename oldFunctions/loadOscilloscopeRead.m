function [x1,y1,x2,y2]=loadOscilloscopeRead(fileName)
load(fileName);
x1=xch1;
x2=xch2;
y1=ych1;
y2=ych2;
end