function suma=sumaFasorialEnGrados(rho,theta)
complexNumbersArray=zeros(1,length(rho));
for indice=1:length(rho)
    rhoActual=rho(indice);
    thetaActual=theta(indice);
    [parteReal, parteImaginaria]=pol2cart(thetaActual,rhoActual);
    complexNumbersArray(indice)=parteReal+1i*parteImaginaria;
end
suma=sum(complexNumbersArray);
