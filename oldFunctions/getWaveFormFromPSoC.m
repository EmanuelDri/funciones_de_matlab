function statisticsArray=getWaveFormFromPSoC(fileName,executionsNumber,varargin)
tic
if ~isempty(varargin)&&islogical(varargin{1})&&~(varargin{1})
    getOtherValues=false;
else
    getOtherValues=true;
end

delete(instrfind)

adcResolution=1024;
RefLo=1.2;
RefHi=3.8;
agnd=(RefHi-RefLo)/2+RefLo;

adcStepDelta=(RefHi-RefLo)/adcResolution;

serialPort=serialSetup();
% executionsNumber=2;
if getOtherValues
    getSerialLineWaveForm(serialPort);
    for tramExecution=1:executionsNumber
        [endTime, samplesArray,samplesNumber]=getSerialLineWaveForm(serialPort);
        waveFormCollection{tramExecution,1}=endTime;
        waveFormCollection{tramExecution,2}=samplesArray;
        waveFormCollection{tramExecution,3}=samplesNumber;
    end
else
    getSerialLineWaveForm2(serialPort);
    for tramExecution=1:executionsNumber
        %disp(tramExecution);
        samplesArray=getSerialLineWaveForm2(serialPort);
        waveFormCollection(tramExecution,:)=samplesArray;
    end
end
save(fileName,'waveFormCollection');

fclose(serialPort);

% [fileName,filePath]=uigetfile();

    function serialPort=serialSetup()
        %         serialPort = serial('/dev/ttyUSB0');
        serialPort = serial('COM8');
        set(serialPort,'DataBits',8);
        set(serialPort,'StopBits',1);
        %         set(serialPort,'BaudRate',115200);
        set(serialPort,'BaudRate',115384);
        set(serialPort,'Parity','none');
        set(serialPort,'Terminator',13);
        set(serialPort,'ReadAsyncMode','continuous');
        set(serialPort,'InputBufferSize',1024*1024*100);
        fopen(serialPort);
%         disp(serialPort);
    end

    function [endTime, samplesArray, samplesNumber]=getSerialLineWaveForm(serialPort)
        samplesArray=fscanf(serialPort,'%f');
        endTime=fscanf(serialPort,'%f');
        samplesNumber=fscanf(serialPort,'%f');
    end

    function samplesArray=getSerialLineWaveForm2(serialPort)
        samplesArray=fscanf(serialPort,'%f');
    end
toc
end

