function [oscilloscopeHandler,oscilloscope]=connectTDS1001B()
    % Find a VISA-USB object.
%     oscilloscope = instrfind('Type', 'visa-usb', 'RsrcName', 'USB0::0x0699::0x0362::C010188::0::INSTR', 'Tag', '');
    oscilloscope= instrfind('Type', 'visa-usb', 'RsrcName', 'USB0::0x0699::0x0362::C020141::0::INSTR', 'Tag', '');instrfind('Type', 'visa-usb', 'RsrcName', 'USB0::0x0699::0x0362::C010188::0::INSTR', 'Tag', '');

    % Create the VISA-USB object if it does not exist
    % otherwise use the object that was found.
    if isempty(oscilloscope)
%         oscilloscope = visa('TEK', 'USB0::0x0699::0x0362::C010188::0::INSTR');
            oscilloscope = visa('TEK', 'USB0::0x0699::0x0362::C010188::0::INSTR');
    else
        fclose(oscilloscope);
        oscilloscope = oscilloscope(1);
    end
    
    % Connect to instrument object, obj1.
    fopen(oscilloscope);
    
    % Create a device object. 
    oscilloscopeHandler = icdevice('tektronix_tds1002.mdd', oscilloscope);

    % Connect device object to hardware.
%     connect(oscilloscopeHandler);
end
