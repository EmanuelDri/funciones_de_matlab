function totalResistor=parallelResistor(varargin)
resistorsArray=cell2sym(varargin);
totalResistor=1/sum(1./resistorsArray);
end