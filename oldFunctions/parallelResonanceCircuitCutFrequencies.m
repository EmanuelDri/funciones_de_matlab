function [w1,w2]=parallelResonanceCircuitCutFrequencies(R,L,C)
commonMember2=sqrt(1/4/R^2/C^2+1/L/C);
commonMember1=1/2/R/C;
w1=commonMember1-commonMember2;
w2=commonMember1+commonMember2;
end