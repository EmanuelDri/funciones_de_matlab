function wp=parallelResonanceFrequency(R,L,C)
wp=1/sqrt(L*C)*sqrt(1-R^2*C/L);
end