% filesNumber=filesNumber+1;
clear statisticsArray;
parentFolder=uigetdir('C:\TRAM\Actividad 4\Mediciones MatLab\');
folders=listFolders(parentFolder);
statisticsArray{1,1}='Tp cruce 1/2';
statisticsArray{1,2}='Tp trigger';
statisticsArray{1,3}='%OS';
statisticsArray{1,4}='Medici�n';%fullfile(filePath,saveGame(1).name);
statisticsArray{1,5}='OS V';
statisticsArray{1,6}='Ruta';
for index=1:length(folders)
    filePath=fullfile(parentFolder, folders(index));
    filePath=filePath{1};
    saveGame=dir(fullfile(filePath,'*.mat'));
    [xch1,ych1,xch2,ych2]=loadOscilloscopeRead(fullfile(filePath,saveGame(1).name));
    [peakTime,overshootPercent,overshootVoltage]=determineOvershootAndPeakTime(xch1,ych1);
    timeZero=middleCross(xch2,ych2);
    
    statisticsArray{index+1,1}=(peakTime-timeZero)*1e6;
    statisticsArray{index+1,2}=peakTime*1e6;
    statisticsArray{index+1,3}=overshootPercent*100;
    statisticsArray{index+1,4}=saveGame(1).name;%fullfile(filePath,saveGame(1).name);
    statisticsArray{index+1,5}=overshootVoltage;
    statisticsArray{index+1,6}=fullfile(filePath,saveGame(1).name);
end 
% [fileName,filePath]=uigetfile();
