function degrees=angled(complexNumber)
degrees=180/pi*angle(complexNumber);
end