function [rs,xs]=parallelToSeries(rp,xp)
commonFactor=rp*xp/norm([rp xp])^2;
rs=commonFactor*xp;
xs=commonFactor*rp;
end