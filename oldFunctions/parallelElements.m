function parallelTotal=parallelElements(elements)
parallelTotal=1/sum(1./elements);
end
