function [w1,w2]=seriesResonantCircuitCutFrequencies(R,L,C)
commonMember2=sqrt(R^2/4/L^2+1/L/C);
commonMember1=R/2/L;
w1=commonMember2-commonMember1;
w2=commonMember2+commonMember1;
end