function [tpStatistics,osStatistics,kStatistics,wnStatistics,qpStatistics,fcStatistics,mrStatistics]=campaignStatisticsPlots()

%% Constants definition
tpIndex=1;
osIndex=3;

% filePath=uigetdir();
filePath=pwd;

if filePath~=0
    oldFolder=cd(filePath);
    matFiles=dir('*.mat');
    [filesNumber,~]=size(matFiles);
    
    headers={'Promedio';'Mediana';'M�nimo';'M�ximo';'Dispersi�n';'Filtro'};
    tpStatistics=cell(filesNumber,length(headers));
    osStatistics=cell(filesNumber,length(headers));
    kStatistics=cell(filesNumber,length(headers));
    wnStatistics=cell(filesNumber,length(headers));
    qpStatistics=cell(filesNumber,length(headers));
    fcStatistics=cell(filesNumber,length(headers));
    mrStatistics=cell(filesNumber,length(headers));
    tpStatistics(1,:)=headers; osStatistics(1,:)=headers; kStatistics(1,:)=headers;
    wnStatistics(1,:)=headers; qpStatistics(1,:)=headers; fcStatistics(1,:)=headers; mrStatistics(1,:)=headers;
    
    global tpMatrix;
    global osMatrix;
    global kMatrix;
    global fileNames;
    tpMatrix=[];
    osMatrix=[];
    kMatrix=[];
    fileNames=cell(1,filesNumber);
    
    for fileIndex=1:filesNumber
        
        %% Load dataa
        fileName=matFiles(fileIndex).name;
        [peakTimeArray,osArray,kArray,wnArray,qpArray,fcArray,mrArray]=loadDataAndConfigureStorage(filePath,fileName);
        fileNames{fileIndex}=fileName;
        osMatrix(:,end+1)=osArray;
        tpMatrix(:,end+1)=peakTimeArray;
        kMatrix(:,end+1)=kArray;
        
        [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(osArray);
        osStatistics(fileIndex+1,:)={meanValue;medianValue;minValue;maxValue;dispersionValue;fileName};
        [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(peakTimeArray);
        tpStatistics(fileIndex+1,:)={meanValue;medianValue;minValue;maxValue;dispersionValue;fileName};
        [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(kArray);
        kStatistics(fileIndex+1,:)={meanValue;medianValue;minValue;maxValue;dispersionValue;fileName};
        [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(wnArray);
        wnStatistics(fileIndex+1,:)={meanValue;medianValue;minValue;maxValue;dispersionValue;fileName};
        [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(qpArray);
        qpStatistics(fileIndex+1,:)={meanValue;medianValue;minValue;maxValue;dispersionValue;fileName};
        [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(fcArray);
        fcStatistics(fileIndex+1,:)={meanValue;medianValue;minValue;maxValue;dispersionValue;fileName};
        [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(mrArray);
        mrStatistics(fileIndex+1,:)={meanValue;medianValue;minValue;maxValue;dispersionValue;fileName};
    end
     mkdir(fullfile(filePath,'resultados'));
     save('resultados\resultados.mat','tpStatistics','osStatistics','kStatistics','wnStatistics','qpStatistics','fcStatistics','mrStatistics');
end

    function [peakTimeArray,osArray,kArray,wnArray,qpArray,fcArray,mrArray]=loadDataAndConfigureStorage(filePath,fileName)
        
        %% Variables definition
        statisticsArray=[];
%         statisticsArrray=[];
        waveFormCollection=[];
  
        %% Get campaign data from file
        
        load(fullfile(filePath,fileName));
        newFolderName=fileName(1:end-4);
%         mkdir(fullfile(filePath,newFolderName));
        
        statisticsMatrix=cell2mat(statisticsArray(2:end,:));
        
        peakTimeArray=statisticsMatrix(:,tpIndex);
        osArray=statisticsMatrix(:,osIndex);
        if(statisticsMatrix(1, 7)<3)
            stepAmplitude=1.3;
        else
            stepAmplitude=2.6;
        end
        kArray=(statisticsMatrix(:,7)-statisticsMatrix(:,6))/stepAmplitude;
        for index=1:length(kArray)
            os=osArray(index);tp=peakTimeArray(index);
            tau=1/sqrt((pi/log(os/100))^2+1);
            qpArray(index)=1/2/tau;
            wn=pi/tp*1e6/sqrt(1-tau^2);wnArray(index)=wn;
            respuesta=tf(wn^2,[1 2*wn*tau wn^2]);
            fc= getGainCrossover(respuesta,db2mag(-3))/2/pi;
%             wr=wn*sqrt(1-2*tau^2);
%             mrArray(index)=20*log10(norm(freqresp(respuesta,wr)));
            [mr,~]=getPeakGain(respuesta,1e-9);mr=20*log10(mr); %mr=((mr-kArray(index))/kArray(index)+1);
            fcArray(index)= fc;
            wr=wn*sqrt(1-2*tau^2);
            mrArray(index)=mr;
        end
        %         pulseWidthArray=statisticsMatrix(:,pulseWidthIndex);
        
    end

    function currentFolderName=getCurrentFolderName()
        [~, currentFolderName, ~] = fileparts(pwd);
    end

    function [meanValue,medianValue,minValue,maxValue,dispersionValue]=calculateStatistics(inputVector)
        meanValue=mean(inputVector);
        medianValue=median(inputVector);
        minValue=min(inputVector);
        maxValue=max(inputVector);
        dispersionValue=(maxValue-minValue)/medianValue*100;
    end

end