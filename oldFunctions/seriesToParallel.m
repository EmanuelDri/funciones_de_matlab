function [rp,xp]=seriesToParallel(rs,xs)
commonFactor=norm([rs xs])^2;
rp=commonFactor/rs;
xp=commonFactor/xs;
end