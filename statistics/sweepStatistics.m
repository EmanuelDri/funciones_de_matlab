tpMeanArray=[];
tpSDArray=[];
tpMaxArray=[];
tpMinArray=[];
tpMedianArray=[];
tpErrorMeanArray=[];
tpErrorSDArray=[];
tpErrorMaxArray=[];
tpErrorMinArray=[];
tpErrorMedianArray=[];
osMeanArray=[];
osSDArray=[];
osMaxArray=[];
osMinArray=[];
osMedianArray=[];
osErrorMeanArray=[];
osErrorSDArray=[];
osErrorMaxArray=[];
osErrorMinArray=[];
osErrorMedianArray=[];

dataMatrix=cell2mat(statisticsArray(2:end,:));
tpCollumn=dataMatrix(:,1);
tpECollumn=dataMatrix(:,2);
osCollumn=dataMatrix(:,3);
osECollumn=dataMatrix(:,4);
pwwCollumn=dataMatrix(:,10);

pwwList=unique(pwwCollumn);

startValue=1;
for pww=pwwList'
    tpMeanArray(end+1)=mean(abs(tpCollumn(pww==pwwCollumn)));
    tpSDArray(end+1)=statisticalDispersion(abs(tpCollumn(pww==pwwCollumn)));
    tpMaxArray(end+1)=max(abs(tpCollumn(pww==pwwCollumn)));
    tpMinArray(end+1)=min(abs(tpCollumn(pww==pwwCollumn)));
    tpMedianArray(end+1)=median(abs(tpCollumn(pww==pwwCollumn)));
    tpErrorMeanArray(end+1)=mean(abs(tpECollumn(pww==pwwCollumn)));
    tpErrorSDArray(end+1)=statisticalDispersion(abs(tpECollumn(pww==pwwCollumn)));
    tpErrorMaxArray(end+1)=max(abs(tpECollumn(pww==pwwCollumn)));
    tpErrorMinArray(end+1)=min(abs(tpECollumn(pww==pwwCollumn)));
    tpErrorMedianArray(end+1)=median(abs(tpECollumn(pww==pwwCollumn)));
    osMeanArray(end+1)=mean(abs(osCollumn(pww==pwwCollumn)));
    osSDArray(end+1)=statisticalDispersion(abs(osCollumn(pww==pwwCollumn)));
    osMaxArray(end+1)=max(abs(osCollumn(pww==pwwCollumn)));
    osMinArray(end+1)=min(abs(osCollumn(pww==pwwCollumn)));
    osMedianArray(end+1)=median(abs(osCollumn(pww==pwwCollumn)));
    osErrorMeanArray(end+1)=mean(abs(osECollumn(pww==pwwCollumn)));
    osErrorSDArray(end+1)=statisticalDispersion(abs(osECollumn(pww==pwwCollumn)));
    osErrorMaxArray(end+1)=max(abs(osECollumn(pww==pwwCollumn)));
    osErrorMinArray(end+1)=min(abs(osECollumn(pww==pwwCollumn)));
    osErrorMedianArray(end+1)=median(abs(osECollumn(pww==pwwCollumn)));
end

figure; plot(pwwList',tpMeanArray,pwwList',tpMedianArray,pwwList',tpMaxArray,pwwList',tpMinArray);
legend('Promedio','Mediana','M�ximo','M�nimo');
title('Tiempo de pico medido vs palabra de ancho de pulso');
xlabel('Palabra de ancho de pulso');
ylabel('Tiempo de pico');
set(gcf,'units','points','position',[10,10,13660,7680]);
print(gcf,'-djpeg','explHIzqArrFuente1.jpeg');

figure; plot(pwwList',tpSDArray);
title('Desviaci�n del tiempo de pico vs palabra de ancho de pulso');
xlabel('Palabra de ancho de pulso');
ylabel('Tiempo de pico');
set(gcf,'units','points','position',[10,10,13660,7680]);
print(gcf,'-djpeg','explHIzqArrFuente2.jpeg');

figure; plot(pwwList',tpErrorMeanArray,pwwList',tpErrorMedianArray,pwwList',tpErrorMaxArray,pwwList',tpErrorMinArray);
legend('Promedio','Mediana','M�ximo','M�nimo');
title('Error te�rico relativo porcentual del tiempo de pico vs palabra de ancho de pulso');
xlabel('Palabra de ancho de pulso');
ylabel('Error del tiempo de pico');
set(gcf,'units','points','position',[10,10,13660,7680]);
print(gcf,'-djpeg','explHIzqArrFuente3.jpeg');

% figure; plot(pwwList',tpErrorSDArray);
% title('Desviaci�n est�ndar del error del tiempo de pico vs palabra de ancho de pulso');
% xlabel('Palabra de ancho de pulso');
% ylabel('Error del tiempo de pico');
% set(gcf,'units','points','position',[10,10,13660,7680]);
% print(gcf,'-djpeg','explHIzqArrFuente4.jpeg');

figure; plot(pwwList',osMeanArray,pwwList',osMedianArray,pwwList',osMaxArray,pwwList',osMinArray);
legend('Promedio','Mediana','M�ximo','M�nimo');
title('Porcentaje de sobreimpulso vs palabra de ancho de pulso');
xlabel('Palabra de ancho de pulso');
ylabel('%OS');
set(gcf,'units','points','position',[10,10,13660,7680]);
print(gcf,'-djpeg','explHIzqArrFuente5.jpeg');

figure; plot(pwwList',osSDArray);
title('Desviaci�n del sobreimpulso vs palabra de ancho de pulso');
xlabel('Palabra de ancho de pulso');
ylabel('%OS');
set(gcf,'units','points','position',[10,10,13660,7680]);
print(gcf,'-djpeg','explHIzqArrFuente6.jpeg');

figure; plot(pwwList',osErrorMeanArray,pwwList',osErrorMedianArray,pwwList',osErrorMaxArray,pwwList',osErrorMinArray);
legend('Promedio','Mediana','M�ximo','M�nimo');
title('Porcentaje de error te�rico del sobreimpulso vs palabra de ancho de pulso');
xlabel('Palabra de ancho de pulso');
ylabel('Error porcentual del sobreimpulso');
set(gcf,'units','points','position',[10,10,13660,7680]);
print(gcf,'-djpeg','explHIzqArrFuente7.jpeg');

% figure; plot(pwwList',osErrorSDArray);
% title('Desviaci�n del error te�rico del sobreimpulso vs palabra de ancho de pulso');
% xlabel('Palabra de ancho de pulso');
% ylabel('%OS');
% set(gcf,'units','points','position',[10,10,13660,7680]);
% print(gcf,'-djpeg','explHIzqArrFuente8.jpeg');

