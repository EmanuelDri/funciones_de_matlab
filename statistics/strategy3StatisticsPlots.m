function strategy3StatisticsPlots()

%% Constants definition
tpIndex=1;
tpErrorIndex=2;
osIndex=3;
osErrorIndex=4;
vOffsetIndex=5;
pulseWidthIndex=9;
global tpMatrix;
global filterNames;
global osMatrix;
global gainMatrix
global wnMatrix;
global qpMatrix;
global f0Matrix;
global mrMatrix;

tpMatrix=[];
osMatrix=[];
gainMatrix=[];
wnMatrix=[];
qpMatrix=[];
f0Matrix=[];
mrMatrix=[];
filePath=uigetdir();

if filePath~=0
    oldFolder=cd(filePath);
    matFiles=dir('*.mat');
    [filesNumber,~]=size(matFiles);
    campaignsMean=cell(filesNumber+1,8);
    campaignsSD=cell(filesNumber+1,8);
    campaignsMin=cell(filesNumber+1,8);
    campaignsMax=cell(filesNumber+1,8);
    campaignsMedian=cell(filesNumber+1,8);
    campaignsMode=cell(filesNumber+1,8);
    
    filterNames=cell(1,filesNumber);
    
    for fileIndex=1:filesNumber
        
        %% Load data
        fileName=matFiles(fileIndex).name;
        filterNames{fileIndex}=fileName;
        [peakTimeArray,wnArray,osArray,qpArray,gainArray,f0Array,mrArray]=loadDataAndConfigureStorage(filePath,fileName);
        
        %% Generate statistics tables
        [currentCampaignMean,currentCampaignSD,currentCampaignMin,currentCampaignMax,currentCampaignMedian,currentCampaignMode]= getCampaignStatistics(peakTimeArray,wnArray,osArray,qpArray,gainArray,f0Array,mrArray,fileName);
        campaignsMean(fileIndex+1,1:end)=currentCampaignMean;
        campaignsSD(fileIndex+1,1:end)=currentCampaignSD;
        campaignsMin(fileIndex+1,1:end)=currentCampaignMin;
        campaignsMax(fileIndex+1,1:end)=currentCampaignMax;
        campaignsMedian(fileIndex+1,1:end)=currentCampaignMedian;
        campaignsMode(fileIndex+1,1:end)=currentCampaignMode;
        
        %% Plot histograms
%                 plotAndSaveHistograms(peakTimeArray,wnArray,osArray,osErrorArray,gainArray,filePath,fileName);
    end
    campaignsMean(1,1:end)=campaignStatisticsHeaders;
    campaignsSD(1,1:end)=campaignStatisticsHeaders;
    campaignsMin(1,1:end)=campaignStatisticsHeaders;
    campaignsMax(1,1:end)=campaignStatisticsHeaders;
    campaignsMedian(1,1:end)=campaignStatisticsHeaders;
    campaignsMode(1,1:end)=campaignStatisticsHeaders;
    mkdir(getCurrentFolderName);
    save(fullfile(pwd,getCurrentFolderName,getCurrentFolderName),'campaignsMean','campaignsSD','campaignsMin','campaignsMax','campaignsMedian','campaignsMode');
    cd(oldFolder);

end

    function [peakTimeArray,wnArray,osArray,qpArray,gainArray,f0Array,mrArray]=loadDataAndConfigureStorage(filePath,fileName)
        
        %% Variables definition
        statisticsArrray=[];
        waveFormCollection=[];
  
        %% Get campaign data from file
        
        load(fullfile(filePath,fileName));
        newFolderName=fileName(1:end-4);
        mkdir(fullfile(filePath,newFolderName));
        
        statisticsMatrix=cell2mat(statisticsArrray(2:end,:));
        
        peakTimeArray=statisticsMatrix(:,tpIndex);
        tpMatrix(:,end+1)=peakTimeArray;
      
        osArray=statisticsMatrix(:,osIndex);
        osMatrix(:,end+1)=osArray;
        if isempty(find(statisticsMatrix(:,end)>3, 1))
            stepAmplitude=1.3;
        else
            stepAmplitude=2.6;
        end
        gainArray=(statisticsMatrix(:,end)-statisticsMatrix(:,end-1))/stepAmplitude;
        gainMatrix(:,end+1)=gainArray;
        %         pulseWidthArray=statisticsMatrix(:,pulseWidthIndex);
        
        tauArray=1./sqrt((pi./log(osArray/100)).^2.+1);
        qpArray=1/2./tauArray;
        wnArray=pi*1e6./peakTimeArray./sqrt(1-tauArray.^2);
        for indice=1:length(wnArray)
            tau=tauArray(indice);
            wn=wnArray(indice);
            wr=wn*sqrt(1-2*tau^2);
            respuesta=tf(wnArray(indice)^2,[1 2*wnArray(indice)*tauArray(indice) wnArray(indice)^2]);
            f0Array(indice) = getGainCrossover(respuesta,db2mag(-3))/2/pi;
            mrArray(indice)=20*log10(norm(freqresp(respuesta,wr)));
        end
        f0Matrix(:,end+1)=f0Array;
        mrMatrix(:,end+1)=mrArray;
        wnMatrix(:,end+1)=wnArray;
        qpMatrix(:,end+1)=qpArray;
    end

    function plotAndSaveHistograms(peakTimeArray,wnArray,osArray,qpArray,gainArray,f0Array,mrArray,filePath,fileName)
        %% Generate peak time histogram
        
        hist(peakTimeArray,11);
        xlabel('Tiempo de pico [microsegundos]');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'Tp');
        
        %% Generate peak time errors histogram
        
        hist(wnArray,11);
        xlabel('Porcentaje de error del tiempo de pico vs valor te�rico');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'TpE');
        
        %% Generate overshoot percent histogram
        
        hist(osArray,35);
        xlabel('Porcentaje de sobreimpulso');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'Os');
        
        %% Generate overshoot percent error histogram
        
        hist(qpArray,35);
        xlabel('Porcentaje de error del sobreimpulso respecto al valor te�rico');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'OsE');
        
        %% Generate offset voltage histogram
        
        hist(gainArray,8);
        xlabel('Tensi�n de offset de salida del filtro [volts]');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'vOff');
        
        %          %% Generate pulse width histogram
        %
        %         hist(pulseWidthArray,5);
        %         xlabel('Ancho del pulso de entrada [microsegundos]');
        %         ylabel('Frecuencia absoluta');
        %         saveFigure(filePath,fileName,'PW');
        
    end

    function [currentCampaignMean,currentCampaignSD,currentCampaignMin,currentCampaignMax,currentCampaignMedian,currentCampaignMode]= getCampaignStatistics(peakTimeArray,wnArray,osArray,qpArray,gainArray,f0Array,mrArray,fileName)
        
        %% Evaluate peak time statistics
        [tpMeanValue,tpSDValue,tpMinValue,tpMaxValue,tpMedianValue,tpModeValue]=getStatisticalCharacteristics(peakTimeArray);
        
        %% Evaluate peak time error statistics
        [tpEMeanValue,tpESDValue,tpEMinValue,tpEMaxValue,tpEMedianValue,tpEModeValue]=getStatisticalCharacteristics(wnArray);
        
        %% Evaluate overshoot statistics
        [osMeanValue,osSDValue,osMinValue,osMaxValue,osMedianValue,osModeValue]=getStatisticalCharacteristics(osArray);
        
        %% Evaluate overshoot error statistics
        [osEMeanValue,osESDValue,osEMinValue,osEMaxValue,osEMedianValue,osEModeValue]=getStatisticalCharacteristics(qpArray);
        
        %% Evaluate offset statistics
        [vOffMeanValue,vOffSDValue,vOffMinValue,vOffMaxValue,vOffMedianValue,vOffModeValue]=getStatisticalCharacteristics(gainArray);
        
        %% Evaluate f-3dB statistics
        [f0MeanValue,f0SDValue,f0MinValue,f0MaxValue,f0MedianValue,f0ModeValue]=getStatisticalCharacteristics(f0Array);
        
        %% Evaluate frequency response ripple
        [mrMeanValue,mrSDValue,mrMinValue,mrMaxValue,mrMedianValue,mrModeValue]=getStatisticalCharacteristics(mrArray);
        %% Store statistics
        
        campaignName=fileName(1:end-4);
        currentCampaignMean={tpMeanValue,tpEMeanValue,osMeanValue,osEMeanValue,vOffMeanValue,f0MeanValue,mrMeanValue, campaignName};
        currentCampaignSD={tpSDValue,tpESDValue,osSDValue,osESDValue,vOffSDValue,f0SDValue,mrSDValue,campaignName};
        currentCampaignMin={tpMinValue,tpEMinValue,osMinValue,osEMinValue,vOffMinValue,f0MinValue,mrMinValue,campaignName};
        currentCampaignMax={tpMaxValue,tpEMaxValue,osMaxValue,osEMaxValue,vOffMaxValue,f0MaxValue,mrMaxValue,campaignName};
        currentCampaignMedian={tpMedianValue,tpEMedianValue,osMedianValue,osEMedianValue,vOffMedianValue,f0MedianValue,mrMedianValue,campaignName};
        currentCampaignMode={tpModeValue,tpEModeValue,osModeValue,osEModeValue,vOffModeValue,f0ModeValue,mrModeValue,campaignName};
        
        %% Statistics sub-function definition
        function [meanValue,sdValue,minValue,maxValue,medianValue,modeValue]=getStatisticalCharacteristics(valuesArray)
            meanValue=mean(valuesArray);
            sdValue=std(valuesArray);
            minValue=min(valuesArray);
            maxValue=max(valuesArray);
            medianValue=median(valuesArray);
            modeValue=mode(valuesArray);
        end
        
        
    end

    function headers=campaignStatisticsHeaders()
        headers{1}='Tiempo de pico';
        headers{2}='wn';
        headers{3}='Porcentaje de sobreimpulso';
        headers{4}='Qp';
        headers{5}='Ganancia';
        headers{6}='F0';
        headers{7}='Mr';
        headers{8}='Filtro';
    end

    function saveFigure(filePath,fileName,suffix)
        saveFolder=fileName(1:end-4);
        saveas(gcf,fullfile(filePath,saveFolder,strcat(fileName(1:end-4),suffix,'.fig')));
        set(gcf,'units','points','position',[8,8,1366,768]);
        print(gcf,'-djpeg',fullfile(filePath,saveFolder,strcat(fileName(1:end-4),suffix)));
    end

    function currentFolderName=getCurrentFolderName()
        [~, currentFolderName, ~] = fileparts(pwd);
    end

end