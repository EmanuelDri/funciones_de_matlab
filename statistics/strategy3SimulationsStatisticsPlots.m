function [tpStatistics,osStatistics,gainStatistics]=strategy3SimulationsStatisticsPlots(varargin)

global tpMatrix;
global filterNames;
global osMatrix;
global normalizedTpMatrix;
global gainMatrix;

filePath=uigetdir();
tic
if filePath~=0
    oldFolder=cd(filePath);
    matFiles=dir('*.mat');
    [filesNumber,~]=size(matFiles);
    
    disp(filesNumber);
    
    headers={'Promedio','Mediana','M�nimo','M�ximo','Rango','Desviaci�n Est�ndar','Coeficiente de variaci�n %','Error estandar','Rango intercuartilico','Filtro'};
    tpStatistics=cell(filesNumber,length(headers));
    tpStatistics(1,:)=headers;
    osStatistics=cell(filesNumber,length(headers));
    osStatistics(1,:)=headers;
    gainStatistics=cell(filesNumber,length(headers));
    gainStatistics(1,:)=headers;
    tpMatrix=[];
    intermediateTpMatrix=[];
    osMatrix=[];
    filterNames=cell(1,filesNumber);
    gainMatrix=[];
    
    for fileIndex=1:filesNumber
        disp(fileIndex);
        %% Load data
        fileName=matFiles(fileIndex).name;
        waveFormsArrray=loadDataAndConfigureStorage(filePath,fileName);
        tpValues=zeros(1,100);
        osValues=zeros(1,100);
        gainValues=zeros(1,100);
        for captureIndex=1:100
            x=waveFormsArrray{captureIndex,1};
            y=waveFormsArrray{captureIndex,2};
            [currentPeakTime,currentOsPercent] =simulateStrategy3( 1,y,x);
            tpValues(captureIndex)=currentPeakTime;
            osValues(captureIndex)=currentOsPercent;
            [lowState,hiState]=getBistateLevels(y);
            if(hiState-lowState<2)
                stepAmplitude=1.3;
            else
                stepAmplitude=2.6;
            end
            gainValues(captureIndex)=(hiState-lowState)/stepAmplitude;
        end
        %% Generate statistics tables
        
        tpMean=mean(tpValues); tpMax=max(tpValues); tpMin=min(tpValues);...
            tpIQE=iqr(tpValues); tpSD=std(tpValues);tpVC=tpSD/tpMean*100;...
            tpMedian=median(tpValues); tpSE=tpSD/sqrt(length(tpValues));
        currentTpStatistics={tpMean*1e6,tpMedian*1e6, tpMin*1e6, tpMax*1e6,(tpMax-tpMin)*1e6, tpSD*1e6, tpVC,...
            tpSE*1e6,tpIQE*1e6,fileName};
        
        tpStatistics(fileIndex+1,:)=currentTpStatistics;
        
        tpMatrix(end+1,:)=tpValues(1:100)';
        
        osMean=mean(osValues); osMax=max(osValues); osMin=min(osValues);...
            osIQE=iqr(osValues); osSD=std(osValues);osVC=osSD/osMean*100;...
            osMedian=median(osValues); osSE=osSD/sqrt(length(osValues));
        currentOsStatistics={osMean,osMedian, osMin, osMax,(osMax-osMin), osSD, osVC,...
            osSE,osIQE,fileName};
        
        osStatistics(fileIndex+1,:)=currentOsStatistics;
        
        osMatrix(end+1,:)=osValues(1:100)';
        
        gainMean=mean(gainValues); gainMax=max(gainValues); gainMin=min(gainValues);...
            gainIQE=iqr(gainValues); gainSD=std(gainValues);gainVC=gainSD/gainMean*100;...
            gainMedian=median(gainValues); gainSE=gainSD/sqrt(length(gainValues));
        currentGainStatistics={gainMean,gainMedian, gainMin, gainMax,(gainMax-gainMin), gainSD, gainVC,...
            gainSE,gainIQE,fileName};
        
        
        gainStatistics(fileIndex+1,:)=currentGainStatistics;
        
        gainMatrix(end+1,:)=gainValues(1:100)';
        
        filterNames{fileIndex}=fileName;
        
    end
    
    mkdir(getCurrentFolderName);
    %     save(fullfile(pwd,getCurrentFolderName,getCurrentFolderName),...
    %         'tpStatistics','osStatistics','initialStateStatistics','finalStateStatistics','peakVoltageStatistics');
    cd(oldFolder);
    normalizedTpMatrix=normalizeMatrix(tpMatrix);
    %     normalizedOsMatrix=normalizeMatrix(osMatrix);
    %     figure(1); boxplot(tpMatrix',filterNames,'Whisker',inf,'Orientation','horizontal');title('tp');
    %     figure(2); boxplot(osMatrix',filterNames,'Whisker',inf,'Orientation','horizontal');title('os');
    %     figure(3); boxplot(normalizedTpMatrix',filterNames,'Whisker',inf,'Orientation','horizontal');title('tp');
    %     figure(4); boxplot(normalizedOsMatrix',filterNames,'Whisker',inf,'Orientation','horizontal');title('os');
    %     figure; boxplot(intermediateTpMatrix',filterNames,'Whisker',100);title('tp int');
    %
    %     figure; boxplot(tpMatrix',filterNames,'Whisker',100);title('tp'); hold on;
    %     boxplot(intermediateTpMatrix',filterNames,'Whisker',100,'Colors','g');title('tp int');
    
    
    
end
toc

    function waveFormsArray=loadDataAndConfigureStorage(filePath,fileName)
        
        %% Variables definition
        waveFormsArray=[];
        
        %% Get campaign data from file
        
        load(fullfile(filePath,fileName));
        newFolderName=fileName(1:end-4);
        mkdir(fullfile(filePath,newFolderName));
    end

    function currentFolderName=getCurrentFolderName()
        [~, currentFolderName, ~] = fileparts(pwd);
    end

    function normalizedMatrix=normalizeMatrix(inputMatrix)
        rowsMedians=median(inputMatrix');
        mediansMatrix=zeros(size(inputMatrix'));
        rowsLength=length(inputMatrix);
        for index=1:rowsLength
            mediansMatrix(index,:)=rowsMedians;
        end
        normalizedMatrix=inputMatrix./mediansMatrix';
    end

end