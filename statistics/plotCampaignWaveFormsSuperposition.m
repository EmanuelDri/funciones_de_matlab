[capturesNumber,~]=size(waveForms);
figure, hold on;
for captureIndex=1:capturesNumber
    xch1=waveForms{captureIndex,1};
    ych1=waveForms{captureIndex,2};
    xch2=waveForms{captureIndex,3};
    ych2=waveForms{captureIndex,4};
    plot(xch1,ych1);
end