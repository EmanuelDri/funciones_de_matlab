function campaignStatisticsPlots()

%% Constants definition
tpIndex=1;
tpErrorIndex=2;
osIndex=3;
osErrorIndex=4;
vOffsetIndex=5;
pulseWidthIndex=9;


filePath=uigetdir();

if filePath~=0
    oldFolder=cd(filePath);
    matFiles=dir('*.mat');
    [filesNumber,~]=size(matFiles);
    campaignsMean=cell(filesNumber+1,6);
    campaignsSD=cell(filesNumber+1,6);
    campaignsMin=cell(filesNumber+1,6);
    campaignsMax=cell(filesNumber+1,6);
    campaignsMedian=cell(filesNumber+1,6);
    campaignsMode=cell(filesNumber+1,6);
    
    global tpMatrix;
    global osMatrix;
    global kMatrix;
    tpMatrix=[];
    osMatrix=[];
    kMatrix=[];
    
    for fileIndex=1:filesNumber
        
        %% Load dataa
        fileName=matFiles(fileIndex).name;
        [peakTimeArray,osArray,kArray]=loadDataAndConfigureStorage(filePath,fileName);
        
        %% Generate statistics tables
        [currentCampaignMean,currentCampaignSD,currentCampaignMin,currentCampaignMax,currentCampaignMedian,currentCampaignMode]= getCampaignStatistics(peakTimeArray,osArray,kArray,fileName);
        campaignsMean(fileIndex+1,1:6)=currentCampaignMean;
        campaignsSD(fileIndex+1,1:6)=currentCampaignSD;
        campaignsMin(fileIndex+1,1:6)=currentCampaignMin;
        campaignsMax(fileIndex+1,1:6)=currentCampaignMax;
        campaignsMedian(fileIndex+1,1:6)=currentCampaignMedian;
        campaignsMode(fileIndex+1,1:6)=currentCampaignMode;
        
        %% Plot histograms
                plotAndSaveHistograms(peakTimeArray,osArray,kArray,filePath,fileName);
    end
    campaignsMean(1,1:6)=campaignStatisticsHeaders;
    campaignsSD(1,1:6)=campaignStatisticsHeaders;
    campaignsMin(1,1:6)=campaignStatisticsHeaders;
    campaignsMax(1,1:6)=campaignStatisticsHeaders;
    campaignsMedian(1,1:6)=campaignStatisticsHeaders;
    campaignsMode(1,1:6)=campaignStatisticsHeaders;
    mkdir(getCurrentFolderName);
    save(fullfile(pwd,getCurrentFolderName,getCurrentFolderName),'campaignsMean','campaignsSD','campaignsMin','campaignsMax','campaignsMedian','campaignsMode');
    cd(oldFolder);

end

    function [peakTimeArray,osArray,kArray]=loadDataAndConfigureStorage(filePath,fileName)
        
        %% Variables definition
        statisticsArray=[];
        waveFormCollection=[];
  
        %% Get campaign data from file
        
        load(fullfile(filePath,fileName));
        newFolderName=fileName(1:end-4);
        mkdir(fullfile(filePath,newFolderName));
        
        statisticsMatrix=cell2mat(statisticsArray(2:end,:));
        
        peakTimeArray=statisticsMatrix(:,tpIndex);
        osArray=statisticsMatrix(:,osIndex);
        if(statisticsMatrix(1:7)<3)
            stepAmplitude=1.3;
        else
            stepAmplitude=2.6;
        end
        kArray=(statisticsMatrix(:,7)-statisticsMatrix(:,6))/stepAmplitude;
        %         pulseWidthArray=statisticsMatrix(:,pulseWidthIndex);
        
        osMatrix(:,end+1)=osArray;
        tpMatrix(:,end+1)=tpArray;
        kMatrix(:,end+1)=kArray;
    end

    function plotAndSaveHistograms(peakTimeArray,peaktTimeErrorArray,osArray,osErrorArray,vOffsetArray,filePath,fileName)
        %% Generate peak time histogram
        
        hist(peakTimeArray,11);
        xlabel('Tiempo de pico [microsegundos]');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'Tp');
        
        %% Generate peak time errors histogram
        
        hist(peaktTimeErrorArray,11);
        xlabel('Porcentaje de error del tiempo de pico vs valor te�rico');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'TpE');
        
        %% Generate overshoot percent histogram
        
        hist(osArray,35);
        xlabel('Porcentaje de sobreimpulso');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'Os');
        
        %% Generate overshoot percent error histogram
        
        hist(osErrorArray,35);
        xlabel('Porcentaje de error del sobreimpulso respecto al valor te�rico');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'OsE');
        
        %% Generate offset voltage histogram
        
        hist(vOffsetArray,10);
        xlabel('Tensi�n de offset de salida del filtro [volts]');
        ylabel('Frecuencia absoluta');
        saveFigure(filePath,fileName,'vOff');
        
        %          %% Generate pulse width histogram
        %
        %         hist(pulseWidthArray,5);
        %         xlabel('Ancho del pulso de entrada [microsegundos]');
        %         ylabel('Frecuencia absoluta');
        %         saveFigure(filePath,fileName,'PW');
        
    end

    function [currentCampaignMean,currentCampaignSD,currentCampaignMin,currentCampaignMax,currentCampaignMedian,currentCampaignMode]= getCampaignStatistics(peakTimeArray,peaktTimeErrorArray,osArray,osErrorArray,vOffsetArray,fileName)
        
        %% Evaluate peak time statistics
        [tpMeanValue,tpSDValue,tpMinValue,tpMaxValue,tpMedianValue,tpModeValue]=getStatisticalCharacteristics(peakTimeArray);
        
        %% Evaluate peak time error statistics
        [tpEMeanValue,tpESDValue,tpEMinValue,tpEMaxValue,tpEMedianValue,tpEModeValue]=getStatisticalCharacteristics(peaktTimeErrorArray);
        
        %% Evaluate overshoot statistics
        [osMeanValue,osSDValue,osMinValue,osMaxValue,osMedianValue,osModeValue]=getStatisticalCharacteristics(osArray);
        
        %% Evaluate overshoot error statistics
        [osEMeanValue,osESDValue,osEMinValue,osEMaxValue,osEMedianValue,osEModeValue]=getStatisticalCharacteristics(osErrorArray);
        
        %% Evaluate offset statistics
        [vOffMeanValue,vOffSDValue,vOffMinValue,vOffMaxValue,vOffMedianValue,vOffModeValue]=getStatisticalCharacteristics(vOffsetArray);
        
        %% Store statistics
        
        campaignName=fileName(1:end-4);
        currentCampaignMean={tpMeanValue,tpEMeanValue,osMeanValue,osEMeanValue,vOffMeanValue,campaignName};
        currentCampaignSD={tpSDValue,tpESDValue,osSDValue,osESDValue,vOffSDValue,campaignName};
        currentCampaignMin={tpMinValue,tpEMinValue,osMinValue,osEMinValue,vOffMinValue,campaignName};
        currentCampaignMax={tpMaxValue,tpEMaxValue,osMaxValue,osEMaxValue,vOffMaxValue,campaignName};
        currentCampaignMedian={tpMedianValue,tpEMedianValue,osMedianValue,osEMedianValue,vOffMedianValue,campaignName};
        currentCampaignMode={tpModeValue,tpEModeValue,osModeValue,osEModeValue,vOffModeValue,campaignName};
        
        %% Statistics sub-function definition
        function [meanValue,sdValue,minValue,maxValue,medianValue,modeValue]=getStatisticalCharacteristics(valuesArray)
            meanValue=mean(valuesArray);
            sdValue=std(valuesArray);
            minValue=min(valuesArray);
            maxValue=max(valuesArray);
            medianValue=median(valuesArray);
            modeValue=mode(valuesArray);
        end
        
        
    end

    function headers=campaignStatisticsHeaders()
        headers{1}='Tiempo de pico';
        headers{2}='Error porcentual del tiempo de pico vs valor te�rico';
        headers{3}='Porcentaje de sobreimpulso';
        headers{4}='Error porcentual del sobreimpulso vs valor te�rico';
        headers{5}='Tensi�n de offset';
        headers{6}='Filtro';
    end

    function saveFigure(filePath,fileName,suffix)
        saveFolder=fileName(1:end-4);
        saveas(gcf,fullfile(filePath,saveFolder,strcat(fileName(1:end-4),suffix,'.fig')));
        set(gcf,'units','points','position',[10,10,13660,7680]);
        print(gcf,'-djpeg',fullfile(filePath,saveFolder,strcat(fileName(1:end-4),suffix)));
    end

    function currentFolderName=getCurrentFolderName()
        [~, currentFolderName, ~] = fileparts(pwd);
    end

end