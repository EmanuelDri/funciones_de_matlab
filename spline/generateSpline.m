function [fittedYData,gof,output]=generateSpline(timeValues,voltageValues,smoothingFactor,varargin)
    
     [xData, yData] = prepareCurveData( timeValues, voltageValues );
        % Set up fittype and options.
        ft = fittype( 'smoothingspline' );
        opts = fitoptions( 'Method', 'SmoothingSpline' );
        opts.Normalize = 'on';

        opts.SmoothingParam =smoothingFactor; %este era, por cierto

        % Fit model to data.
        [fitModel, gof,output] = fit( xData, yData, ft, opts );
        if isempty(varargin)
            fittedYData=feval(fitModel,timeValues);
        else
            fittedYData=feval(fitModel,varargin{1});
        end
end