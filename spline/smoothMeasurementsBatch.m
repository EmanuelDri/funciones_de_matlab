function smoothedMeasurements=smoothMeasurementsBatch(allMeasurements,smoothingFactor)
% allMeasurements=importdata(measurementsFile);
smoothedMeasurements=cell(length(allMeasurements),4);
ys=allMeasurements(:,2);
xs=allMeasurements(:,1);
inputsT=allMeasurements(:,3);
inputsY=allMeasurements(:,4);
parfor index=1:length(allMeasurements)
    x=xs{index};
    y=ys{index};
    xin=inputsT{index};
    yin=inputsY{index};
    y=generateSpline(x,y,smoothingFactor); y=reshape(y,1,[]);
    smoothedMeasurements(index,:)={x,y,xin,yin};
end

% save('smoothedMeasurements.mat','smoothedMeasurements');