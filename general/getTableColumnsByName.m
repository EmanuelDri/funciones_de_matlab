function [columns,varargout]=getTableColumnsByName(table,name,caseSensitive)
tableVariables=table.Properties.VariableNames;
if ~caseSensitive
    tableVariables=lower(tableVariables);
end
indexes=find(contains(tableVariables,name));
columns=[];
if length(indexes)>1
    for index=indexes(1):indexes(end)
        columns=[columns,table.(index)];
    end
elseif length(indexes)==1
    columns=table.(indexes);
end

if nargout>1
    varargout={indexes};
end

end