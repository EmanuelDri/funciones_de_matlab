function boxplotDiffSizeArrays(arrays,labels,varargin)
%%
% 
%  This function creates a boxplot of the vectors contained in the cell
%  array <arrays> and assignes them the labels contained in the cell array
%  labels, also admits Name,Value arguments to boxplot passed in the
%  varargin input
%%


if length(arrays)~=length(labels)
    error('The number of arrays does not match the number of labels');
elseif mod(length(varargin),2)>0
    error('The number of boxplot Name,Value parameters is not pair');
else
    elementsNumber=length(arrays);
    data=[];
    redundantLabels=[];
    for index=1:elementsNumber
        currentArray=arrays{index};
        data=[data;reshape(currentArray,[],1)];
        if ~isempty(labels)
            redundantLabels=[redundantLabels;repmat(labels{index},...
                length(currentArray),1)];
        end
    end
    
    if isempty(labels)
        boxplot(data,varargin{:});
    else
        boxplot(data,redundantLabels,varargin{:});
    end
    
end
