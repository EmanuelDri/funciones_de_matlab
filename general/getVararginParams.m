function varargout=getVararginParams(paramsList,varargin)
varargout=paramsList(2:2:end);

if~isempty(varargin)
    characterInputs=find(cellfun(@ischar,varargin));
    for index=characterInputs
        currParameter=varargin{index};
        parametererIndex=find(ismember(paramsList(1:2:end),currParameter));
        if ~isempty(parametererIndex)
            varargout{parametererIndex}=varargin{index+1};
        end
    end
end

end