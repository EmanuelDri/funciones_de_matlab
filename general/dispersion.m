function delta=dispersion(vector)
delta=(max(vector)-min(vector))/mean(vector)*100;
end