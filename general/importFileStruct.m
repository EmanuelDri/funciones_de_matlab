function data=importFileStruct(fileStruct)
fileAddress=fullfile(fileStruct.folder,fileStruct.name);
data=importdata(fileAddress);
end