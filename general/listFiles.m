function filesStructs=listFiles(searchString)
d = dir(searchString);
isub = ~[d(:).isdir]; %# returns logical vector
filesStructs=d(isub);
end
