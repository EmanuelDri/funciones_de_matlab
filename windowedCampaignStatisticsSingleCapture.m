function summary=windowedCampaignStatisticsSingleCapture(varargin)
if ~isempty(varargin)
    dirPattern=varargin{1};
else
    dirPattern='*.mat';
end
filePath=pwd;
% filePath=uigetdir();
tic
mkdir analysis
if filePath~=0
    matFiles=dir(fullfile(filePath,dirPattern));
    [filesNumber,~]=size(matFiles);
    [tpStatistics,osStatistics,gainStatistics,qpStatistics,wnStatistics,fcStatistics,mrStatistics]=initializeStatisticsCells(filesNumber);

    for fileIndex=1:filesNumber
        disp(fileIndex);
        %% Load data
        fileName=matFiles(fileIndex).name;
        fileAddress=fullfile(filePath,fileName);
        waveFormsArray=importdata(fileAddress);

        %% Generate statistics tables
        statisticsTable=getWindowedCampaignStatisticsSingleCapture(waveFormsArray);
        [currentTpStatistics,currentOSStatistics,currentGainStatistics,currentQpStatistics,currentWnStatistics,currentMrStatistics,currentFcStatistics]=generateStatistics(statisticsTable,fileName);
        save(fullfile(filePath,'analysis',fileName),'statisticsTable')

        tpStatistics(fileIndex+1,:)=currentTpStatistics;
        osStatistics(fileIndex+1,:)=currentOSStatistics;
        gainStatistics(fileIndex+1,:)=currentGainStatistics;
        qpStatistics(fileIndex+1,:)=currentQpStatistics;
        wnStatistics(fileIndex+1,:)=currentWnStatistics;
        mrStatistics(fileIndex+1,:)=currentMrStatistics;
        fcStatistics(fileIndex+1,:)=currentFcStatistics;

    end
    summary=cell(2,7);
    summary(1,:)={'Tp','OS','Gain','Qp','Wn','Mr','Fc'};
    summary(2,:)={tpStatistics,osStatistics,gainStatistics,qpStatistics,wnStatistics,mrStatistics,fcStatistics};
    save('summary.mat','summary');
end
toc

    function [tpStatistics,osStatistics,gainStatistics,qpStatistics,wnStatistics,fcStatistics,mrStatistics]=initializeStatisticsCells(filesNumber)
        headers={'Mean','Maximum','Minimum','Dispersion','Filter'};
        pattern=cell(filesNumber,length(headers));
        pattern(1,:)=headers;
        tpStatistics=pattern;
        osStatistics=pattern;
        gainStatistics=pattern;
        qpStatistics=pattern;
        wnStatistics=pattern;
        fcStatistics=pattern;
        mrStatistics=pattern;
    end
end

function [currentTpStatistics,currentOSStatistics,currentGainStatistics,currentQpStatistics,currentWnStatistics,currentMrStatistics,currentFcStatistics]=generateStatistics(statisticsTable,fileName)
osValues=cell2mat(statisticsTable(2:end,1));
tpValues=cell2mat(statisticsTable(2:end,2))*1e6;
gainValues=cell2mat(statisticsTable(2:end,3));
qpValues=cell2mat(statisticsTable(2:end,4));
wnValues=cell2mat(statisticsTable(2:end,5));
fcValues=cell2mat(statisticsTable(2:end,6));
mrValues=cell2mat(statisticsTable(2:end,7));

currentTpStatistics=calculateParameterStatistics(tpValues,fileName);
currentOSStatistics=calculateParameterStatistics(osValues,fileName);
currentGainStatistics=calculateParameterStatistics(gainValues,fileName);
currentQpStatistics=calculateParameterStatistics(qpValues,fileName);
currentWnStatistics=calculateParameterStatistics(wnValues,fileName);
currentMrStatistics=calculateParameterStatistics(mrValues,fileName);
currentFcStatistics=calculateParameterStatistics(fcValues,fileName);

    function statistics=calculateParameterStatistics(parameterValues,fileName)
        average=mean(parameterValues);
        maximum=max(parameterValues);
        minimum=min(parameterValues);
        dispersion=(maximum-minimum)/average*100;
        statistics={average,maximum,minimum,dispersion,fileName};
    end
end
