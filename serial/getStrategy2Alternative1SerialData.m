function statisticsArray=getStrategy2Alternative1SerialData(executions,pulseWidthWordsArray,filterPosition)
%%
% Parameters definition
% executions: number of tram BIST algorithm execution for each pulse width
% word
% pulseWidthWordsArray: collection of filter input pulse width words
% filterPosition: string 'vertical'/'horizontal
%%

%fclose(instrfind);delete(instrfind)

theoreticalTp=287.5;
theoreticalOS=15.68706233;
adcResolution=2^14;
RefLo=1.2;
RefHi=3.8;

% filesNumber=filesNumber+1;
clear statisticsArray;

statisticsArray{1,1}='Tp';
statisticsArray{1,2}='Error tp';
statisticsArray{1,3}='%OS';
statisticsArray{1,4}='Error %OS';%fullfile(filePath,saveGame(1).name);
statisticsArray{1,5}='Voffset';
statisticsArray{1,6}='ADC os';
statisticsArray{1,7}='ADC Voff';
statisticsArray{1,8}='Vos';
statisticsArray{1,9}='Ancho de pulso';
statisticsArray{1,10}='Palabra de ancho de pulso';
statisticsArray{1,11}='ADC ini';
statisticsArray{1,12}='ADC fin';

serialPort=serialSetup();

executionsNumber=executions*length(pulseWidthWordsArray);
pulseWidthWordsNumber=length(pulseWidthWordsArray);

for pulseWidthIndex=1:pulseWidthWordsNumber
    
    %% Change filter input pulse width word
    currentPulseWord=pulseWidthWordsArray(pulseWidthIndex);
    fprintf(serialPort,['pw ' num2str(currentPulseWord)]);
    
    %% Tram execution
    for execution=1:executions

        fprintf(serialPort,'exe 1');
        gatherTramExecutionData();
        
    end
end

fclose(serialPort);

    function gatherTramExecutionData()
        tramExecution=execution+(executions*(pulseWidthIndex-1));
        serialLine=getSerialLine(serialPort);
        
        k=0.5;
        agnd=2.5;
        
        if (~isempty(serialLine))

            [adcOSVoltage,tp, adcVOffset,pw,adcInitialState,adcFinalState]=getSerialLineData(serialLine);
            
            adcStepDelta=(RefHi-RefLo)/adcResolution;
            
            vOffset=adcStepDelta*adcVOffset;
            if strcmp(filterPosition,'vertical')
                osVoltage=((adcOSVoltage*adcStepDelta)+1.2); % Esta línea y la de abajo son para filtros en posición vertical
                finalStateVoltage=((adcFinalState*adcStepDelta)+1.2);
                initialStateVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,k,adcInitialState);
                os=(osVoltage-finalStateVoltage)/(finalStateVoltage-initialStateVoltage)*100;
            else
%                 osVoltage=((adcOSVoltage*adcStepDelta)+1.2-2.5)*2+2.5; % Esta línea y la de abajo son para filtros en posición horizontal
                osVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,k,adcOSVoltage);
                initialStateVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,k,adcInitialState);
                finalStateVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,k,adcFinalState);
                os=(osVoltage-finalStateVoltage)/(finalStateVoltage-initialStateVoltage)*100;
%                 os=(osVoltage-vOffset-RefHi)/(RefHi-RefLo)*100;
            end
            
            tpError=errorR(tp,theoreticalTp);
            osError=errorR(os,theoreticalOS);
            
            statisticsArray{tramExecution+1,1}=tp;
            statisticsArray{tramExecution+1,2}=tpError;
            statisticsArray{tramExecution+1,3}=os;
            statisticsArray{tramExecution+1,4}=osError;
            statisticsArray{tramExecution+1,5}=vOffset;
            statisticsArray{tramExecution+1,6}=adcOSVoltage;
            statisticsArray{tramExecution+1,7}=adcVOffset;
            statisticsArray{tramExecution+1,8}=osVoltage;
            statisticsArray{tramExecution+1,9}=pw;
            statisticsArray{tramExecution+1,10}=currentPulseWord;
            statisticsArray{tramExecution+1,11}=initialStateVoltage;
            statisticsArray{tramExecution+1,12}=finalStateVoltage;
            disp(statisticsArray(tramExecution+1,:));
        end
    end

    function serialPort=serialSetup()
       serialPort = serial('COM6');
        set(serialPort,'DataBits',8);
        set(serialPort,'StopBits',1);
        set(serialPort,'BaudRate',115200);
        set(serialPort,'Parity','none');
        set(serialPort,'Terminator',13);
        set(serialPort,'ReadAsyncMode','continuous');
        fopen(serialPort);
        disp(serialPort);
    end

    function serialLine=getSerialLine(serialPort)
        serialLine=fscanf(serialPort,'%i');
    end

    function [os, tp, vOffset, pulseWidth, initialState,finalState]=getSerialLineData(serialLine)
        tp=serialLine(1);
        os=serialLine(2);
        vOffset=serialLine(3);
        pulseWidth=serialLine(4);
        initialState=serialLine(5);
        finalState=serialLine(6);
    end

    function vin=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,k,adcValue)
        vin=(adcValue*RefHi - adcValue*RefLo - agnd*adcResolution+ RefLo*adcResolution+ agnd*k*adcResolution)/(k*adcResolution);
    end

end

