function statisticsArray=getStrategy3SerialData(executions,pgADCGain)
%%
% Parameters definition
% executions: number of tram BIST algorithm execution for each pulse width
% word
% pulseWidthWordsArray: collection of filter input pulse width words
% filterPosition: string 'vertical'/'horizontal
%%

%fclose(instrfind);delete(instrfind)

theoreticalTp=287.5;
theoreticalOS=15.68706233;
adcResolution=2^14;
RefLo=1.2;
RefHi=3.8;

% filesNumber=filesNumber+1;
clear statisticsArray;

statisticsArray{1,1}='Tp';
statisticsArray{1,2}='Error tp';
statisticsArray{1,3}='%OS';
statisticsArray{1,4}='Error %OS';%fullfile(filePath,saveGame(1).name);
statisticsArray{1,5}='Voffset';
statisticsArray{1,6}='ADC os';
statisticsArray{1,7}='ADC Voff';
statisticsArray{1,8}='Vos';
statisticsArray{1,9}='ADC ini';
statisticsArray{1,10}='ADC fin';

serialPort=serialSetup();




%% Tram execution
pause on;
fprintf(serialPort,'exe 255');
for execution=1:executions
    %     disp(execution);
    fprintf(serialPort,'exe 255');
    gatherTramExecutionData();
    if execution==250
        fprintf(serialPort,'exe 255');
    end
    
end

fclose(serialPort);

    function gatherTramExecutionData()
        %         pause(.5);
        serialLine=getSerialLine(serialPort);
        
        %         pgADC=pgADC; %esta es la ganancia del amplificador del ADC
        agnd=2.5;
        
        if (~isempty(serialLine))
            
            [adcOSVoltage,tp, adcVOffset,adcInitialState,adcFinalState]=getSerialLineData(serialLine);
            
            adcStepDelta=(RefHi-RefLo)/adcResolution;
            
            vOffset=adcStepDelta*adcVOffset;
            %             if strcmp(filterPosition,'vertical')
            %                 osVoltage=((adcOSVoltage*adcStepDelta)+1.2); % Esta línea y la de abajo son para filtros en posición vertical
            %                 finalStateVoltage=((adcFinalState*adcStepDelta)+1.2);
            %                 initialStateVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,k,adcInitialState);
            %                 os=(osVoltage-finalStateVoltage)/(finalStateVoltage-initialStateVoltage)*100;
            %             else
            %                 osVoltage=((adcOSVoltage*adcStepDelta)+1.2-2.5)*2+2.5; % Esta línea y la de abajo son para filtros en posición horizontal
            osVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,pgADCGain,adcOSVoltage);
            initialStateVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,pgADCGain,adcInitialState);
            finalStateVoltage=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,pgADCGain,adcFinalState);
            os=(osVoltage-finalStateVoltage)/(finalStateVoltage-initialStateVoltage)*100;
            %                 os=(osVoltage-vOffset-RefHi)/(RefHi-RefLo)*100;
            %             end
            
            tpError=errorR(tp,theoreticalTp);
            osError=errorR(os,theoreticalOS);
            
            statisticsArray{execution+1,1}=tp;
            statisticsArray{execution+1,2}=tpError;
            statisticsArray{execution+1,3}=os;
            statisticsArray{execution+1,4}=osError;
            statisticsArray{execution+1,8}=osVoltage;
            statisticsArray{execution+1,9}=initialStateVoltage;
            statisticsArray{execution+1,10}=finalStateVoltage;
            disp(statisticsArray(execution+1,:));
            %             if tp>350
            %                 input('error');
            %             end
            %             if os<15
            %                 input('OS<14');
            %             end
            %             if os>16.15
            %                 input('OS>16.15');
            %             end
        end
    end

    function serialPort=serialSetup()
        serialPort = serial('COM3');
        set(serialPort,'DataBits',8);
        set(serialPort,'StopBits',1);
        set(serialPort,'BaudRate',9600);
        set(serialPort,'Parity','none');
        set(serialPort,'Terminator',13);
        set(serialPort,'ReadAsyncMode','continuous');
        set(serialPort,'Timeout',30);
        fopen(serialPort);
        disp(serialPort);
    end

    function serialLine=getSerialLine(serialPort)
        serialLine=fscanf(serialPort,'%x');
    end

    function [os, tp, vOffset, initialState,finalState]=getSerialLineData(serialLine)
        tp=serialLine(1)/12;
        os=serialLine(2);
        vOffset=0;
        %         vOffset=serialLine(3);
        initialState=serialLine(3);
        finalState=serialLine(4);
    end

    function vin=convertFromADCToVoltage(RefHi,RefLo,agnd,adcResolution,k,adcValue)
        vin=((adcValue/adcResolution*(RefHi - RefLo) + RefLo-agnd)/k)+agnd;
    end

end

