function tramFaultInjection(capacitor,value)

delete(instrfind('Port','COM4'));

serialPort=serialSetup();
switch capacitor
    case '1'
        capacitorCommand=1;
    case '2'
        capacitorCommand=2;
    case '3'
        capacitorCommand=3;
    case '4'
        capacitorCommand=4;
    case 'a'
        capacitorCommand=5;
    case 'b'
        capacitorCommand=6;
end        

fprintf(serialPort, [ 'reconf ' num2str(capacitorCommand) ' ' num2str(value)]);

fclose(serialPort);

% [fileName,filePath]=uigetfile();

    function serialPort=serialSetup()
        serialPort = serial('COM4');
        set(serialPort,'DataBits',8);
        set(serialPort,'StopBits',1);
        set(serialPort,'BaudRate',4761);
        set(serialPort,'Parity','none');
        set(serialPort,'Terminator',13);
        set(serialPort,'ReadAsyncMode','continuous');
        fopen(serialPort);
        disp(serialPort);
    end

end

