function statisticsArray=getStrategy2Alternative2SerialData()

serialPort=serialSetup();
fclose(instrfind);delete(instrfind)

theoreticalTp=287.5;
theoreticalOS=15.68706233;
adcResolution=2^14;
RefLo=1.2;
RefHi=3.8;

% filesNumber=filesNumber+1;
clear statisticsArray;

statisticsArray{1,1}='Tp';
statisticsArray{1,2}='Error tp';
statisticsArray{1,3}='%OS';
statisticsArray{1,4}='Error %OS';%fullfile(filePath,saveGame(1).name);
statisticsArray{1,5}='Voffset';
statisticsArray{1,6}='ADC os';
statisticsArray{1,7}='ADC Voff';
statisticsArray{1,8}='Vos';
% statisticsArray{1,9}='Ancho de pulso';

serialPort=serialSetup();
tic
pause on;

for tramExecution=1:1000
    flushinput(serialPort);
    fprintf(serialPort,'exe 1');
%     pause(7);
    serialLine=getSerialLine(serialPort);
    if (~isempty(serialLine))
%         [adcOSVoltage,tp, adcVOffset, pw]=getSerialLineData(serialLine);
        [adcOSVoltage,tp, adcVOffset]=getSerialLineData(serialLine);

        adcStepDelta=(RefHi-RefLo)/adcResolution;
        
        vOffset=adcStepDelta*adcVOffset;
        osVoltage=((adcOSVoltage*adcStepDelta)+1.2-2.5)*2+2.5; % Esta línea y la de abajo son para filtros en posición horizontal
        os=(osVoltage-vOffset-RefHi)/(RefHi-RefLo)*100;
%         osVoltage=((adcOSVoltage*adcStepDelta)+2.5); % Esta línea y la de abajo son para filtros en posición vertical
%         os=(osVoltage-vOffset-(RefHi-RefLo)/2-RefLo)/(RefHi/2-RefLo/2)*100;
        
        tpError=errorR(tp,theoreticalTp);
        osError=errorR(os,theoreticalOS);
        
        statisticsArray{tramExecution+1,1}=tp;
        statisticsArray{tramExecution+1,2}=tpError;
        statisticsArray{tramExecution+1,3}=os;
        statisticsArray{tramExecution+1,4}=osError;
        statisticsArray{tramExecution+1,5}=vOffset;
        statisticsArray{tramExecution+1,6}=adcOSVoltage;
        statisticsArray{tramExecution+1,7}=adcVOffset;
        statisticsArray{tramExecution+1,8}=osVoltage;
%         statisticsArray{tramExecution+1,9}=pw;
        disp(statisticsArray(tramExecution+1,:));
    end
    %     end
end
save('24MHzIzqArr','statisticsArray');
clock

toc

fclose(serialPort);

% [fileName,filePath]=uigetfile();

    function serialPort=serialSetup()
        serialPort = serial('com4');
        set(serialPort,'DataBits',8);
        set(serialPort,'StopBits',1);
        set(serialPort,'BaudRate',57600);
        set(serialPort,'Parity','none');
        set(serialPort,'Terminator',13);
        set(serialPort,'ReadAsyncMode','continuous');
        fopen(serialPort);
        disp(serialPort);
    end
    
    function serialLine=getSerialLine(serialPort)
        serialLine=fscanf(serialPort,'%i');
    end
    
    function [os, tp, vOffset]=getSerialLineData(serialLine)
        tp=serialLine(1);
        os=serialLine(2);
        vOffset=serialLine(3);
    end

%     function [os, tp, vOffset, pulseWidth]=getSerialLineData(serialLine)
%         tp=serialLine(1);
%         os=serialLine(2);
%         vOffset=serialLine(3);
%         pulseWidth=serialLine(4);
%     end
end

