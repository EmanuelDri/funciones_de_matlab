function statisticsArray=getSerialStatisticsEstrategia1()

% fclose(instrfind);delete(instrfind)

theoreticalTp=287.5;
theoreticalOS=15.68706233;
adcResolution=64;
RefLo=1.2;
RefHi=3.8;
agnd=(RefHi-RefLo)/2+RefLo;

% filesNumber=filesNumber+1;
clear statisticsArray;

statisticsArray{1,1}='Tp';
statisticsArray{1,2}='Error tp';
statisticsArray{1,3}='%OS';
statisticsArray{1,4}='Error %OS';%fullfile(filePath,saveGame(1).name);
statisticsArray{1,5}='Voffset';
statisticsArray{1,6}='Vos';
% statisticsArray{1,9}='Ancho de pulso';
adcStepDelta=(RefHi-RefLo)/adcResolution;

serialPort=serialSetup();
tic

for tramExecution=1:3000
    fprintf(serialPort,'exe 1');
    [timeArray, samplesArray]=getSerialLineWaveForm(serialPort);
    waveFormCollection{tramExecution,1}=timeArray;
    waveFormCollection{tramExecution,2}=samplesArray;
    timeArray=timeArray/12;
    samplesArray=(samplesArray*adcStepDelta+RefLo-agnd)*2+agnd;
    [tp,os,osVoltage]=determineOvershootAndPeakTime(timeArray',samplesArray');
    
    os=os*100;
    vOffset=mode(samplesArray)-RefHi;
    tpError=errorR(tp,theoreticalTp);
    osError=errorR(os,theoreticalOS);
    
    statisticsArray{tramExecution+1,1}=tp;
    statisticsArray{tramExecution+1,2}=tpError;
    statisticsArray{tramExecution+1,3}=os;
    statisticsArray{tramExecution+1,4}=osError;
    statisticsArray{tramExecution+1,5}=vOffset;
    statisticsArray{tramExecution+1,6}=osVoltage;
    
    disp(statisticsArray(tramExecution+1,:));
    %     end
end
save('campaña/estrategia1/Estrategia1VColumna3','statisticsArray','waveFormCollection');
clock

toc

fclose(serialPort);

% [fileName,filePath]=uigetfile();

    function serialPort=serialSetup()
        serialPort = serial('COM6');
        set(serialPort,'DataBits',8);
        set(serialPort,'StopBits',1);
        set(serialPort,'BaudRate',115200);
        set(serialPort,'Parity','none');
        set(serialPort,'Terminator',13);
        set(serialPort,'ReadAsyncMode','continuous');
        fopen(serialPort);
        disp(serialPort);
    end


    function [os, tp, vOffset]=getSerialLineData(serialLine)
        tp=serialLine(1);
        os=serialLine(2);
        vOffset=serialLine(3);
    end

    function [timeArray, samplesArray]=getSerialLineWaveForm(serialPort)
        samplesArray=fscanf(serialPort,'%i');
        timeArray=fscanf(serialPort,'%i');
    end

end

