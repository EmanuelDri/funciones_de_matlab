function tps=serialCollectHexMeasurements(varargin)
delete(instrfind);
tic
tps=[];
if isempty(varargin)
    iterations=inf;
    printValues=true;
else
    iterations=varargin{1};
    printValues=false;
end

% % fclose(instrfind);delete(instrfind)

adcResolution=2^14;
RefLo=1.2;
RefHi=3.8;

serialPort=serialSetup();

for tramExecution=1:iterations
    [tp,os,gain,maximumVoltage,finalVoltage,initialVoltage]=collectTps(serialPort,RefHi,RefLo,adcResolution);
    if printValues
        disp(['Tp: ' num2str(tp) ' OS: ' num2str(os) ' K: ' num2str(gain) ' Vpeak: ' num2str(maximumVoltage) ' Vfin: ' num2str(finalVoltage) ' Vini: ' num2str(initialVoltage)] );
    else
        tps(end+1,:)=[tp,os,gain,maximumVoltage,finalVoltage,initialVoltage];
        disp(tramExecution);
    end
end

clock

toc

fclose(serialPort);

% [fileName,filePath]=uigetfile();

    function [tp,os,gain,maximumVoltage,finalVoltage,initialVoltage]=collectTps(serialPort,RefHi,RefLo,adcResolution,~)
        serialLine=getSerialLine(serialPort);
        if (~isempty(serialLine))
            %         [adcOSVoltage,tp, adcVOffset, pw]=getSerialLineData(serialLine);
            [maximumInt, tpInt, finalInt, initialInt, stepHigh, stepLow, BISTIterations]=getSerialLineData(serialLine);
            
            adcStepDelta=(RefHi-RefLo)/adcResolution;
            tp=tpInt/BISTIterations*5+2.5/2;
            maximumVoltage=ADC2Voltage(maximumInt,adcStepDelta,RefLo,BISTIterations);
            finalVoltage=ADC2Voltage(finalInt ,adcStepDelta,RefLo,BISTIterations);
            initialVoltage=ADC2Voltage(initialInt,adcStepDelta,RefLo,BISTIterations);
            os=(maximumVoltage-finalVoltage)/(finalVoltage-initialVoltage)*100;
            gain=(finalVoltage-initialVoltage)/(abs(stepHigh-stepLow)*2.6/64);
        end
    end

    function serialPort=serialSetup()
        serialPort = serial('COM3');
        set(serialPort,'DataBits',8);
        set(serialPort,'StopBits',1);
        set(serialPort,'BaudRate',115384);
        set(serialPort,'Parity','none');
        set(serialPort,'Terminator',13);
        set(serialPort,'ReadAsyncMode','continuous');
        set(serialPort,'Timeout',50);
        fopen(serialPort);
        disp(serialPort);
    end

    function serialLine=getSerialLine(serialPort)
        serialLine=fscanf(serialPort,'%x');
    end

    function [maximum, tp, final, initial, stepHigh, stepLow, iterations]=getSerialLineData(serialLine)
        tp=serialLine(1);
        maximum=serialLine(2);
        final=serialLine(3);
        initial=serialLine(4);
        stepHigh=serialLine(5);
        stepLow=serialLine(6);
        iterations=serialLine(7);
    end

    function voltage=ADC2Voltage(adcValue,adcDelta,adcMinimum,BISTIterations)
        voltage=adcValue*adcDelta/BISTIterations+adcMinimum;
    end
end

