function [statTables,paramVals,normTestTable]=generateStatisticsBISTFloat(varargin)
if ~isempty(varargin)
    address=varargin{1};
else
    address=fullfile(pwd,'*.mat');
end
files=dir(address);

tp=table;
os=table;
gain=table;

wp=table;
qp=table;

fc=table;
mr=table;

tpsAll=[];
ossAll=[];
ksAll=[];

wpsAll=[];
qpsAll=[];

fcsAll=[];
mrsAll=[];

filterNames={};
normTestTable=table;

for index=1:length(files)
    fileName1=files(index).name;
    folder=files(index).folder;
    data=importdata(fullfile(folder,fileName1));
    
    currentTps=data(:,1);
    currentOSs=data(:,2);
    currentKs=data(:,3);
    
    currentQps=data(:,4);
    currentWps=data(:,5);
        
%     currentQps=1./calculateDumpingFactorFromOS(currentOSs)./2;
%     currentWps=calculateNaturalFrequencyFromTpAndOS(currentTps/1e6,currentOSs);
    [currentFcs,currentMrs]=calculateFcAndMr(currentQps,currentWps);
    
    tpsAll=[tpsAll,currentTps];
    ossAll=[ossAll,currentOSs];
    ksAll=[ksAll,currentKs];
    
    wpsAll=[wpsAll,currentWps];
    qpsAll=[qpsAll,currentQps];
    
    fcsAll=[fcsAll,currentFcs];
    mrsAll=[mrsAll,currentMrs];
   
    filterName=fileName1(1:end-4);
    
    normTestTable.chi(['Tp ' filterName])=chi2gof(chori(tpsAll),'Alpha',5e-2);
    normTestTable.chi(['Os ' filterName])=chi2gof(chori(ossAll),'Alpha',5e-2);
    normTestTable.chi(['K ' filterName])=chi2gof(chori(ksAll),'Alpha',5e-2);
    normTestTable.chi(['Wp ' filterName])=chi2gof(chori(wpsAll),'Alpha',5e-2);
    normTestTable.chi(['Qp ' filterName])=chi2gof(chori(qpsAll),'Alpha',5e-2);
    normTestTable.chi(['Fc ' filterName])=chi2gof(chori(fcsAll),'Alpha',5e-2);
    normTestTable.chi(['Mr ' filterName])=chi2gof(chori(mrsAll),'Alpha',5e-2);
    
    normTestTable.ks(['Tp ' filterName])=kstest(chori(tpsAll),'Alpha',1e-10);
    normTestTable.ks(['Os ' filterName])=kstest(chori(ossAll),'Alpha',1e-10);
    normTestTable.ks(['K ' filterName])=kstest(chori(ksAll),'Alpha',1e-10);
    normTestTable.ks(['Wp ' filterName])=kstest(chori(wpsAll),'Alpha',1e-10);
    normTestTable.ks(['Qp ' filterName])=kstest(chori(qpsAll),'Alpha',1e-10);
    normTestTable.ks(['Fc ' filterName])=kstest(chori(fcsAll),'Alpha',1e-10);
    normTestTable.ks(['Mr ' filterName])=kstest(chori(mrsAll),'Alpha',1e-10);
    
    filterNames{end+1}=filterName;
    
    results=analyzeMeasurement(data);
    tp.max(filterName)=results.Tp('Max');
    tp.min(filterName)=results.Tp('Min');
    tp.mean(filterName)=results.Tp('Media');
    tp.deviation(filterName)=results.Tp('Desviacion');
        
    os.max(filterName)=results.OS('Max');
    os.min(filterName)=results.OS('Min');
    os.mean(filterName)=results.OS('Media');
    os.deviation(filterName)=results.OS('Desviacion');
    
    gain.max(filterName)=results.K('Max');
    gain.min(filterName)=results.K('Min');
    gain.mean(filterName)=results.K('Media');
    gain.deviation(filterName)=results.K('Desviacion');
    
    wp.max(filterName)=max(currentWps);
    wp.min(filterName)=min(currentWps);
    wp.mean(filterName)=mean(currentWps);
    wp.deviation(filterName)=std(currentWps);
    
    qp.max(filterName)=max(currentQps);
    qp.min(filterName)=min(currentQps);
    qp.mean(filterName)=mean(currentQps);
    qp.deviation(filterName)=std(currentQps);

    fc.max(filterName)=max(currentFcs);
    fc.min(filterName)=min(currentFcs);
    fc.mean(filterName)=mean(currentFcs);
    fc.deviation(filterName)=std(currentFcs);
    
    mr.max(filterName)=max(currentMrs);
    mr.min(filterName)=min(currentMrs);
    mr.mean(filterName)=mean(currentMrs);
    mr.deviation(filterName)=std(currentMrs);
end

normTestTable.chi('Tp')=chi2gof(chori(tpsAll),'Alpha',1e-10);
normTestTable.chi('Os')=chi2gof(chori(ossAll),'Alpha',1e-10);
normTestTable.chi('K')=chi2gof(chori(ksAll),'Alpha',1e-10);
normTestTable.chi('Wp')=chi2gof(chori(wpsAll),'Alpha',1e-10);
normTestTable.chi('Qp')=chi2gof(chori(qpsAll),'Alpha',1e-10);
normTestTable.chi('Fc')=chi2gof(chori(fcsAll),'Alpha',1e-10);
normTestTable.chi('Mr')=chi2gof(chori(mrsAll),'Alpha',1e-10);

normTestTable.ks('Tp')=kstest(chori(tpsAll),'Alpha',1e-10);
normTestTable.ks('Os')=kstest(chori(ossAll),'Alpha',1e-10);
normTestTable.ks('K')=kstest(chori(ksAll),'Alpha',1e-10);
normTestTable.ks('Wp')=kstest(chori(wpsAll),'Alpha',1e-10);
normTestTable.ks('Qp')=kstest(chori(qpsAll),'Alpha',1e-10);
normTestTable.ks('Fc')=kstest(chori(fcsAll),'Alpha',1e-10);
normTestTable.ks('Mr')=kstest(chori(mrsAll),'Alpha',1e-10);

statTables.tp=tp;
statTables.os=os;
statTables.K=gain;
statTables.wp=wp;
statTables.qp=qp;
statTables.fc=fc;
statTables.mr=mr;

paramVals.tp=tpsAll;
paramVals.os=ossAll;
paramVals.K=ksAll;
paramVals.wp=wpsAll;
paramVals.qp=qpsAll;
paramVals.fc=fcsAll;
paramVals.mr=mrsAll;

end

function results=analyzeMeasurement(varargin)
if isempty(varargin)
    file=uigetfile();
    measurement=importdata(file);
else
    measurement=varargin{1};
end

tps=measurement(:,1);
os=measurement(:,2);
gains=measurement(:,3);
results=table();
results.Tp=[std(tps);mean(tps);min(tps);max(tps)];
results.OS=[std(os);mean(os);min(os);max(os)];
results.K=[std(gains);mean(gains);min(gains);max(gains)];
results.Properties.RowNames={'Desviacion','Media','Min','Max'};
end

function chato=chori(vector)
chato=reshape(vector,[],1);
end
