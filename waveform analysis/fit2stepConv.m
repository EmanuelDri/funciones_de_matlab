function [left,right]=fit2stepConv(y)

inputArrayLength=length(y);
x = 1:inputArrayLength;


sigma = 3;
cutoff = ceil(4*sigma);
kernel = -cutoff:cutoff;
kernel = -kernel .* exp(-0.5 * kernel.^2 / sigma.^2);
grad = conv(y,kernel,'same');

errPer=inputArrayLength*1e-2;

[~,ii] = max(grad(errPer:end-errPer));
ii=ii+errPer;

px = x(ii-1:ii+1).';
py = grad(ii-1:ii+1).';
% solve the equation: py = [px.*px, px, ones(3,1)] * params;
params = [px.*px, px, ones(3,1)] \ py;
x_max = -params(2)/(2*params(1));

left = median(y(x<x_max));
right = median(y(x>x_max));

% plot(x,y)
% hold on
% plot([x(1),x_max,x_max,x(end)],[left,left,right,right])