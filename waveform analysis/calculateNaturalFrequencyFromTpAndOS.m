function wn=calculateNaturalFrequencyFromTpAndOS(tp,os)
dumping=calculateDumpingFactorFromOS(os);
wn=pi./tp./sqrt(1-dumping.^2);
end