    function cleanSignal=averageAndDenoiseSignals(signals,varargin)
    if nargin>2
        samplesNumber=varargin{1};
        cleanSignal = movmean(mean(cell2mat(signals)),samplesNumber);
    else
        cleanSignal = movmean(mean(cell2mat(signals)),100);
    end