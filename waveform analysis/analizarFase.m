function [faseOsc,faseorcad]=analizarFase(time,oscIn,oscOut,orcadIn,orcadOut,frecuencia)
    [~,subidasOrcadSalida,~]=risetime(orcadOut,time);
    [~,subidasOrcadEntrada,~]=risetime(orcadIn,time);
    [~,subidasOscSalida,~]=risetime(oscOut,time);
    [~,subidasOscEntrada,~]=risetime(oscIn,time);
    
    faseorcad=0;
    faseOsc=0;
    if isempty(intersect(subidasOrcadEntrada,subidasOrcadSalida))
        subidasOrcadSalida(subidasOrcadSalida<min(subidasOrcadEntrada)*.98)=[];
        subidasOrcadEntrada(subidasOrcadEntrada>max(subidasOrcadSalida)*1.02)=[];
        subidasOrcadEntrada(subidasOrcadEntrada<unique(min(subidasOrcadSalida)-mean(diff(subidasOrcadEntrada)*9/10)))=[];
        faseorcad=mean(subidasOrcadSalida-subidasOrcadEntrada)*360*frecuencia;
    end
    
    if isempty(intersect(subidasOscEntrada,subidasOscSalida))
        subidasOscSalida(subidasOscSalida<min(subidasOscEntrada)*.98)=[];
        subidasOscEntrada(subidasOscEntrada>max(subidasOscSalida)*1.02)=[];
        subidasOscEntrada(subidasOscEntrada<unique(min(subidasOscSalida)-mean(diff(subidasOscEntrada)*9/10)))=[];
        faseOsc=mean(subidasOscSalida-subidasOscEntrada)*360*frecuencia;
    end
    