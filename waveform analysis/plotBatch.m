function figHandler=plotBatch(measBatch)
[measNumber,~]=size(measBatch);
for index=1:measNumber
    x=measBatch{index,1};
    y=measBatch{index,2};
    figHandler=plot(x*1e6,y);hold on;
end