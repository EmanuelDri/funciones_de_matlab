function settlingTimeIndex=getSettlingTime(y,settlingTimeThreshold)
stepInformation=stepinfo(y,'SettlingTimeThreshold',settlingTimeThreshold); %calculamos las caracter�sticas de la respuesta escal�n del patr�n/promedio
settlingTimeIndex=floor(stepInformation.SettlingTime); %obtenemos el tiempo de establecimiento del patr�n
end