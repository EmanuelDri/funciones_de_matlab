function plotFileAverage(varargin)
if nargin==0
    [file,folderPath]=uigetfile;
    searchString=fullfile(folderPath,file);
else
    searchString=varargin{1};
end

holdOn=false;
if length(varargin)>1&&contains(varargin{2},'hold on')
    holdOn=true;
end

if ~isempty(searchString)
    files=listFiles(searchString);
    plotFileList(files,holdOn);
end
    function plotFileList(fileStructures,holdOn)
        for index=1:length(fileStructures)
            if length(fileStructures)>1&&~holdOn, figure; end
            fileStruct=fileStructures(index);
            address=fullfile(fileStruct.folder,fileStruct.name);
            resultados=importdata(address);
            x=mean(cell2mat(resultados(:,1)));
            y=mean(cell2mat(resultados(:,2)));
            plot(x,y),hold on;
            title(fileStruct.name(1:end-3));
        end
    end
end