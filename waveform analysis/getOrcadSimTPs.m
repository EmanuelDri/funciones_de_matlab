function tps=getOrcadSimTPs(orcadSim,varargin)
%%
% 
%  orcadSim must be a table containing 3 or more columns, one of them
%  called Time must contain the time references for each sample, the
%  columns corresponding to the output of the simulated circuit must
%  contain on any part of its name the string 'out' the remaining one will
%  be considered as the circuit input
%  
% 

continousTimeFilter=false;
phi2Correction=false;
if~isempty(varargin)
    characterInputs=find(cellfun(@ischar,varargin));
    for index=characterInputs
        switch varargin{index}
            case 'SallenKey'
                continousTimeFilter=true;
            case 'PSoC'
                continousTimeFilter=false;
            case 'phi2Correction'
                phi2Correction=varargin{index+1};
        end
    end
end

[time,timeIndex]=getTableColumnsByName(orcadSim,'time',0);
if isempty(timeIndex)
    time=orcadSim.(1);
    timeIndex=1;
end
[vout,voutIndexes]=getTableColumnsByName(orcadSim,'out',0);
if isempty(voutIndexes)
    vout=orcadSim(:,3:end);
    [~,columns]=size(orcadSim);
    voutIndexes=3:columns;
end
    
[~,columnsNumber]=size(orcadSim);
stepIndex=find(~(ismember(1:columnsNumber,timeIndex)|ismember(1:columnsNumber,voutIndexes)));
stepInput=orcadSim.(stepIndex);

% stepAmplitude=max(stepInput)-min(stepInput);
stepAmplitude=abs(stepInput(end)-stepInput(1));
[~,~,ut]=slewrate(stepInput);
stepInstant=time(floor(ut(1)))+2.5e-6*phi2Correction*~continousTimeFilter;

simulationsNumber=length(voutIndexes);

tps=cell(simulationsNumber,3);

for index=1:length(voutIndexes)
    if istable(vout)
        filterResponse=vout.(index);
    else
        filterResponse=vout(:,index);
    end
    
    if ~continousTimeFilter
        filterResponseTp=movmean(filterResponse,700);
        filterResponseOS=movmean(filterResponse,100);
    else
        filterResponseTp=filterResponse;
        filterResponseOS=filterResponse;
    end
    vpeak=max(filterResponseOS);
    vfinal=(filterResponseOS(end));
    vini=filterResponseOS(1);
    
    os=calculateOSFromVoltages(vpeak,vfinal,vini);
    tp=mean(time(filterResponseTp==max(filterResponseTp)))-stepInstant;
    gain=(vfinal-vini)/stepAmplitude;
    
    tps(index,:)={tp,os,gain};
end
    