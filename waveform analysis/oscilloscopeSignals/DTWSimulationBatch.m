classdef DTWSimulationBatch < matlab.mixin.Copyable
    properties (Access=protected)
        time;
        inputStep;
        simsUnderTest;
    end
    methods
        %% Constructor
        function obj=DTWSimulationBatch(simulationStructure,varargin)
            obj.time=simulationStructure(22:end,1);
            obj.time=obj.time.(1);
            obj.inputStep=simulationStructure(22:end,2);
            obj.inputStep=obj.inputStep.(1);
            obj.simsUnderTest=simulationStructure(22:end,3:end);
            obj.preprocessToDetectFaults(varargin{:});
        end
    end
    %% getters and setters
    methods
        function time=getTime(obj)
            time=obj.time;
        end
        function inputStep=getInputStep(obj)
            inputStep=obj.inputStep;
        end
        function simsResponses=getSimsResponses(obj)
            simsResponses=obj.simsUnderTest;
        end
        function simsNumber=getSimsNumber(obj)
            simsNumber=width(obj.simsUnderTest);
        end
        function [faultFreeSim,time,inputStep]=getFaultFreeSimulation(obj)
            faultFreeIndex=obj.getFaultFreeIndex();
            faultFreeSim=obj.simsUnderTest.(faultFreeIndex);
            time=obj.getTime();
            inputStep=obj.getInputStep();
        end
    end
    
    methods (Access=private)
        function obj=preprocessToDetectFaults(obj,varargin)
            [scale,amplitudeCorrection,downSampleFreq]=getVararginParams({'Scale',true,'AmplitudeCorrection',1,'Downsample',[]},varargin{:});
            
            simulations=obj.simsUnderTest;
            simInputStep=obj.inputStep;
            simulationsNumber=width(simulations);
            Time=obj.getTime();
            
            if scale
                amplitudeFactor=obj.getInputStepAmplitude(amplitudeCorrection);
                lt=BistateAnalyzer.getStepTime(simInputStep)-1;
            else
                amplitudeFactor=1*amplitudeCorrection;
            end
            
            for simIndex=1:simulationsNumber
                vvout=simulations.(simIndex);
                if scale
                    vvout=vvout-vvout(floor(lt));
                end
                if ~isempty(downSampleFreq)
                    vvout=WaveFormAnalysis.downsampleSignal(Time,vvout,downSampleFreq);
                end
                simulations.(simIndex)=vvout/amplitudeFactor;
            end
            obj.simsUnderTest=simulations;
        end
        function simIndex=getFaultFreeIndex(obj)
            simIndex=median(1:obj.getSimsNumber());
        end
        function inputStepAmp=getInputStepAmplitude(obj,amplitudeCorrection)
            inputStepAmp=abs(obj.inputStep(end)-obj.inputStep(1))*amplitudeCorrection;
        end
    end
end