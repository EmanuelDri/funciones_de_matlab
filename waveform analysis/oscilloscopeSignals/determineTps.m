function tps=determineTps(measurements)
tpMin=inf; tpMax=-inf;
osMin=inf; osMax=-inf;
gainMin=inf; gainMax=-inf;
wnMin=inf; wnMax=-inf;
qpMin=inf; qpMax=-inf;
tpMean=0; osMean=0; gainMean=0; qpMean=0; wnMean=0;

for index=1:length(measurements)
    xout=measurements{index,1}; yout=measurements{index,2};
    xin=measurements{index,3}; yin=measurements{index,4};
    youtClean=cleanSignal(yout);
    
    [inputIni,inputFinal]=getInputSignalStateLevels(movmean(yin,20));
    [outputIni,outputFinal]=getOutputSignalStateLevels(movmean(yout,50));
    offset=determineOutputSignalOffset(youtClean);
    stepData=stepinfo(youtClean-offset,xout);
    
    gain=(outputFinal-outputIni)/(inputFinal-inputIni);
    tp=stepData.PeakTime;
    os=stepData.Overshoot;
    dumping=calculateDumpingFactorFromOS(os);
    qp=1/2/dumping;
    wn=calculateNaturalFrequencyFromTpAndOS(tp,os);
    [tpMin,tpMax]=computeExtremes(tp,tpMin,tpMax);
    tpMean=tpMean+tp;
    [osMin,osMax]=computeExtremes(os,osMin,osMax);
    osMean=osMean+os;
    [gainMin,gainMax]=computeExtremes(gain,gainMin,gainMax); 
    gainMean=gain+gainMean;
    [qpMin,qpMax]=computeExtremes(qp,qpMin,qpMax);
    qpMean=qpMean+qp;
    [wnMin,wnMax]=computeExtremes(wn,wnMin,wnMax);
    wnMean=wnMean+wn;
    
end

tpMean=tpMean/length(measurements);
osMean=osMean/length(measurements);
gainMean=gainMean/length(measurements);
qpMean=qpMean/length(measurements);
wnMean=wnMean/length(measurements);


tps=table([tpMin; tpMax;tpMean],[osMin ;osMax;osMean],[gainMin; gainMax;gainMean],[qpMin; qpMax;qpMean],[wnMin; wnMax;wnMean],'RowNames',{'min';'max';'mean'},'VariableNames',{'Tp';'OS';'K';'Qp';'Wn'});

    function [minimum,maximum]=computeExtremes(parameter,minimum,maximum)
        if parameter<minimum
            minimum=parameter;
        elseif parameter>maximum
            maximum=parameter;
        end
    end

    function processedSignal=cleanSignal(signal)
        processedSignal = movmean(signal,17);
    end

    function [ini,final]=getInputSignalStateLevels(stepSignal)
        [ini,final]=getBistateLevels(stepSignal,'Histogram mean',399);
%         final=stepSignal(1);ini=stepSignal(end);
    end

    function [ini,final]=getOutputSignalStateLevels(signal)
        [ini,final]=getBistateLevels(signal,'Histogram mode',399);
    end

    function offset=determineOutputSignalOffset(yprom)
        % yprom = averageAndDenoiseSignals(results(:,2));
        [offset,~]=getBistateLevels(yprom(1:end),'Histogram mode',399);
    end

end