classdef DTWRealMeasUnderTest < DTWFilterUnderTest
    methods
        function obj=DTWRealMeasUnderTest(varargin)
            obj@DTWFilterUnderTest(varargin{:});
        end
        function data=detectFaults(obj,maxDTWDistance,pattern,dtwWindow,movmeanWindow,varargin)
            measurements=obj.filterData;
           
            [measNumber,~]=size(measurements);
            data=zeros(measNumber,3);
            
            patternWaveForm=pattern{2};
            
            for measIndex=1:measNumber
                measurement=measurements(measIndex,:);
                measY=measurement{2};
                if length(measY)>=length(patternWaveForm)
                    measY=measY(1:length(patternWaveForm));
                end
                
                cleanMeas=WaveFormAnalysis.movmean(measY,movmeanWindow);
                
                [detected,dist,wp1,wp2]=obj.testFaultDetection(patternWaveForm,cleanMeas,maxDTWDistance,dtwWindow);
                data(measIndex,:)=[detected dist errorR(dist,maxDTWDistance)];
            end
            
        end
    end
end