classdef DTWSimFilterUnderTest < DTWFilterUnderTest
    methods
        function obj=DTWSimFilterUnderTest(simulationsStruct,varargin)
            %superclass constructor
            obj@DTWFilterUnderTest(simulationsStruct,varargin{:});
            %subclass constructor
        end
        
        function data=detectFaults(obj,maxDTWDistance,patternWaveForm,window,movmeanWindow,varargin)
            [scaled,downSample]=getVararginParams({'Scaled',true,'DownSample',true},varargin{:});
            Time=obj.filterData.getTime();
            simulations=obj.filterData.getSimsResponses();
            testFaultDetection=@obj.testFaultDetection;
            
            simulationsNumber=width(simulations);
            signalVoltages=patternWaveForm{2};
            
            fs=WaveFormAnalysis.getSampleFrequency(patternWaveForm{1}); %Determinar la frecuencia de muestreo de la captura de osciloscopio
            %% consigna de Eduardo
            simInputStep=obj.filterData.getInputStep();
            %% scale waveforms
            if ~scaled
                simIndex=obj.getFaultFreeIndex(simulationsNumber);
                faultFreeSim=simulations.(simIndex);
                faultFreeAmp=max(faultFreeSim)-faultFreeSim(1);
                patternOffset=signalVoltages(1);
                patternAmplitude=max(signalVoltages)-patternOffset;
            else
                faultFreeAmp=1;
                patternOffset=0;
                patternAmplitude=1;
            end
            
            data=zeros(simulationsNumber,3);
            for simIndex=1:simulationsNumber
                vvout=simulations.(simIndex);
                
                vvout=vvout/faultFreeAmp*patternAmplitude+patternOffset;
                
                ych1=signalVoltages;
                if downSample
                    resampledVoltage=WaveFormAnalysis.downsampleSignal(Time,vvout,fs);
                    resampledInput=WaveFormAnalysis.downsampleSignal(Time,simInputStep,fs);%% consigna de Eduardo
                else
                    resampledVoltage=vvout;
                    resampledInput=simInputStep;
                end
                simOutAV=WaveFormAnalysis.movmean(resampledVoltage,movmeanWindow);
                lt=BistateAnalyzer.getStepTime(resampledInput)+1;
                simOutAV=simOutAV(lt:min(length(simOutAV),length(ych1)+lt-1)); %consigna de Eduardo
%                 [ych1,simOutAV]=WaveFormAnalysis.alignSignals(ych1,simOutAV,'TimeAlignment','dtw');
                
                [detected,dist,wp1,wp2]=testFaultDetection(ych1,simOutAV,maxDTWDistance,window);
                deviation=simIndex-median(1:simulationsNumber);
                data(simIndex,:)=[deviation detected errorR(dist,maxDTWDistance)];
            end
        end
    end
    
    methods (Access=private,Static)
        %% debug functions, not for production
        
        function simIndex=getFaultFreeIndex(simulationsNumber)
            simIndex=median(1:simulationsNumber);
        end
        
        function generateDebugFigure(ych1,simOutAV,xch1)
            
            figure,hold on;
            plot(xch1*1e6,ych1,'LineWidth',6);
            plot(xch1*1e6,simOutAV,'LineWidth',3);
            legend({'Forma de onda patrón','Simulación'},'Location','best');
            ylabel 'Tensión [V]';
            xlabel 'Tiempo [\muS]';
            nombre=input('ingresar Nombre del circuito: ','s');
            title(nombre);
%             savefig(fullfile('Figures for debug',['simVsPat ' nombre]));
        end
        
        
    end
end
