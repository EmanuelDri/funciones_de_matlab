classdef DTWFilterUnderTest < matlab.mixin.Copyable
    properties (Access=protected)
        distanceFunction;
        filterData;
    end
    methods
        function obj=DTWFilterUnderTest(filterData,distanceFunction)
            obj.distanceFunction=@WaveFormAnalysis.dtwDist;
            switch nargin
                case 0
                    obj.filterData=[];
                case 1
                    obj.filterData=filterData;
                case 2
                    if ~ischar(distanceFunction)
                        obj.distanceFunction=distanceFunction;
                    else
                        obj.distanceFunction=obj.getDistanceFunction(distanceFunction);
                    end
            end
        end
    end
    
    methods (Abstract)
        data=detectFaults(obj,maxDTWDistance,pattern,dtwWindow,movmeanWindow,varargin)
    end
    
    methods (Access=private)
        function distanceFunction=getDistanceFunction(distanceFunName)
            switch distanceFunName
                case 'EuclideanDistance'
                    distanceFunction=@WaveFormAnalysis.euclideanDist;
                otherwise
                    distanceFunction=@WaveFormAnalysis.dtwDist;
            end
            
        end
    end
    
    methods (Access=protected)
        function [detected,dist,wp1,wp2]=testFaultDetection(obj,pattern,signalUnderTest,maxDistance,varargin)
            detected=1;
            [dist,wp1,wp2]=obj.distanceFunction(pattern,signalUnderTest,varargin{:});
            %             figure(1),plot(xch1,ych1,measTime(1:end),cleanMeas(1:end))
            if dist<=maxDistance
                detected=0;
            end
        end
    end
end