classdef DTWMeasurement < OscilloscopeMeasurement
    properties 
    end
    methods
        function obj=DTWMeasurement(batch,varargin)
            %superclass constructor
            obj@OscilloscopeMeasurement(batch);
            %subclass constructor
            obj.preprocessToDetecFaults(varargin{:});
        end
    end
    methods (Access=protected)
        function obj=preprocessToDetecFaults(obj,varargin)
            %% Description
            %%%
            %%%  Optional parammeters:
            %%% * Scale (boolean): determines whether or not to scale the filter
            %%% responses according to the input signal amplitude. Default (false)
            %%% * SettlingTimeThreshold (float): this is the settling time percentage desired for
            %%% the singnals. All posterior points are removed. Default (0.1%)
            %%% * RemoveInitialState (boolean): determines whether or not to remove the initial
            %%% state of the filter responses. Default (true)
            %%% * Euclidean (boolean): if true it forces the algorithm to use euclidean
            %%% distance function instead of DTW distance function
            %%%
            
            %% settle the optional parammeters
            [scale,removeInitialState,performFit2Step]=getVararginParams({'Scale',true,'RemoveInitialState',true,'fit2Step',false},varargin{:});
            
            obj.resyncMeas();
            %% scale the filter responses if requested by its corresponding input parammeter
            if scale
                obj.scaleAndRemoveOffset('fit2Step',performFit2Step);
            end
            if removeInitialState
                obj.removeInitialState(); %remove the initial and final states from the filter output signals
            end
            obj.resyncMeas();
%             obj.internalWritePattern();
        end
        
        function obj=resyncMeas(obj)
            measurements=obj.batch;
            %% sync all waveforms from the measurements batch
            times=measurements(:,3);
            startPos=@(array) array(1);
            endPos=@(array) array(end);
            syncStartTime=max(cellfun(startPos, times));
            syncEndTime=min(cellfun(endPos, times));
            for measIndex=1:length(measurements)
                %% disgreggate the filter I/O signal vectors from each measurement
                measOutTime=measurements{measIndex,1}; %get the filter response time vector
                measOutV=measurements{measIndex,2}; %get the filter response voltages vector
                measInTime=measurements{measIndex,3}; %get the input signal time vector
                measInV=measurements{measIndex,4}; %get the input signal voltages vector
                %% Resync all signals
                measOutV=measOutV(measOutTime>=syncStartTime&measOutTime<=syncEndTime);
                measOutTime=measOutTime(measOutTime>=syncStartTime&measOutTime<=syncEndTime);
                measInV=measInV(measInTime>=syncStartTime&measInTime<=syncEndTime);
                measInTime=measInTime(measInTime>=syncStartTime&measInTime<=syncEndTime);
                measurements(measIndex,:)={measOutTime,measOutV,measInTime,measInV}; %update the signals collection with the current sample resynced
            end
            %%% determine the minimum length signal and the others to match its length
            minLength=min(min(cellfun(@length,measurements)));
            for measIndex=1:length(measurements) %for each measurement
                for colIndex=1:4 %for each array from a measurement
                    currentElement=measurements{measIndex,colIndex};
                    currentElement=currentElement(1:minLength); %remove final elements to match the size of the shortest measurement
                    measurements{measIndex,colIndex}=currentElement; %update the samples collection
                end
            end
            obj.batch=measurements;
        end
    end
end