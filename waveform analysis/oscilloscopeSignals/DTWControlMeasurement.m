classdef DTWControlMeasurement < DTWMeasurement
    methods
        function obj=DTWControlMeasurement(batch,varargin)
            obj@DTWMeasurement(batch,varargin{:});
            obj.internalAverageBatch();
        end
    end
end