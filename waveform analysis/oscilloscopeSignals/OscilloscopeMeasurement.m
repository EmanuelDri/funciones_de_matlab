classdef OscilloscopeMeasurement < matlab.mixin.Copyable %this means that the object
    %member of the handle class and copiable, to generate a copy use copy()
    %otherwise it will copy a pointer
    
    %%%this class will be splitted into a super class and a daughter class
    %%%with the corresponding methods and attributes to fault detection
    properties (Access=protected)
        batch;
    end
    methods
        %% constructor
        function obj=OscilloscopeMeasurement(batch)
            if nargin>0
                obj.batch=batch;
            else
                obj.batch=[];
            end
        end
        %% getters
        function batch=getBatch(obj)
            batch=obj.batch;
        end
        
        %% setters
        function obj=setBatch(obj,batch)
            obj.batch=batch;
        end
        %% internal state manipulation
        function obj=internalAverageBatch(obj)
            obj.batch=obj.averageMeasBatch();
        end
        
        %% processing methods
        function measAV=averageMeasBatch(obj)
            meas=obj.batch;
            xout=mean(cell2mat(meas(:,1)));
            yout=mean(cell2mat(meas(:,2)));
            xin=mean(cell2mat(meas(:,3)));
            yin=mean(cell2mat(meas(:,4)));
            
            measAV={xout,yout,xin,yin};
        end
        
        function obj = scaleAndRemoveOffset(obj,varargin)
            performFit2Step=getVararginParams({'fit2Step',false},varargin{:});
            signals=obj.batch;
            %SCALEANDREMOVEOFFSET Summary of this function goes here
            %   Detailed explanation goes here
            [nroMuestras,~]=size(signals);
            
            responses=signals(:,2); %filter responses
            inputs=signals(:,4);    %input signals
            
            parfor index=1:nroMuestras %we use paralellism to reduce computing time
                y=responses{index};
                inputStep=inputs{index};
                
                yAV=WaveFormAnalysis.movmean(y,100);
                if performFit2Step
                    inputStepAmplitude=getInputStepAmplitude(inputStep,'fit2Step',true);
                    offset=getSignalStateLevels(inputStep,yAV,'fit2Step',true); %get the response's offset
                else
                    inputStepAmplitude=getInputStepAmplitude(inputStep);
                    offset=getSignalStateLevels(yAV); %get the response's offset
                end
                signals{index,2}=(y-offset)/inputStepAmplitude;%(final-ini); %here we remove the offset and after that scale the signal according to its input step
            end
            obj.batch=signals;
        end
        
        function obj=removeInitialState(obj)
            obj=obj.removeStationaryStates(0,true);
        end
        
        function obj=removeFinalState(obj,settlingTimeThreshold)
            obj=obj.removeStationaryStates(settlingTimeThreshold,false);
        end
        
    end
    
    methods (Access=private)
        function obj=removeStationaryStates(obj,settlingTimeThreshold,removeInitialState)
            
            [xoutMat,youtMat,xinMat,yinMat]=batchToArrays(obj.batch);
            
            promedio=mean(youtMat,1);
            meanStepInput=mean(yinMat,1); %obtenemos la forma de onda escalón medio
            meanStepTime=mean(xinMat,1); %obtenemos el tiempo medio de las señales
            
            if settlingTimeThreshold==0
                settlingTimeIndex=length(promedio);
            else
                settlingTimeIndex=BistateAnalyzer.getSettlingTime(promedio,settlingTimeThreshold);
            end
            %% recorte de señales promedio y patron
            if removeInitialState
                [~,~,~,stepTimeIndex]=BistateAnalyzer.removeInitalState(meanStepTime,promedio,meanStepInput);
            else
                stepTimeIndex=1;
            end
            
            %% remove final and initial states
            xoutMat=xoutMat(:,stepTimeIndex:settlingTimeIndex);
            youtMat=youtMat(:,stepTimeIndex:settlingTimeIndex);
            xinMat=xinMat(:,stepTimeIndex:settlingTimeIndex);
            yinMat=yinMat(:,stepTimeIndex:settlingTimeIndex);
            
            obj.batch=arraysToBach(xoutMat,youtMat,xinMat,yinMat);
        end
        
    end
end
%% private functions
function [x1,y1,x2,y2]=batchToArrays(cellBatch)
x1=cell2mat(cellBatch(:,1));
y1=cell2mat(cellBatch(:,2));
x2=cell2mat(cellBatch(:,3));
y2=cell2mat(cellBatch(:,4));
end

function cellBatch=arraysToBach(x1,y1,x2,y2)
x1=num2cell(x1,2);
y1=num2cell(y1,2);
x2=num2cell(x2,2);
y2=num2cell(y2,2);
cellBatch=[x1,y1,x2,y2];
end

function inputStepAmplitude=getInputStepAmplitude(y,varargin)
inputStepAV=WaveFormAnalysis.movmean(y,10); %average all input step stimuli waveforms and denoise the resulting waveform using moving average
[ini,final]=getSignalStateLevels(inputStepAV,varargin{:}); %here we get the input signal statelevels
inputStepAmplitude=abs(final-ini); %determine the input step amplitude
end

function [ini,final]=getSignalStateLevels(stepSignal,varargin)
[performFit2Step,inputSignal]=getVararginParams({'fit2Step',false,'inputSignal',[]},varargin{:});
if performFit2Step
    fittedData=fit2step(stepSignal,inputSignal);
    ini=fittedData(1);
    final=fittedData(end);
else
    [ini,final]=BistateAnalyzer.getBistateLevels(stepSignal,'Histogram mode',520);
end
end