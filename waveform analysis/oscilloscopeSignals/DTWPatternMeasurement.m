classdef DTWPatternMeasurement < DTWMeasurement
    properties (Access=protected)
        intermediateMeas;
        threshold;
        patternWaveForm;
    end
    methods
        function obj=DTWPatternMeasurement(batch,varargin)
            %superclass constructor
            obj@DTWMeasurement(batch,varargin{:});
            %subclass constructor
%             obj.preprocessToDetecFaults(varargin{:});
            obj.intermediateMeas=obj.batch;
            obj.threshold=[];
            obj.patternWaveForm=[];
        end
        
        function threshold=getThreshold(obj)
            threshold=obj.threshold;
        end
        
        function obj=generatePattern(obj,settlingTimeThres)
            obj.batch=obj.intermediateMeas;
            obj.removeFinalState(settlingTimeThres);
            obj.internalWritePattern();
        end
        
        function patternWaveForm=getPattern(obj)
            if isempty(obj.patternWaveForm)
                obj.internalWritePattern();
            end
            patternWaveForm=obj.patternWaveForm;
        end
               
        function obj=generateThreshold(obj,window,varargin)
            promedio=obj.patternWaveForm{2};
            patternWaveForms=obj.batch;
            distanceFunction=@WaveFormAnalysis.dtwDist;
            if ~isempty(varargin)&&contains(varargin{1},'EuclideanDistance')
                distanceFunction=@WaveFormAnalysis.euclideanDist;
            end
            distances=zeros(1,length(patternWaveForms));
            elementsNumber=length(distances);
            for index=1:elementsNumber
                element=patternWaveForms{index,2};
                dist=distanceFunction(promedio,element,window);
                distances(index)=dist;
            end
            obj.threshold=max(max(distances));
            
        end
        
        function setPattternWvFrm(obj,patternWaveForm)
            obj.patternWaveForm=patternWaveForm;
        end
        
    end
    methods (Access=private)
        function obj=internalWritePattern(obj)
            measAV=obj.averageMeasBatch();
            obj.patternWaveForm=measAV(1:2);
        end
        
    end
end