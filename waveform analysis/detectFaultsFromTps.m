function summary=detectFaultsFromTps(measurements, simulationFiles,varargin)
%%
%  *measurements* must be a cell array containing the structure used on other
%  places of this libray nx4, where n is the number of measuremst, 
%  expressed as tuples of {outputTimeVector,outputVoltagesVector,...
% ...inputTimeVector,inputVoltagesVector}
%
% *simulationFiles* must be a structure coming from dir, containing the
% addresses of the simulations to be evaluated
% 
% the function admits an additional parameter called filter type, where
% 'SallenKey' resembles a sallen key filter, whereas 'PSoC' resembles a
% psoc1 filter
%

if ~isempty(varargin), filterType=varargin{1};  else, filterType=[]; end

results=getWindowedCampaignStatisticsSingleCapture(measurements);
tps=cell2mat(results(2:end,:));

tpArray=tps(:,2);
osArray=tps(:,1);
gainArray=tps(:,3);

maxTp=max(tpArray);
minTp=min(tpArray);

maxOS=max(osArray);
minOS=min(osArray);

maxGain=max(gainArray);
minGain=min(gainArray);

summary={'Tp','OS%','Gain','File Name'};

for fileIndex=1:length(simulationFiles)
    fileName=simulationFiles(fileIndex).name;
    fileFolder=simulationFiles(fileIndex).folder;
    fileAddress=fullfile(fileFolder,fileName);
    
    simulation=importdata(fileAddress);
    simTps=getOrcadSimTPs(simulation,varargin{:});
    
    [numberOfRuns,~]=size(simTps);
    
    for runIndex=1:numberOfRuns
        tp=simTps{runIndex,1};
        os=simTps{runIndex,2};
        gain=simTps{runIndex,3};
        
        tpFault=false;
        osFault=false;
        gainFault=false;
        if ischar(filterType)&&contains(filterType,'PSoC')
            if tp>maxTp+2.5e-6||tp<minTp-2.5e-6
                tpFault=true;
            end
        else
            if tp>maxTp||tp<minTp
                tpFault=true;
            end
        end
        if os>maxOS||os<minOS
            osFault=true;
        end
        if gain>maxGain||gain<minGain
            gainFault=true;
        end
        summary=[summary;{tpFault,osFault,gainFault,fileName}];
    end
end

disp(summary);