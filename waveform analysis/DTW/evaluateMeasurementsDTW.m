function summary=evaluateMeasurementsDTW(thresholdFile,patternWaveFormsFile,measurementsFolder,smoothingFactor)
tic
maxDTWDistance=importdata(thresholdFile);
allMeasurements=importdata(patternWaveFormsFile);

cd(measurementsFolder);
summary={};
folderNames=listFolders;
for folderIndex=1:folderNames
    cd(folderNames(folderIndex));
    files=dir('*.mat');
    if ~isempty(files)
        for fileIndex=length(files)
            fileName=files(fileIndex).name;
            testSignals=importdata(fileName);
            detectionsNumber=performDetections(testSignals,allMeasurements,maxDTWDistance,smoothingFactor);
            summary(end+1,:)={detectionsNumber;[folderName ':' fileName]};
        end
    end
    cd ..;
end
cd ..;

toc
end

function detectionsNumber=performDetections(testSignals,allMeasurements,maxDTWDistance,smoothingFactor)
detections=[];
parfor acum=1:length(testSignals)
    testSignal=testSignals{acum,2};
    tamano=length(allMeasurements);
    detected=1;
    index1=1;
    while index1<=tamano&&detected
        y=allMeasurements{index1,2};
        if smoothingFactor~=1
            t=allMeasurements{index1,1};
            y=generateSpline(t,y,smoothingFactor);
        end
        [dist,path,~]=dtw(y,testSignal);
        dist=dist/length(path);
        index1=index1+1;
        if dist<maxDTWDistance
            detected=0;
        end
    end
    detections(acum)=detected;
end
detectionsNumber=sum(detections);
end

function folderNames=listFolders
folderElements=dir('*'); folderElements([1 2])=[];
folderElements=struct2cell(folderElements);folders=find(cell2mat(folderElements(5,:)));
if isempty(folders)
    folderNames={pwd};
else
    folderNames=folderElements(1,folders);
end
end