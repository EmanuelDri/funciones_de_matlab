function data=evaluateOrcadSimFilterTPs(simsFile,desviaciones,tps,timeCorrection)

simulations=importdata(simsFile);

Time=simulations.(1)+timeCorrection;
data=[];
Time=Time(22:end);
simulationsNumber=width(simulations);
inputStep=simulations.(2); inputStep=inputStep(22:end);
inputAmplitude=max(inputStep)-min(inputStep);
for simIndex=3:simulationsNumber
    vvout=simulations.(simIndex); vvout=vvout(22:end); 
    vvout=(vvout-min(vvout))/1.01;%min(vvout);
    stepData=stepinfo(vvout,Time);
    tp=stepData.PeakTime;
    os=stepData.Overshoot;
    gain=vvout(end)/inputAmplitude;
    [detected,tpDetection,osDetection,gainDetection]=computeDetection(tps,tp,os,gain);
    data(simIndex-2,:)=[desviaciones(simIndex-2) detected tpDetection tp osDetection os gainDetection gain];
end
end

function [detection,tpDetection,osDetection,gainDetection]=computeDetection(tps,tp,os,gain)
        tpDetection=tp>tps.Tp('max')+5e-6|| tp<tps.Tp('min')-5e-6;
        osDetection=os>tps.OS('max')|| os<tps.OS('min');
        gainDetection=gain>tps.K('max')|| gain<tps.K('min');
        detection=tpDetection||osDetection||gainDetection;
end