function efficiency=dtwFaulDetectionEfficiency(summary)
[casesNumber,collumnsNumber]=size(summary(2:end,:));
variablesNumber=collumnsNumber/3;
summaryResults=cell2mat(summary(2:end,(1:variablesNumber)*3-1));
if isempty(find(summaryResults(round(median(1:casesNumber)),:), 1))
    efficiency=sum(summaryResults,'All')/(casesNumber-1)/variablesNumber*100;
else
    efficiency=0;
end
end
