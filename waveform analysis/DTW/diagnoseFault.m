function varargout=diagnoseFault(simUnderTest,dtwWindow,movmeanWindow,patterns,varargin)
%%
%
%  This algorithm would need the definition of the structure that our
%  patterns file should have in order to make this operative.
%  simUnderTest is the simulationFile address, dtwWindow is the window used
%  on DTW, movmeanWindow is the number of samples previously used to
%  generate the patters by the movmean function, pattersFile is the file
%  address of the patterns previously defined
%
%
cropStep=true;
if ~isempty(varargin)&&contains(varargin{1},'RemoveInitialState')
    cropStep=varargin{2};
end
distances=zeros(length(patterns),1);

parfor fileIndex=1:length(patterns)
    %get the pattern to be used on the current comparison
    patternY=patterns{fileIndex,2};patternX=patterns{fileIndex,1};
%     patternY=movmeanNew(patternY,movmeanWindow);
%     patternY=removeOffset(patternY);
    fs=1/mean(diff(patternX));
    
    [simX,simY]=donwsampleAndCrop(simUnderTest,fs,movmeanWindow,cropStep);
    [patternY,patternX,simY,simX]=alignSignals(patternY,simY,patternX,simX);
    plot(patternX,patternY,simX,simY);
%       simY=trimSimulation(simX,simY,patternX,patternY);
    dist=dtwDistanceFunction(simY,patternY,dtwWindow);
    distances(fileIndex,1)=dist;
end

[~,fileIndexes]=sort(distances);

outputArgs=1;
outputDiagIndex=1;
accumulatedDistances=sum(distances);
while(outputArgs<=nargout)
    diagnosisIndex=fileIndexes(outputDiagIndex);
    if(outputArgs<=nargout)
        varargout{outputArgs}=patterns{diagnosisIndex,3};
        outputArgs=outputArgs+1;
    end
    if(outputArgs<=nargout)
        varargout{outputArgs}=distances(diagnosisIndex);
        outputArgs=outputArgs+1;
    end
    outputDiagIndex=outputDiagIndex+1;
end
end

function [time,vvout]=donwsampleAndCrop(tabla,fs,movmeanWindow,cropStep)
[time,vvout,stepInput]=getAttributesFromTable(tabla);
vvout=removeOffset(vvout);
% vvout=rescaleSimulation(stepInput,vvout);
[~,vvout]=downsampleSignal(time,vvout,fs);
[time,stepInput]=downsampleSignal(time,stepInput,fs);
if cropStep
    [time,vvout]=removeInitalState(time,vvout,stepInput);
end
vvout=movmeanNew(vvout,movmeanWindow);

    function rescaledSim=rescaleSimulation(stepInput,vvout)
         stepAmp=abs(stepInput(31)-stepInput(end));
         rescaledSim=vvout/stepAmp;
    end

    function [croppedTime,croppedVvout]=removeInitalState(time,vvout,stepInput)
        crossIndex=getStepTime(stepInput);
        croppedVvout=vvout(crossIndex:end);
        croppedTime=time(crossIndex:end);
        croppedTime=croppedTime-min(croppedTime);
    end

    function [time,vvout,stepInput]=getAttributesFromTable(t)
        tableVariables=lower(t.Properties.VariableNames);
        stepInput=t.(2);
        vvout=table2array(t(:,contains(tableVariables,'out')));
        time=table2array(t(:,contains(tableVariables,'time')));
    end
end

function trimmedSignal=trimSimulation(simX,simY,patternX,patternY)
% if length(simY)>length(patternY)
    %         signalY=signalY(1:length(patternY));
    patternX=reshape(patternX,1,[]); simX=reshape(simX,1,[]);
    simY=reshape(simY,1,[]);
    trimmedSignal=simY((max(patternX)-min(patternX))<=simX);

% else
%     trimmedSignal=simY;
% end
end

function yZeroRef=removeOffset(yOffsetRef)
if max(yOffsetRef)-min(yOffsetRef)>0.1
    %     [~,midlev]=midcross(vvout);
    yZeroRef=yOffsetRef-min(yOffsetRef);
else
    yZeroRef=yOffsetRef;
end
end

function smoothedSignal=movmeanNew(signal,window)
if window==0
    smoothedSignal=signal;
else
    smoothedSignal=movmean(signal,window);
end
end