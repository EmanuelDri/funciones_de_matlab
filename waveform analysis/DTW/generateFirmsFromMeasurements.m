function row=generateFirmsFromMeasurements(movmeanWindow,folderAddress,filterOrder,varargin)
recurseFolders=true;
cropStep=true;
characterParameters=cellfun(@ischar,varargin);
thresholds=[0,1.256605314896180e-2,0.555633842280334e-2 ];

switch filterOrder
    case 4
        circuits={'Entrada';'Salida';'Completo'};
    case 2
        circuits={[]};
end

if~isempty(characterParameters)
    for index=find(characterParameters)
        switch varargin{index}
            case 'RecurseFolders'
                recurseFolders=varargin{index+1};
            case 'Thresholds'
                thresholds=varargin{index+1};
            case 'ExplicitCircuit'
                circuits=varargin(index+1);
            case 'RemoveInitialState'
                cropStep=varargin{index+1};
        end
    end
end

for index=1:length(circuits)
    row=generatePattern(circuits{index},folderAddress,recurseFolders,cropStep,movmeanWindow);
    cropPatterns(circuits{index},thresholds(index));
end


    function patterns=generatePattern(circuitName,folderAddress,recurseFolders,cropStep,movmeanWindow)
        if recurseFolders
            folders=listFolders(folderAddress); %listar las carpetas
        else
            folders={folderAddress};
        end
        patterns={};
        accumulator=0;
        for folderIndex=1:length(folders)
            folderName=folders{folderIndex};
            if isempty(circuitName)
                searchString='';
            else
                searchString=['*' circuitName];
            end
            if recurseFolders
                files=dir(fullfile(folderAddress,folderName,[searchString '*.mat'])); %obtener la lista de ficheros de la carpeta
            else
                files=dir(fullfile(folderAddress,[searchString '*.mat'])); %obtener la lista de ficheros de la carpeta
            end
            if ~contains(folderName,'libreDeFallas')  %para las mediciones libres de fallas hacemos otro tratamiento
                for fileIndex=1:length(files)
                    %% get file info
                    [measurements,fileAddress]=importFileStruct(files(fileIndex));
                    [subrow,accumulator]=generatePatternRow(measurements,accumulator,fileAddress,cropStep,movmeanWindow);
                    patterns(accumulator,:)=subrow;
                end
            else
                allMeasurements={};
                for fileIndex=1:length(files) %se listan todos los ficheros con mediciones libres de fallas y se juntan sus resultados en uno solo representativos de ellos
                    [measurements,~]=importFileStruct(files(fileIndex));
                    allMeasurements=[allMeasurements;measurements];
                end
                [subrow,accumulator]=generatePatternRow(allMeasurements,accumulator,folderName,cropStep,movmeanWindow);
                patterns(accumulator,:)=subrow;
                save(['firma' circuitName '.mat'],'allMeasurements');
            end
        end
        save(['patterns' circuitName '.mat'],'patterns');
    end

    function [data,fileAddress]=importFileStruct(fileStruct)
        fileName=fileStruct.name;
        fileFolder=fileStruct.folder;
        fileAddress=fullfile(fileFolder,fileName);
        data=importdata(fileAddress);
    end

    function [row,accumulator]=generatePatternRow(measurements,accumulator,storeString,cropStep,movmeanWindow)
        row=cell(1,3);
        [averageTime,averagePattern,~,inputSignal]=averageFile(measurements,movmeanWindow);
        averagePattern=removeOffset(averagePattern);
        accumulator=accumulator+1;
        if cropStep
            crosstimeIndex=getCrossTime(inputSignal);
        else
            crosstimeIndex=1;
        end
        averageTime=averageTime(crosstimeIndex:end);
        row{1}=averageTime-min(averageTime);
        row{2}=averagePattern(crosstimeIndex:end);
        row{3}=storeString;
    end

    function [x1,y1,x2,y2]=averageFile(measurements,movmeanWindow)
        y1=cleanAndAverageMeasurementsBach(measurements,movmeanWindow);
        x1=mean(cell2mat(measurements(:,1)));
        x2=mean(cell2mat(measurements(:,3)));
        y2=mean(cell2mat(measurements(:,4)));
        %         [ini,fin]=getBistateLevels(y2,'Histogram mode',200);
        %         stepAmp=abs(fin-ini);
        %         y1=y1/stepAmp;
    end
end

function cropPatterns(circuitName,threshold)
patterns=importdata(['patterns' circuitName '.mat']);
for index=1:length(patterns)
    yp=patterns{index,2};
    xp=patterns{index,1};
    stepInformation=stepinfo(yp,xp,'SettlingTimeThreshold',threshold);
    maxTime=stepInformation.SettlingTime;
    transientIndexes=xp<=maxTime;
    yp=yp(transientIndexes);
    xp=xp(transientIndexes);
    patterns{index,1}=xp; patterns{index,2}=yp;
end
save(['patterns' circuitName '.mat'],'patterns');
end

function signalWOOFset=removeOffset(signal)
        if (max(signal)-min(signal))>0.1
            [offset,~]=getBistateLevels(signal,'Histogram mode',555);
            signalWOOFset=signal-offset;
        else
            signalWOOFset=signal;
        end
    end

function yMean=cleanAndAverageMeasurementsBach(measurements,movmeanWindow)
[tamano,~]=size(measurements);
promedio=zeros(1,length(measurements{1,1}));
for index=1:tamano
    y=measurements{index,2};
%     yClean=movmean(y,movmeanWindow);
    promedio=promedio+y;
end
yMean=promedio/tamano;
yMean=movmean(yMean,movmeanWindow);
end

function crossIndex=getCrossTime(inputSignal)
[~,t1,t2]=slewrate(inputSignal);
crossIndex=floor(abs(t1+t2)/2);
end