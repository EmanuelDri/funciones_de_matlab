function [dtwWindow,settlingTimeThreshold,psoResults]=optimizeDTWHyperParamsPSO(dtwTestObj,realMeas,sims,psoIterations,lb,ub,psoptions,varargin)

psoResults=zeros(psoIterations,3);
for iteration=1:psoIterations
    [x,fval]=psoRun(lb,ub,psoptions);
    iterString=['Porcent. de est.: ' num2str(x(1)) '%    Ventana DTW: ' num2str(floor(x(2)))];
    disp(iterString);
    psoResults(iteration,1:2)=x;
    psoResults(iteration,3)=fval;
end
disp(psoResults);

bestFvalInd=find(psoResults(:,3)==min(psoResults(:,3)),1);
dtwWindow=floor(psoResults(bestFvalInd,2));
settlingTimeThreshold=round(psoResults(bestFvalInd,1),2);

    function [x,fval]=psoRun(lb,ub,psoptions)
        if length(ub)~=length(lb)
            error('lower bounds are not the same size as upper bounds');
        else
            [x,fval]=particleswarm(@costFunction,length(ub),lb,ub,psoptions);
        end
    end

    function costValue=costFunction(psoData)
        settlingTimeThres=round(psoData(1),2);
        dtwWin=floor(psoData(2));
        objCopy=copy(dtwTestObj);
        objCopy.setDTWWinAndSettl(dtwWin,settlingTimeThres);
        if ~isempty(realMeas)
            [sumReal,accuracyList]=objCopy.realFilter(realMeas);
        else
            accuracyList=true;
        end
        [~,accuracySim,minFaults,maxDTW]=objCopy.simFilter(varargin{:});
        costValue=min(minFaults,[],'all');
        
        if accuracyList
            costValue=costValue-accuracySim/100;%+maxDTW/20;
        else
            realFilterDistances=sumReal(:,3);
            falsePositives=realFilterDistances(realFilterDistances>0);
            costValue=costValue+sum(falsePositives)+3;
        end 
    end

end