

function [x,fval]=optimizeDTWHyperParamsPSO(lb,ub,varargin)
if length(ub)~=length(lb)
    error('lower bounds are not the same size as upper bounds');
else
    psoptions=optimoptions(varargin{:});
    [x,fval]=particleswarm(@costFunction,length(ub),lb,ub,psoptions);
end
end


function costValue=costFunction(psoData)
settlingTimeThreshold=psoData(1);
dtwWindow=floor(psoData(2));

global realMeasurementsGL measurementsGL simsGL;

[sumReal,accuracyList]=EvaluateFilterDTW.realFilter(settlingTimeThreshold,realMeasurementsGL,dtwWindow,measurementsGL);
[~,~,minFaults,faultsDetectedPercent]=EvaluateFilterDTW.simFilter(settlingTimeThreshold,simsGL,dtwWindow,measurementsGL);
costValue=min(minFaults,[],'all');

if accuracyList
    costValue=costValue-faultsDetectedPercent/100;
    if(minFaults<90)
%         costValue=costValue/10;
%         disp('eureka\n');
    else
%         costValue=costValue*1.5;
    end
else
    costValue=costValue+sumReal(3)*3;
end

end


