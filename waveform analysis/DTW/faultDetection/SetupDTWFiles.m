classdef SetupDTWFiles
    methods (Static)
        function measObj=importMeasFile(file,varargin)
            fileStruct=validateFileInput(file);
            if ~isempty(fileStruct)
                measData=importFileStruct(fileStruct); %use this to use inter-chip measurements
                measObj=DTWPatternMeasurement(measData,varargin{:});
            else
                measObj=[];
            end
        end
        
        function controlMeasObj=importControlMeas(file,varargin)
            fileStruct=validateFileInput(file);
            if ~isempty(fileStruct)
                measData=importFileStruct(fileStruct);
                controlMeasObj=DTWControlMeasurement(measData,varargin{:});
            else
                controlMeasObj=[];
            end
        end
        
        function sims=importSim(file,varargin)
            fileStruct=validateFileInput(file);
            if ~isempty(fileStruct)
                %% import sim files
                sims=retrieveSimulationFiles(fileStruct,varargin{:});
            else
                sims=[];
            end
        end
    end
end

function fileStruct=validateFileInput(file)

if ischar(file)
    fileStruct=dir(file);
else
    fileStruct=file;
end
end

function outputStruct=retrieveSimulationFiles(simAddressesStruct,varargin)
simulationsNumber=length(simAddressesStruct);
simulations=cell(1,simulationsNumber);
names=cell(1,simulationsNumber);
parfor index=1:simulationsNumber
    currentFile=simAddressesStruct(index);
    currentSim=importFileStruct(currentFile);
    currentSimObj=DTWSimulationBatch(currentSim,varargin{:});
    simulations{index}=currentSimObj;
    [~,simName,~]=fileparts(currentFile.name);
    names{index}=simName;
end
outputStruct=[names;simulations];
end
