classdef EvaluateFilterDTW < matlab.mixin.Copyable
    properties (Access=protected)
        dtwWindow;
        movmeanWindow;
        settlingTimeThreshold;
        patternMeasurements;
        patternWaveForm;
        dtwThreshold;
        devStep;
        sims;
        patternSimulation;
        faultFreeSim;
    end
    methods
        %% constructor
        function obj=EvaluateFilterDTW(dtwWindow,settlingTimeThreshold,patternMeasurements,devStep,movmeanWindow,sims,faultFreeSim)
            obj.dtwWindow=dtwWindow;
            obj.movmeanWindow=movmeanWindow;
            obj.settlingTimeThreshold=settlingTimeThreshold;
            obj.patternMeasurements=patternMeasurements;
            if nargin>4
                obj.devStep=devStep;
            else
                obj.devStep=1;
            end
            if nargin>5
                obj.movmeanWindow=movmeanWindow;
            else
                obj.movmeanWindow=1;
            end
            obj.sims=sims;
            obj.faultFreeSim=faultFreeSim;
            obj.generatePattern();
            obj.generateThreshold();
        end
        %% setter
        function setDTWWinAndSettl(obj,dtwWindow,settlingTimeThreshold)
            if obj.settlingTimeThreshold~=settlingTimeThreshold
                obj.settlingTimeThreshold=settlingTimeThreshold;
                obj.dtwWindow=dtwWindow;
                obj.generatePattern();
                obj.generateThreshold();
            elseif obj.dtwWindow~=dtwWindow
                obj.dtwWindow=dtwWindow;
                obj.generatePattern();
                obj.generateThreshold();
            end
        end
        %% getter
        function threshold=getThreshold(obj)
            threshold=obj.dtwThreshold;
        end
        %% test simulations
        function [summary,accuracy,minFault,threshold,faultAnalysis]=simFilter(obj,varargin)
            simulations=obj.sims;
            simulationsNumber=length(simulations);
            singleSim=simulations{2,1};
            [~,tableArraysNumber]=size(singleSim.getSimsResponses());
            summary=cell(1+tableArraysNumber,simulationsNumber*3);
            resultsCollection=cell(1,simulationsNumber);
            %% get variables
            dtwThres=obj.dtwThreshold;
            patternWvFrm=obj.patternSimulation;
            dtwWin=obj.dtwWindow;
            movmeanWin=obj.movmeanWindow;
            dvStep=obj.devStep;
            parfor simIndex=1:simulationsNumber
                currentSimBatch=simulations{2,simIndex};
                simObj=DTWSimFilterUnderTest(currentSimBatch);
                resultados=simObj.detectFaults(dtwThres,patternWvFrm,dtwWin,movmeanWin,varargin{:}); %evaluate which faults are detectable in the simulation file
                resultados(:,1)=resultados(:,1).*dvStep; %correct the deviation percentage number introduced in the filter's capacitors
                resultsCollection{simIndex}=resultados;
            end
            for simIndex=1:simulationsNumber
                currentSimName=simulations{1,simIndex};
                resultados=resultsCollection{simIndex};
                summary(1,(1:3)+(simIndex*3-3))={currentSimName,'',''};
                summary(2:end,(1:3)+(simIndex*3-3))=num2cell(resultados);
            end
            [accuracy,minFault]=calculateDetectionAccuracy(summary); %%calculate the percentaje of faults injected that could
            %%be detected and the minimum deviation upon which all faults are detected
            faultAnalysis=faultDetectionPerComponent(summary,obj.devStep);
            threshold=obj.dtwThreshold;
        end
        %% test real measurements
        function [summary,accuracy,minFault,threshold]=realFilter(obj,realMeasurement)
            realMeasObj=DTWRealMeasUnderTest(realMeasurement.getBatch());
            summary=realMeasObj.detectFaults(obj.dtwThreshold,obj.patternWaveForm,obj.dtwWindow,obj.movmeanWindow); %evaluate which faults are detectable in the simulation file
            accuracy=sum(summary(:,1))<1;
            minFault=accuracy;
            threshold=obj.dtwThreshold;
        end
        %% optimizer hyper params and test both real filter and simulated filters
        function [dtwWindow,settlingTimeThreshold,psoResults,faultsPerComponent]=evaluateCase(obj,configStruct,optimizeHP,varargin)
            ctrlMeasurements=configStruct.controlMeas;
            %% optimize test params
            psoResults=[];
            if optimizeHP
                tic
                psoptions=optimoptions('particleswarm','Display','iter','UseParallel',false,'ObjectiveLimit',0.9);
                [dtwWindow,settlingTimeThreshold,psoResults]=optimizeDTWHyperParamsPSO(copy(obj),ctrlMeasurements,obj.sims,1,...
                    [0 4],[7 50],psoptions,varargin{:});%,'MaxIter',1);
                toc
                psoResults=[dtwWindow,settlingTimeThreshold,psoResults(3)];
                obj.setDTWWinAndSettl(dtwWindow,settlingTimeThreshold)
                disp(configStruct.name);
                save(configStruct.saveAddress);
            else
                dtwWindow=obj.dtwWindow;
                settlingTimeThreshold=obj.settlingTimeThreshold;
                psoResults=[];
            end
            
            %% evaluate simulations
            [summaries,accuracyList,minFaults,thresholds,faultsPerComponent]=obj.simFilter(varargin{:});
            disp(faultsPerComponent);
            if ~isempty(ctrlMeasurements)
                % evaluate real measurements
                [summariesRF,~,~,~]=obj.realFilter(ctrlMeasurements);
                disp(summariesRF);
            end
        end
    end
    methods (Access=private)
        function obj=generatePattern(obj)
            %             patternNonCropped=obj.patternMeasurements.getPattern();
            obj.patternMeasurements.generatePattern(obj.settlingTimeThreshold/100);
            obj.patternWaveForm=obj.patternMeasurements.getPattern();
            
            meanTime=obj.patternWaveForm{1};
            meanResponse=obj.patternWaveForm{2};
            fs=WaveFormAnalysis.getSampleFrequency(meanTime);
            singleSim=obj.sims{2,1};
            time=obj.faultFreeSim{2};
            %             time=singleSim.getTime();
            inputStep=obj.faultFreeSim{3};
            %             inputStep=singleSim.getInputStep();
            patternSim=obj.faultFreeSim{1};
            %             patternSim=singleSim.getFaultFreeSimulation();
            [resSim,resTime]=WaveFormAnalysis.downsampleSignal(time,patternSim,fs);
            [resStep,~]=WaveFormAnalysis.downsampleSignal(time,inputStep,fs);
            lt=BistateAnalyzer.getStepTime(resStep)+1;
            finalIndex=min(length(resSim),length(meanResponse)+lt-1);
            resTime=resTime(lt:finalIndex);
            resSim=resSim(lt:finalIndex);
            obj.patternSimulation={resTime;resSim};
            %             patternCropped=obj.patternWaveForm;
            %             printPatternCompare(patternCropped,patternNonCropped,obj.settlingTimeThreshold);
        end
        function obj=generateThreshold(obj)
            obj.patternMeasurements.generateThreshold(obj.dtwWindow);
            patternMeasObj=copy(obj.patternMeasurements);
            patternMeasObj.setPattternWvFrm(obj.patternSimulation);
            obj.dtwThreshold=obj.patternMeasurements.getThreshold();
        end
    end
end
%% private functions
function printPatternCompare(baseWvFrm,croppedPattern,settlthres)
figure;hold on;
plot(croppedPattern{1}*1e6,croppedPattern{2},'LineWidth',5);
plot(baseWvFrm{1}*1e6,baseWvFrm{2},'LineWidth',3);
legend({'Forma de onda patrón original',['Forma de onda patrón recortada al ' num2str(settlthres) '%']},'Location','best');
xlabel('Tiempo [\mus]');
ylabel('Tensión [V]');
nombre=input('ingresar Nombre del circuito: ','s');
title(nombre);
% savefig(fullfile('Figures for debug',['comparePattern ' nombre]));
% exportgraphics(gcf,fullfile('Figures for debug',['comparePattern ' nombre ]));

end

function results=faultDetectionPerComponent(summary,varargin)
faultsScale=1;
if ~isempty(varargin)
    faultsScale=varargin{1};
end
labels=summary(1,1:3:end-2);
[simulationsNumber,~]=size(summary(2:end,:));
componentsNumber=length(labels);
detectionResults=cell2mat(summary(2:end,2:3:end-1));
[rows,~]=size(detectionResults);
if rows>1
    faultFreeIndex=median(1:simulationsNumber);
    results=cell(3,componentsNumber);
    faultDeviations=(1:simulationsNumber)-faultFreeIndex;
    faultDevMatrix=repmat(faultDeviations',1,componentsNumber).*~detectionResults;
    detectedFaults=[min(faultDevMatrix)-1;max(faultDevMatrix)+1];
    % detectedFaults(abs(detectedFaults)>max(faultDeviations))=200/faultsScale;
    detectedFaults(:,detectionResults(faultFreeIndex,:)==1)=100/faultsScale;
    detectedFaults=detectedFaults*faultsScale;
    results(2:3,:)=num2cell(detectedFaults);
else
    results=cell(2,length(labels));
    results(2,:)=num2cell(detectionResults);
end
results(1,:)=labels;
end

function [accuracy,minFault]=calculateDetectionAccuracy(summary)
data=cell2mat(summary(2:end,:));
[~,w]=size(data);
detections=data(:,2:3:w);
coefficients=abs((data(:,1:3:w-2)).^1);
accuracyCoeficients=coefficients.*~detections;
[h2,w2]=size(detections);
% accuracy=sum(sum(detections))/w2/(h2-1)*100;
faultFreeEvaluations=detections(median(1:h2),:);
minFault=max(max(accuracyCoeficients))+1;
accuracy=100*(1-sum(accuracyCoeficients)/sum(coefficients));
if sum(faultFreeEvaluations)==0
    if max(max(accuracyCoeficients))==max(max(coefficients))
        accuracy=accuracy/10;
        minFault=minFault+10;
    end
else
    accuracy=accuracy/100;
    minFault=100;
end
end