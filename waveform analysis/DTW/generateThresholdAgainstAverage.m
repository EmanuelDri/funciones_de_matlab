function [threshold,formattedSignals]=generateThresholdAgainstAverage(window,movmeanWindow,filterWaveForms,varargin)
% The input argument must be boolean, which indicates if the signals should
% or not be scaled
%% Description
% 
%  Optional parammeters:
% * Scale (boolean): determines whether or not to scale the filter
% responses according to the input signal amplitude. Default (false)
% * SettlingTimeThreshold (float): this is the settling time percentage desired for
% the singnals. All posterior points are removed. Default (0.1%)
% * RemoveInitialState (boolean): determines whether or not to remove the initial
% state of the filter responses. Default (true)
% * Euclidean (boolean): if true it forces the algorithm to use euclidean
% distance function instead of DTW distance function
% 

%% settle the optional parammeters
scale=false;
settlingTimeThreshold=0.001;
removeInitialState=true;
distanceFunction='DTW';

if~isempty(varargin)
    characterInputs=find(cellfun(@ischar,varargin));
    for parameterIndex=characterInputs
        switch varargin{parameterIndex}
            case 'Scale'
                scale=varargin{parameterIndex+1};             
            case'SettlingTimeThreshold'
                settlingTimeThreshold=varargin{parameterIndex+1}/100;
            case 'RemoveInitialState'
                removeInitialState=varargin{parameterIndex+1};             
            case 'EuclideanDistance'
                distanceFunction=varargin{parameterIndex};             
        end
    end
end

%% sync all waveforms from the measurements batch
times=cell2mat(filterWaveForms(:,3));
syncStartTime=max(times(:,1));
syncEndTime=min(times(:,end));
for measIndex=1:length(filterWaveForms)
    %% disgreggate the filter I/O signal vectors from each measurement 
    measOutTime=filterWaveForms{measIndex,1}; %get the filter response time vector
    measOutV=filterWaveForms{measIndex,2}; %get the filter response voltages vector
    measInTime=filterWaveForms{measIndex,3}; %get the input signal time vector
    measInV=filterWaveForms{measIndex,4}; %get the input signal voltages vector
    %% Resync all signals
    measOutV=measOutV(measOutTime>=syncStartTime&measOutTime<=syncEndTime);
    measOutTime=measOutTime(measOutTime>=syncStartTime&measOutTime<=syncEndTime);
    measInV=measInV(measInTime>=syncStartTime&measInTime<=syncEndTime);
    measInTime=measInTime(measInTime>=syncStartTime&measInTime<=syncEndTime);
    filterWaveForms(measIndex,:)={measOutTime,measOutV,measInTime,measInV}; %update the signals collection with the current sample resynced
end
%%% determine the minimum length signal and the others to match its length
minLength=min(min(cellfun(@length,filterWaveForms)));
for measIndex=1:length(filterWaveForms) %for each measurement
    for colIndex=1:4 %for each array from a measurement
        currentElement=filterWaveForms{measIndex,colIndex};
        currentElement=currentElement(1:minLength); %remove final elements to match the size of the shortest measurement
        filterWaveForms{measIndex,colIndex}=currentElement; %update the samples collection
    end
end
%% scale the filter responses if requested by its corresponding input parammeter
if scale
    preprocessedSignals=scaleAndRemoveOffset(filterWaveForms);
else
    preprocessedSignals=filterWaveForms;
end
%% preprocessing signals
promedio=averageResponse(preprocessedSignals,movmeanWindow); %%Ac� se genera la forma de onda patr�n y ya es filtrada con promedio m�vil
preprocessedSignals=denoiseSignals(preprocessedSignals,movmeanWindow); % remove signals noise
[preprocessedSignals,patternSignal]=removeStationaryStates(preprocessedSignals,promedio,settlingTimeThreshold,removeInitialState); %remove the initial and final states from the filter output signals
threshold=generateThreshold(window,preprocessedSignals,patternSignal,distanceFunction);


formattedSignals={preprocessedSignals{1,1},patternSignal};

    function [mediciones,patron]=removeStationaryStates(signals,promedio,settlingTimeThreshold,removeInitialState)
        mediciones={};
        stepInformation=stepinfo(promedio,'SettlingTimeThreshold',settlingTimeThreshold); %calculamos las caracter�sticas de la respuesta escal�n del patr�n/promedio
        meanStepInput=mean(cell2mat(signals(:,4))); %obtenemos la forma de onda escal�n medio
        meanStepTime=mean(cell2mat(signals(:,3))); %obtenemos el tiempo medio de las se�ales
        settlingTimeIndex=floor(stepInformation.SettlingTime); %obtenemos el tiempo de establecimiento del patr�n
        %% recorte de se�ales promedio y patron
%         meanStepInput=meanStepInput(1:settlingTimeIndex); %recortamos el escal�n de entrada de acuerdo al establecimiento del patr�n
%         meanStepTime=meanStepTime(1:settlingTimeIndex); %recortamos el tiempo de acuerdo al establecimiento del patr�n
        patron=promedio(1:settlingTimeIndex); %recortamos el patr�n hasta su establecimiento
        if removeInitialState
            [~,patron]=removeInitalState(meanStepTime,patron,meanStepInput);
        end
                
        for index=1:length(signals)
            %% recorte del estado final
            xout=signals{index,1};xout=xout(1:settlingTimeIndex); %recortamos el tiempo de la respuesta del filtro
            yout=signals{index,2};yout=yout(1:settlingTimeIndex); %recortamos las tensiones de la respuesta del filtro
            xin=signals{index,3};xin=xin(1:settlingTimeIndex); %recortamos el tiempo de la entrada del filtro
            yin=signals{index,4};yin=yin(1:settlingTimeIndex); %recortamos las tensiones de la entrada del filtro
            %% quitar el estado inicial
            if removeInitialState
                %% remove the initial state from each signal
                [xout,yout]=removeInitalState(xout,yout,yin);
                yin=yin((xin>=min(xout))&(xin<=max(xout)));
            end
            xin=xout;
            mediciones(index,:)={xout,yout,xin,yin};
        end
    end

    function promedio=averageResponse(signals,movmeanWindow)
        promedio=mean(cell2mat(signals(:,2)));
        promedio=movmean(promedio,movmeanWindow);
    end

    function cleanSignals=denoiseSignals(measurements,movmeanWindow)
        cleanSignals=measurements;
        parfor index=1:length(measurements)
            y=movmean(measurements{index,2},movmeanWindow);
            cleanSignals{index,2}=y;
        end
    end

end