function threshold=generateDTWThreshold(window,distanceFunctionExponent,waveForms)
if nargin==0
    window=[];
    distanceFunctionExponent=1;
end

distances=zeros(length(waveForms));
measurementsNumber=length(waveForms);
amplitudes=waveForms(:,2);
for index1=1:measurementsNumber
    ych1=amplitudes{index1};
    parfor index2=1:measurementsNumber
        ych2=amplitudes{index2};
        dist=dtwDistanceFunction(ych1,ych2,window,distanceFunctionExponent);
        distances(index1,index2)=dist;
    end
end
threshold=max(max(distances));
end

