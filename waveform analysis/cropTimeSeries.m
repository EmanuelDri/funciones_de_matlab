function [x, y]=cropTimeSeries(amplitude,times,startTime,endTime)
y=amplitude(times<=endTime&times>=startTime);
x=time(times<=endTime&times>=startTime);
end