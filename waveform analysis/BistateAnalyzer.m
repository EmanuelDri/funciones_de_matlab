classdef BistateAnalyzer
    methods (Static)
        function settlingTimeIndex=getSettlingTime(y,settlingTimeThreshold)
            stepInformation=stepinfo(y,'SettlingTimeThreshold',settlingTimeThreshold); %calculamos las características de la respuesta escalón del patrón/promedio
            settlingTimeIndex=floor(stepInformation.SettlingTime); %obtenemos el tiempo de establecimiento del patrón
        end
        function [stepIndex,varargout]=getStepTime(inputSignal,varargin)
            [stepIndex,timeOutput]=getStepTimeImplementation(inputSignal,varargin{:});
            if ~isempty(varargin)&&nargout>0
                varargout{1}=timeOutput;
            end
        end
        function [croppedTime,croppedSignal,croppedInputStep,crossIndex]=removeInitalState(time,signalY,stepInput)
            crossIndex=getStepTimeImplementation(stepInput); %determine the step slope instant
            croppedSignal=signalY(crossIndex:end); %remove points previous to the step slope
            croppedTime=time(crossIndex:end); %remove points previous to the step slope
            croppedTime=croppedTime-min(croppedTime); %set the step instant as t=0
            croppedInputStep=stepInput(crossIndex:end);
        end
        function [inferior,superior]=getBistateLevels(samplesArray,varargin)
            % getBistateLevels: this function determines the stable state levels of the
            % input array named samplesArray. Optionally the calculus method and the
            % number of bins to be used in the histogram generation can be passed as
            % input parameters as getBistateLevels(samplesArray,method,numberOfBins)
            % DESCRIPTIVE TEXT
            
            numbins=100;
            if isempty(varargin)
                method='Histogram mode';
            else
                method=varargin{1};
                if length(varargin)>1
                    numbins=varargin{2};
                end
            end
            stateLevelEstimationSystemObject=dsp.StateLevels('Method',method,'HistogramNumBins',numbins);
            levels=step(stateLevelEstimationSystemObject,reshape(samplesArray,[],1));
            inferior=levels(1);
            superior=levels(2);
        end
    end
end

function [stepIndex,timeOutput]=getStepTimeImplementation(inputSignal,varargin)
[~,t1,t2]=slewrate(inputSignal);
stepIndex=floor(mean([t1 t2]));
if ~isempty(varargin)
    time=varargin{1};
    timeOutput=time(stepIndex);
else
    timeOutput=[];
end
end