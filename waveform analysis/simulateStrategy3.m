function [peakTime,osPercent] = simulateStrategy3( ~,voltages,times)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

pgaGain=1/32;
dacWord=31;
% hystList=[.021 .042 .062 .125 .188 .25 .312 .375 .437 .5 .562 .625 .688 .75 .812 .875 .937 1];
hystList=1/16:1/16:1;
osVoltage=max(voltages);
hystIndex=0;
thresholdVoltage=0;
while(thresholdVoltage<=osVoltage)&&(hystIndex<=length(hystList))
    hystIndex=hystIndex+1;
    hyst=hystList(hystIndex);
    thresholdVoltage=calculateThreshold(0,1,hyst);
end
hyst=hystList(hystIndex-1);
thresholdVoltage=calculateThreshold(31,pgaGain,hyst);

while(thresholdVoltage<=osVoltage)&&(pgaGain<31/32)
    pgaGain=pgaGain+1/32;
    thresholdVoltage=calculateThreshold(dacWord,pgaGain,hyst);
end

while(thresholdVoltage>osVoltage)&&(dacWord>-31)
    dacWord=dacWord-1;
    thresholdVoltage=calculateThreshold(dacWord,pgaGain,hyst);
end

comparatorTriggeredInterval=times(voltages>=thresholdVoltage);
peakTime=mean(comparatorTriggeredInterval);
osVoltage=mean(voltages(voltages>=thresholdVoltage));
[inferiorVoltage,superiorVoltage]=getBistateLevels(voltages);
osPercent=(osVoltage-superiorVoltage)/(superiorVoltage-inferiorVoltage)*100;

    function thresholdVoltage=calculateThreshold(dacWord,pgaGain,hyst)
        dacVoltage=calculateDACVoltage(dacWord,0);
        referenceVoltage=calculateReferenceVoltage(pgaGain,dacVoltage);
        thresholdVoltage=comparatorThreshold(hyst,referenceVoltage,5);
    end

    function dacVoltage=calculateDACVoltage(controlWord,dacGain)
        dacVoltage=(3.8-2.5)*(1+dacGain)*(controlWord/32)+2.5;
    end

    function vref=calculateReferenceVoltage(pgaGain,dacVoltage)
        vref=(dacVoltage-2.5)*pgaGain+2.5;
    end

    function thresholdVoltage=comparatorThreshold(hyst,vref,vcc)
        thresholdVoltage=vref+(vcc-vref)*hyst;
    end

end

