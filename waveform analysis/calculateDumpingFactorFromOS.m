function dumping=calculateDumpingFactorFromOS(os)
dumping=1./sqrt((pi./log(os/100)).^2+1);
end