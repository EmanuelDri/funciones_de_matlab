function [peakTime,overshootPercent,overshootVoltage]=determineOvershootAndPeakTime(x,y)
[waveLowState,waveHighState]=getBistateLevels(y);
overshootVoltage=max(y);
overshootPercent=(overshootVoltage-waveHighState)/( waveHighState-waveLowState);
peakTimesArray=x(y==overshootVoltage);
peakTime=mean(peakTimesArray);
end