function os=calculateOSFromVoltages(vpeak,vfinal,vini)
%%
% 
%  vpeak is the overshoot peak voltage, vfinal is the settling voltage,
%  vini is the initial state voltage of the filter response
% os is the overshoot percentage
% 

os=(vpeak-vfinal)/(vfinal-vini)*100;