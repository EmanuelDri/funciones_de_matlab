function signalWithoutOffset=removeOffset(signal)
        %     [ini1,~]=getBistateLevels(signal);
        %     ini1=min(signal);
        [~,midlevel]=midcross(signal);
        signalWithoutOffset=signal-midlevel;
end
