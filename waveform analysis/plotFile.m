function figHandler=plotFile(varargin)
if nargin==0
    [file,folderPath]=uigetfile;
    fileAddress=fullfile(folderPath,file);
elseif isstruct(varargin{1})
        for 
    fileAddress=varargin{1};
end

function figHandler=plotFileAddress(fileAddress)
data=importdata(fileAddress);
figHandler=plotBatch(data);
[~,fileName,ext]=fileparts(fileAddress);
title([fileName ext]);
end

    end

    