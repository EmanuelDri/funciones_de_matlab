function [fc,mr]=freqRespParams(wp,qp)
tau=1/2/qp;
sys=tf(wp^2,[1 2*wp*tau wp^2]);
fc = getGainCrossover(sys,1/sqrt(2))/2/pi;
mr=(getPeakGain(sys,1e-9));
end