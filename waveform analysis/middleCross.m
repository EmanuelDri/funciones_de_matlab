function [middleCrossTime,middleCrossVoltage]=middleCross(x,y)
transitionMetricsObject = dsp.TransitionMetrics('TimeInputPort',true,'StateLevelsSource','Auto');
[transition] = step(transitionMetricsObject,y',x');
middleCrossTime=transition.MiddleCross;
middleCrossVoltage=y(middleCrossTime==x);
end