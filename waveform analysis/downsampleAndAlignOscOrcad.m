function [time,oscIn,oscOut,orcadOut,orcadIn]=downsampleAndAlignOscOrcad(ychOut,xchIn,ychIn,timeOrcad,vinOrcad,voutOrcad)
osciloscopeSampleRate=1/mean(diff(xchIn));
% orcadSampleRate=mean(diff(timeOrcad));
% [fsnumerator,fsdenominator]=rat(orcadSampleRate/osciloscopeSampleRate);
% tOrcad=resample(timeOrcad,timeOrcad,fsdenominator);
[orcadIn,tOrcad]=resample(vinOrcad,timeOrcad,osciloscopeSampleRate);
orcadOut=resample(voutOrcad,timeOrcad,osciloscopeSampleRate);

tOrcad=tOrcad(10:end-10);
orcadIn=orcadIn(10:end-10);
orcadOut=orcadOut(10:end-10);

if length(tOrcad)>25e3
    tOrcad=tOrcad(1:25e3);
    orcadIn=orcadIn(1:25e3);
    orcadOut=orcadOut(1:25e3);
end

[~,xi,yi]=dtw(orcadIn,ychIn);
freqxi=tabulate(xi);
freqyi=tabulate(yi);

[~,index1]=max(freqyi(:,2));
[~,index2]=max(freqxi(:,2));

    oscIn=ychIn;
    oscOut=ychOut;
    time=xchIn;

if index1<floor(length(orcadIn)/2)
    orcadIn=orcadIn(index1:end);
    orcadOut=orcadOut(index1:end);
else
    if index1>length(orcadIn)
        index1=length(orcadIn);
    end
    orcadIn=orcadIn(1:index1);
    orcadOut=orcadOut(1:index1);
end

if index2<floor(length(xchIn)/2)
    oscIn=oscIn(index2:end);
    oscOut=oscOut(index2:end);
    time=time(index2:end);
%     orcadIn(1:index2)=[];
%     orcadOut(1:index2)=[];
else
    if index2>length(oscIn)
        index2=length(oscIn);
    end
    oscIn=oscIn(1:index2);
    oscOut=oscOut(1:index2);
    time=time(1:index2);
end

%%

[time,oscIn,oscOut,orcadOut,orcadIn]=alignSignals(oscIn,tOrcad,orcadIn,orcadOut,time,oscOut);
crucesOrcad=midcross(orcadIn); crucesOrcad=floor(reshape(crucesOrcad,1,[])); 
crucesOsc=midcross(oscIn); crucesOsc=floor(reshape(crucesOsc,1,[])); crucesOsc(1)=[];
delay=floor(mean(crucesOrcad-crucesOsc));

if delay>0
    orcadIn(1:delay)=[];
    orcadOut(1:delay)=[];
    oscIn(length(orcadIn)+1:end)=[];
    oscOut(length(orcadIn)+1:end)=[];
else
    oscIn(1:-delay)=[];
    oscOut(1:-delay)=[];
    orcadIn(length(oscIn)+1:end)=[];
    orcadOut(length(oscIn)+1:end)=[];
end
time(length(orcadIn)+1:end)=[];

        [~,midlevOrcadOutput]=midcross(orcadOut);
        [~,midlevOscilloscopeOutput]=midcross(oscOut);
%         orcadOut=orcadOut-(midlevOrcadOutput-midlevOscilloscopeOutput);

% [time,oscIn,oscOut,orcadOut,orcadIn]=alignSignals(ychIn,tOrcad,orcadIn,orcadOut,xchIn,ychOut);
%{
delay=mean(midcross(orcadIn,time)-midcross(oscOut,time)');
tOrcad=time-delay;
[time,oscIn,oscOut,orcadOut,orcadIn]=alignSignals(oscIn,tOrcad,orcadIn,orcadOut,time,oscOut);
%}
    function [time,oscIn,oscOut,orcadOut,orcadIn]=alignSignals(ychIn,tOrcad,orcadIn,orcadOut,xchIn,ychOut)
        
        [~,midlevOrcad]=midcross(orcadIn);
        [~,midlevOscilloscope]=midcross(ychIn);
        
%         orcadIn=orcadIn-midlevOrcad; ychIn=ychIn-midlevOscilloscope;
        
        [istart,istop,~] = findsignal(orcadIn-midlevOrcad,ychIn-midlevOscilloscope);
        if isempty(istart)
            [istart,istop,~] = findsignal(ychIn-midlevOscilloscope,orcadIn-midlevOrcad);
            time=tOrcad;
            oscIn=ychIn(istart:istop);
            oscOut=ychOut(istart:istop);
        else
            time=xchIn;
            oscIn=ychIn;
            oscOut=ychOut;
            orcadIn=orcadIn(istart:istop);
            orcadOut=orcadOut(istart:istop);
        end
    end
end