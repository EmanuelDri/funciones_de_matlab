function [fcs,mrs]=calculateFcAndMr(qps,wps)

fcs=ones(length(qps),1);
mrs=fcs;

for index=1:length(fcs)
    [fc,mr]=freqRespParams(wps(index),qps(index));
    fcs(index)=fc;
    mrs(index)=mr;
end

end