function plotSimFile(varargin)
if nargin==0
    [file,folderPath]=uigetfile;
    fileAddress=fullfile(folderPath,file);
    if ~isempty(fileAddress)
        plotSingleFile(varargin{1});
    end
elseif isstruct(varargin{1})
    plotFileStruct(varargin{1})  ;
elseif ischar(varargin{1})
    inputString=varargin{1};
    if isfile(inputString)
        plotSingleFile(inputString);
    elseif isfolder(inputString)
        filesEnum=dir(fullfile(inputString,'**/*.mat'));
        plotFileStruct(filesEnum);
    end
end

    function plotFileStruct(fileStruct)
        figure,hold on;
        dirResults=fileStruct;
        legends=cell(1,length(dirResults));
        for fileIndex=1:length(dirResults)
            currentFile=dirResults(fileIndex);
            fileAdd=fullfile(currentFile.folder,currentFile.name);
            fileNameLabel=plotFileAddress(fileAdd);
            legends{fileIndex}=fileNameLabel;
        end
        legend(legends);
    end

    function plotSingleFile(fileAddress)
        fileName=plotFileAddress(fileAddress);
        title(fileName);
    end

    function fileName=plotFileAddress(fileAddress)
        data=importdata(fileAddress);
        stepIndex=BistateAnalyzer.getStepTime(data.(2));
        time=data.(1);
        time=time-time(stepIndex);
        plot(time,data.(3));
        [~,fileName,~]=fileparts(fileAddress);
    end

end

