function plotAllFiles(varargin)
if nargin~=0
    searchTemplateString=varargin{1};
else
    searchTemplateString='';
end

figure,archivos=dir(['*' searchTemplateString '.mat']);
allMeasurements={};
for fileIndex=1:length(archivos)
    resultados=importdata(archivos(fileIndex).name);
    allMeasurements=[allMeasurements;resultados];
end
for index=1:length(allMeasurements)
    x=allMeasurements{index,1};y=allMeasurements{index,2};
    plot(x,y);hold on
end