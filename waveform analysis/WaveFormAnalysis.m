classdef WaveFormAnalysis
    methods (Static)
        function filteredSignal=movmean(signal,movmeanWindow)
            if movmeanWindow<2
                filteredSignal=signal;
            else
                filteredSignal=movmean(signal,movmeanWindow);
            end
        end
        
        function [Y1,Y2,X1,X2]=alignSignals(Y1,Y2,varargin)
            %% Description
            %%%
            %%% X1 and Y1 represent one signal to be aligned and X2 and Y2
            %
            inputParams=varargin;
            X1=[];
            X2=[];
            if ~isempty(inputParams)&&isvector(inputParams{1})&&isvector(inputParams{2})
                X1=varargin{1};
                X2=varargin{2};
                inputParams(1:2)=[];
            end
            
            [istart,istop,~] = findsignal(Y2,Y1,inputParams{:});
%             [istart,istop,~] = findsignal(Y2,Y1,'TimeAlignment','dtw', ...
%             'Normalization','center', ...
%             'NormalizationLength',100, ...
%             'MaxNumSegments',1);
%             if (istop-istart)~=length(Y1)
%                 istop=length(Y1)+istart;
%             end
            if istop>length(Y2)
                istop=length(Y2);
            end
            
                Y2=Y2(istart:istop);
%             if ~isempty(X1)
%                 X2=X2(istart:istop);X2=X2-min(X2);
%             end
            
        end
        
        function [resampledAmplitudes,resampledTime]=downsampleSignal(t,y,fs)
            [resampledAmplitudes,resampledTime]=resample(y,t,fs); %Resamplear la respuesta de salida de la simulaci�n para igualar a la de la medici�n experimental
            cropLen=floor(length(resampledAmplitudes)*0.01); %This is done to remove ringing at the beginning and the end of the signal, introduced by the resample function
            resampledAmplitudes=resampledAmplitudes(cropLen:end-cropLen);
            resampledTime=resampledTime(cropLen:end-cropLen); %Corregir el desajuste de amplitudes entre la medici�n y la simulaci�n
        end
        
        %% distance functions
        function [distance,warpingPathX,warpingPathY]=dtwDist(signal1,signal2,window)
            if ~isempty(window)&&~(window==0)
                [distance,warpingPathX,warpingPathY]=dtw(signal1,signal2,window);
            else
                [distance,warpingPathX,warpingPathY]=dtw(signal1,signal2);
            end
%             distance=distance/length(warpingPathX);
        end
        
        function [distance,varargout]=euclideanDist(signal1,signal2,varargin)
            varargout={0,0};
            signal1=reshape(signal1,1,[]);
            signal2=reshape(signal2,1,[]);
            distance=sum(abs(signal1-signal2))/length(signal1);
        end
        
        function fs=getSampleFrequency(timeValues)
            fs=1/mean(diff(timeValues)); %Determinar la frecuencia de muestreo de la captura de osciloscopio
        end
    end
end