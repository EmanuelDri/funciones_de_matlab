function [fittedData,fitresult, gof] = fit2step(inputStep,stepResponse)
%CREATEFIT(MEANRESPONSEPATTERN)
%  Create a fit.
%
%  Data for 'untitled fit 1' fit:
%      Y Output: meanResponsePattern
%  Output:
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%
%  See also FIT, CFIT, SFIT.

%  Auto-generated by MATLAB on 15-Jul-2020 01:27:09


%% Fit: 'untitled fit 1'.
stepIndex=BistateAnalyzer.getStepTime(inputStep);
if nargin==1||isempty(stepResponse)
    stepResponse=inputStep;
end
[xData, yData] = prepareCurveData( [], stepResponse);

% Set up fittype and options.
ft = fittype( ['a*((x-' num2str(stepIndex) ')<=0)+y0'], 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Robust = 'Bisquare';
opts.StartPoint = [0.711215780433683 0.22174673401724];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
fittedData=feval(fitresult,xData);

function plotFitResults(xData,yData,fitresult)
% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fitresult, xData, yData );
legend( h, 'meanResponsePattern', 'untitled fit 1', 'Location', 'NorthEast', 'Interpreter', 'none' );
% Label axes
ylabel( 'meanResponsePattern', 'Interpreter', 'none' );
grid on
end

end
