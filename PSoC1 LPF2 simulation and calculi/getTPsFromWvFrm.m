function [tp,os,q,wn,fc,mr_bw,respuesta]=getTPsFromWvFrm(time,y,varargin)
mode='lp';
if ~isempty(varargin)
    mode=varargin{1};
end

maxVoltage=max(y);
[ini,fin]=BistateAnalyzer.getBistateLevels(y);
tp=mean(time(y==maxVoltage));
os=(maxVoltage-fin)/(fin-ini)*100;
tau=calculateDumpingFactorFromOS(os);
q=1/2/tau;
wn=calculateNaturalFrequencyFromTpAndOS(tp,os);

switch mode
    case 'lp'
        respuesta=tf(wn^2,[1 2*wn*tau wn^2]);
        fc = getGainCrossover(respuesta,db2mag(-3))/2/pi;
%         wr=wn*sqrt(1-2*tau^2);
        mr_bw=(getPeakGain(respuesta,1e-9));
    case 'bp'
        respuesta=tf([wn/q 0],[1 2*wn*tau wn^2]);
        fc = mean(getGainCrossover(respuesta,getPeakGain(respuesta,1e-10)))/2/pi;
        mr_bw=range(getGainCrossover(respuesta,db2mag(-3)))/2/pi;
end
end