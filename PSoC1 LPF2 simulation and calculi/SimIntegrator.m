classdef SimIntegrator
    methods (Static)
        function transF=getIntTF(fs)
            if nargin==0
                s=tf('s');
                transF=1/s;
            else
                z=tf('z',1/fs);
                transF=z/(z-1);
            end
        end
    end
end