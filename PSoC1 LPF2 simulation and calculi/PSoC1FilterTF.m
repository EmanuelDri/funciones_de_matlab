classdef PSoC1FilterTF
    methods (Static)
        function [transferFunction,zeta,wn,gain] = lpFilterTF( c1,c2,c3,c4,ca,cb,fs)
            [gain,wn,d,wnq,zeta]=getFilterParams(c1,c2,c3,c4,ca,cb,fs);
            transferFunction=tf(-gain*wnq*[-1/(2*fs)^2 0 1],[1 d*wn wnq]);
        end
        function [transferFunction,zeta,wn,gain] = bpFilterTF( c1,c2,c3,c4,ca,cb,fs)
            [gain,wn,d,wnq,zeta]=getFilterParams(c1,c2,c3,c4,ca,cb,fs);
            transferFunction=tf(-gain*cb/c3*wnq/fs*[1/(2*fs) 1 0],[1 d*wn wnq]);
        end
        function [transferFunction,zeta,wn,gain]= lpFilterZTF( c1,c2,c3,c4,ca,cb,fs)
            numer=[c1*c3 0];
            [gain,wn,~,~,zeta]=getFilterParams(c1,c2,c3,c4,ca,cb,fs);
            transferFunction=generateFilterTF(numer,c1,c2,c3,c4,ca,cb,fs);
        end
        function [transferFunction,zeta,wn,gain] = bpFilterZTF( c1,c2,c3,c4,ca,cb,fs)
            numer=[-cb*c1 cb*c1 0];
            [gain,wn,~,~,zeta]=getFilterParams(c1,c2,c3,c4,ca,cb,fs);
            transferFunction=generateFilterTF(numer,c1,c2,c3,c4,ca,cb,fs);
        end
    end
end

function [gain,wn,d,wnq,zeta]=getFilterParams(c1,c2,c3,c4,ca,cb,fs)
gain=c1/c2;
wnq=fs^2/(ca*cb/c2/c3-1/4-c4/c2/2);
wn=sqrt(wnq);
d=wn/fs*c4/c2;
zeta=d/2;
end

function transF=generateFilterTF(numer,~,c2,c3,c4,ca,cb,fs)
denom=[cb*ca -2*cb*ca+c2*c3+c4*c3 -c4*c3+cb*ca];
transF=tf(numer,denom,1/fs);
end