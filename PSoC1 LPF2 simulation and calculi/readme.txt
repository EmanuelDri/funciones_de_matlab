- [transferFunction,zeta,wn] = scFilterTF( c1,c2,c3,c4,ca,cb,fs ) 
	returns the continous transfer function object corresponding to the selection of capacitors c1-c4 & ca-cb, having a sampling frequency fs. zeta is the dumping factor and wn is the natural frequency
- transferFunction = scFilterZTF( c1,c2,c3,c4,ca,cb,fs)
	This functions generates the transfer z-domain function object corresponding to the selection of capacitors c1-c4 & ca-cb, having a sampling frequency fs