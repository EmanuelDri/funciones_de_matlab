classdef TRAMTPs
    properties (Access=private)
        system;
        t;
        y;
    end
    methods
        function obj=TRAMTPs(systemOrY,t)
            if ~nargin>0
                obj.system=[];
                obj.t=[];
                obj.y=[];
            elseif issystem(systemOrY)
                obj.system=systemOrY;
                obj.y=[];
                obj.t=[];
            else
                obj.system=[];
                obj.y=y;
                if nargin==2
                    obj.t=1:length(y);
                else
                    obj.t=t;
                end
            end
        end
        
    end
    methods (Access=protected)
        function [tp,os]=calculateTpAndOSFromSys(obj)
            stepInformation=stepinfo(obj.system);
            tp=stepInformation.PeakTime;
            os=stepInformation.Overshoot;
        end
        function [tp,os]=calculateTpAndOSFromWvfrm(obj)
            maxY=max(obj.y);
            [ini,fin]=BistateAnalyzer.getBistateLevels(obj.y,'Histogram mode',299);
            os=(maxY-fin)/(fin-ini)*100;
            tp=mean(obj.t(obj.y==maxY));
        end
    end
end