classdef SimPSoC1Filter
    properties (Access=private)
        transF;
        Q;
        wn;
        gain;
        domain;
        fs;
        mode;
    end
    methods
        function obj=SimPSoC1Filter (c1,c2,c3,c4,ca,cb,fs,mode,domain)
            if nargin>1
                if contains(mode,'BP')&&contains(domain,'Z')
                    [transferFunction,zeta,omega_n,k] = PSoC1FilterTF.bpFilterZTF( c1,c2,c3,c4,ca,cb,fs);
                elseif contains(mode,'BP')&&contains(domain,'S')
                    [transferFunction,zeta,omega_n,k] = PSoC1FilterTF.bpFilterTF( c1,c2,c3,c4,ca,cb,fs);
                elseif contains(mode,'LP')&&contains(domain,'Z')
                    [transferFunction,zeta,omega_n,k] = PSoC1FilterTF.lpFilterZTF( c1,c2,c3,c4,ca,cb,fs);
                elseif contains(mode,'LP')&&contains(domain,'S')
                    [transferFunction,zeta,omega_n,k] = PSoC1FilterTF.lpFilterTF( c1,c2,c3,c4,ca,cb,fs);
                end
                obj.transF=transferFunction;
                obj.Q=1/2/zeta;
                obj.wn=omega_n;
                obj.gain=k;
                obj.domain=domain;
                obj.fs=fs;
                obj.mode=mode;
            elseif nargin==1
                obj.transF=c1;
            elseif nargin==0
                obj.transF=[];
                obj.Q=[];
                obj.wn=[];
                obj.gain=[];
                obj.domain=[];
                obj.fs=[];
                obj.mode=[];
            end
        end
        function varargout=step(obj,varargin)
            if nargout>0
                varargout{:}=step(obj.transF,varargin{:});
            else
                step(obj.transF,varargin{:});
            end
        end
        function fc=getFc(obj)
            switch obj.mode
                case 'LP'
                    fc = getGainCrossover(obj.transF,1/sqrt(2))/2/pi;
                case 'BP'
                    fc=mean(getGainCrossover(obj.transF,getPeakGain(obj.transF,1e-10)))/2/pi;
            end
        end
        function mr=getMr(obj)
            mr=(getPeakGain(obj.transF,1e-9));
        end
        function h=bodeplot(obj,varargin)
            h=bodeplot(obj.transF);
            setoptions(h,'FreqUnits','Hz',varargin{:});
        end
        function wn=getWnFromTF(obj)
            wnRes=damp(obj.transF);
            wn=wnRes(1);
        end
        function Q=getQFromTF(obj)
            [~,dp]=damp(obj.transF);
            Q=norm(1/2/dp);
        end
        function bw=getBW(obj)
            bw=range(getGainCrossover(obj.transF,db2mag(-3)))/2/pi;
        end
        function [q,wn,fc,mr_bw]=getFunctionalParams(obj)
            q=obj.getQFromTF();
            wn=obj.getWnFromTF();
            fc=obj.getFc();
            switch obj.mode
                case 'LP'
                    mr_bw=obj.getMr();
                case 'BP'
                    mr_bw=obj.getBW();
            end
        end
        function varargout=rampResponse(obj,gain,varargin)
            switch obj.domain
                case 'Z'
                    intTF=-SimIntegrator.getIntTF(obj.fs);
                case 'S'
                    intTF=SimIntegrator.getIntTF();
            end
            rampResponseExpr=intTF*gain*obj.transF;
            if nargout>0
                varargout=step(rampResponseExpr,varargin{:});
            else
                step(rampResponseExpr,varargin{:});
                title 'Ramp Response';
            end
        end
        function [tp,os,q,wn,fc,mr]=stepParams(obj,linearSystem,varargin)
            if isempty(linearSystem)
                stepInformation=stepinfo(obj.transF,varargin{:});
            else
                stepInformation=stepinfo(obj.transF*linearSystem,varargin{:});
            end
            tp=stepInformation.PeakTime;
            os=stepInformation.Overshoot;
            tau=calculateDumpingFactorFromOS(os);
            q=1/2/tau;
            wn=calculateNaturalFrequencyFromTpAndOS(tp,os);
            
            respuesta=tf(wn^2,[1 2*wn*tau wn^2]);
            fc = getGainCrossover(respuesta,db2mag(-3))/2/pi;
            wr=wn*sqrt(1-2*tau^2);
            %     mr=20*log10(getPeakGain(respuesta,1e-9));
            mr=20*log10(norm(freqresp(respuesta,wr)));
            
        end
    end
    %% getters and setters
    methods
        function transF=TF(obj)
            transF=obj.transF;
        end
        function Q=getQ(obj)
            Q=obj.Q;
        end
        function wn=getWn(obj)
            wn=obj.wn;
        end
        function gain=getK(obj)
            gain=obj.gain;
        end
        function obj=setTF(obj,TF)
            obj.transF=TF;
        end
    end
end