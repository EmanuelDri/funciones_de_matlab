function readcsvTektronixApp
folder=uigetdir;
if ~isempty(folder)
    ficheros=dir('*.csv');
    if ~isempty(ficheros)
        for fileIndex=0:(length(ficheros)-2)/2
            index1=fileIndex*2+1;
            index2=fileIndex*2+2;
            inputBuffer=csvread(ficheros(index1).name,0,3);
            xch1=inputBuffer(:,1); ych1=inputBuffer(:,2);
            inputBuffer=csvread(ficheros(index2).name,0,3);
            xch2=inputBuffer(:,1); ych2=inputBuffer(:,2);
            save([ficheros(index1).name(1:end-7) '.mat'],'xch1','ych1','xch2','ych2');
        end
    end
end