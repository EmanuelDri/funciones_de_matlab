function waveForms=tektronixCampaign(iterations)
tic
waveForms=cell(iterations,4);

% fclose(instrfind);
oscilloscope=connectTektronixOscilloscope();
pause on;
for execution=1:iterations
    disp(execution);
    [channel1TimeValues,channel1VoltageValues]= readOscilloscopeChannel(1,oscilloscope);
    [channel2TimeValues,channel2VoltageValues]= readOscilloscopeChannel(2,oscilloscope);
    waveForms(execution,:)={channel1TimeValues,channel1VoltageValues,channel2TimeValues,channel2VoltageValues};
%     pause(.002);
end
pause off;
disp(toc)
end