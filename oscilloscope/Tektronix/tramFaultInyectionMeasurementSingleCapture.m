function [waveFormsArray,estimationWaveForm]=tramFaultInyectionMeasurementSingleCapture(iterations,varargin)
tic
global waveBKPFormsArray;
global giterations;
deviceObj=connectTektronixOscilloscope();
waveFormsArray=cell(iterations,2*1);
[initialState,finalState,osVoltage,tp,xch1,ych1]=getScreenCapturesSetup(deviceObj);
transitionMiddleVoltage=(osVoltage+initialState)/2;
voltageRange=osVoltage-initialState;
voltageRange=voltageRange*1.10;
scale=voltageRange*1/8;
if scale<20e-3
    scale=20e-3;
end
% scale=.204;
[xch2,ych2]= readOscilloscopeChannel(2,deviceObj);
estimationWaveForm={xch1,ych1};

set(deviceObj.Acquisition(1), 'Mode', 'average');
set(deviceObj.Acquisition(1), 'NumberOfAverages', 128.0);

%% Setup Horizontal delay and scale
if (nargin>0)
    set(deviceObj.Acquisition(1), 'Timebase', varargin{1});
else
    set(deviceObj.Acquisition(1), 'Timebase', 250e-6);
end
% set(deviceObj.Acquisition(1), 'Delay', tp+270e-6);
set(deviceObj.Channel(1), 'Scale', scale);
tektronixGotoVoltageLevel(transitionMiddleVoltage,1,scale,deviceObj);
% set(deviceObj.Channel(1), 'Scale', .5);
% tektronixGotoVoltageLevel(2.6,1,.5,deviceObj);

for iteration=1:iterations
    giterations=iteration;
    disp(iteration);
    [xTp,yTp]=captureFullWaveForm(transitionMiddleVoltage,scale,deviceObj);
    arrayRow={xTp;yTp};    
    waveFormsArray(iteration,:)=arrayRow;
    waveBKPFormsArray=waveFormsArray;
end
    arrayRow={xch2;ych2};    
    waveFormsArray(iteration+1,:)=arrayRow;
toc

    function [initialState,finalState,osVoltage,tp,xch1,ych1]=getScreenCapturesSetup(deviceObj)
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 500e-6);
        set(deviceObj.Acquisition(1), 'Delay', 500e-6);
        voltageScale=1;
        set(deviceObj.Channel(1), 'Scale', voltageScale);
        tektronixGotoVoltageLevel(2.5,1,voltageScale,deviceObj);
        
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
        [initialState,finalState,osVoltage,tp]=estimateTRAMParameters(xch1,ych1);
    end

    function [xch1,ych1]=captureFullWaveForm(verticalPosition,scale,deviceObj)
        
        %% Start Oscilloscope Adquisition
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
       
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end


    function [xch1,ych1]=getAnAveragedSample(channel,deviceObj)
        set(deviceObj.Acquisition(1), 'Control', 'single');
        pause (128/140.1);
        [xch1,ych1]= readOscilloscopeChannel(channel,deviceObj);
    end

     function tektronixGotoVoltageLevel(voltage,channel,scale,deviceObj)
        position=-voltage/scale;
        set(deviceObj.Channel(channel), 'Position', position);
    end


end