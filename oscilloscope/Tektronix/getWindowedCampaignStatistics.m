function statisticsTable=getWindowedCampaignStatistics(waveFormsCellArray)


tableHeaders={'Estado inicial','Estado final','Tensi�n de pico','Sobreimpulso','Tiempo de pico (inicio)','Tiempo de pico (fin)','Tiempo de pico medio'};

emptyRows=cellfun(@isempty,waveFormsCellArray);
if (~isempty(find(emptyRows, 1)))
    emptyRows=emptyRows(:,1);
    capturesNumber=find(emptyRows,1,'first')-1;
else
    [capturesNumber,~]=size(waveFormsCellArray);
end

statisticsTable=cell(capturesNumber+1,length(tableHeaders));

statisticsTable(1,:)=tableHeaders;

for captureIndex=1:capturesNumber-1
    
%     iniTimeValues=waveFormsCellArray{captureIndex,1};
%     iniVoltageValues=waveFormsCellArray{captureIndex,2};
%     
%     endTimeValues=waveFormsCellArray{captureIndex,3};
%     endVoltageValues=waveFormsCellArray{captureIndex,4};
%     
%     overshootTimeValues=waveFormsCellArray{captureIndex,5};
%     overshootVoltageValues=waveFormsCellArray{captureIndex,6};
%     
%     tpTimeValues=waveFormsCellArray{captureIndex,7};
%     tpVoltageValues=waveFormsCellArray{captureIndex,8};
       
    
    timeValues=waveFormsCellArray{captureIndex,1};
    voltageValues=waveFormsCellArray{captureIndex,2};
    
     [xData, yData] = prepareCurveData( timeValues, voltageValues );
        % Set up fittype and options.
        ft = fittype( 'smoothingspline' );
        opts = fitoptions( 'Method', 'SmoothingSpline' );
        opts.Normalize = 'on';
        opts.SmoothingParam =0.9999968746273752;
        % Fit model to data.
        [fitresult, ~] = fit( xData, yData, ft, opts );
        fittedYData=feval(fitresult,timeValues);
    
%     initialStateVoltage=getInitialStateVoltage(iniTimeValues,iniVoltageValues);
%     endStateVoltage=getEndStateVoltage(endTimeValues,endVoltageValues,3e-3);
    [meanPeakTime,peakTimeStart,peakTimeEnd]=getPeakTime(tpTimeValues,tpVoltageValues);

%     initialStateVoltage=getInitialStateVoltage(timeValues,fittedYData);
%     endStateVoltage=getEndStateVoltage(timeValues,fittedYData,1e-3);
%     overshootTimeArray=timeValues(fittedYData>=max(fittedYData)-10e-3);

%         peakTimeStart=overshootTimeArray(1);
%         peakTimeEnd=overshootTimeArray(end);
%         meanPeakTime=mean(overshootTimeArray);

    peakTimePercentage=5;
%     overshootVoltage=getOvershootVoltage(timeValues,fittedYData,...
%         meanPeakTime*(1-peakTimePercentage/100),meanPeakTime*(1+peakTimePercentage/100));
    overshootVoltage=getOvershootVoltage(overshootTimeValues,overshootVoltageValues,...
        meanPeakTime*(1-peakTimePercentage/100),meanPeakTime*(1+peakTimePercentage/100));
%     overshootVoltage=getOvershootVoltage(overshootTimeValues,overshootVoltageValues,280e-6,305e-6);
    overshootPercent=calculateOvershootPercent(initialStateVoltage,endStateVoltage,overshootVoltage)
    
    tableRow={initialStateVoltage;endStateVoltage;overshootVoltage;overshootPercent;...
        peakTimeStart;peakTimeEnd;meanPeakTime};
    
    statisticsTable(captureIndex+1,:)=tableRow;
    
end

    function initialStateVoltage=getInitialStateVoltage(timeValues,voltageValues)

        
%% Fit: 'untitled fit 1'.
%          [xData, yData] = prepareCurveData( timeValues, voltageValues );
%          
%           % Set up fittype and options.
%          ft = fittype( 'poly1' );
%          opts = fitoptions( 'Method', 'LinearLeastSquares' );
%         opts.Normalize = 'on';
%         opts.Robust = 'Bisquare';
% 
%          % Fit model to data.
%         [fitresult, gof] = fit( xData, yData, ft, opts );
%          initialStateVoltage=mean(feval(fitresult,timeValues));

         initialStateVoltage=mean(voltageValues(timeValues<0));
    end

    function endStateVoltage=getEndStateVoltage(timeValues,voltageValues,settlingTime)
%         endStateVoltage=mean(voltageValues(timeValues>settlingTime));
%         [xData, yData] = prepareCurveData( timeValues, voltageValues );
%          
%          % Set up fittype and options.
%          ft = fittype( 'poly1' );
%          opts = fitoptions( 'Method', 'LinearLeastSquares' );
%         opts.Normalize = 'on';
%         opts.Robust = 'Bisquare';
% 
%          % Fit model to data.
%         [fitresult, gof] = fit( xData, yData, ft, opts );
%          endStateVoltage=mean(feval(fitresult,timeValues));
         endStateVoltage=mean(voltageValues(timeValues>settlingTime));
    end

    function overshootVoltage=getOvershootVoltage(timeValues,voltageValues,timeWindowStart,timeWindowEnd)
        
        overshootVoltage=mean(voltageValues(timeValues>=timeWindowStart &...
            timeValues<=timeWindowEnd));
        
        %{
        [xData, yData] = prepareCurveData( timeValues, voltageValues );
        % Set up fittype and options.firefox
        
        ft = fittype( 'smoothingspline' );
        opts = fitoptions( 'Method', 'SmoothingSpline' );
        opts.Normalize = 'on';
%         opts.SmoothingParam = 0.998410273359396;
            opts.SmoothingParam = 0.8746377451362785;
            opts.SmoothingParam = 0.898069292936176;
        % Fit model to data.
        [fitresult, gof] = fit( xData, yData, ft, opts );
        timeWindow=timeValues(timeValues<=timeWindowEnd&timeValues>=timeWindowStart);
        overshootVoltage=max(feval(fitresult,timeWindow));
%}
%{
    voltageHistogram=tabulate(voltageValues);
    histogramPercentage=.1;
    histogramHigherPartIndex=floor(length(voltageHistogram)*(1-histogramPercentage));
    histogramVoltages=voltageHistogram(histogramHigherPartIndex:end,1);
    histogramFrequencies=voltageHistogram(histogramHigherPartIndex:end,2);
    overshootVoltage=mean(histogramVoltages(histogramFrequencies==max(histogramFrequencies)));
  %}      
    end

    function [meanPeakTime,peakTimeStart,peakTimeEnd]=getPeakTime(timeValues,voltageValues)
%                 overshootTimeArray=timeValues(voltageValues==max(voltageValues));
               %{ 
                [envHigh, envLow] = envelope(voltageValues,240,'peak');
                meanEnvelope=((envLow+envHigh)/2);
                meanEnvelope=meanEnvelope(timeValues>10e-6);
                auxiliarTimeArray=timeValues(timeValues>10e-6);
                overshootTimeArray=(auxiliarTimeArray(meanEnvelope>=max(meanEnvelope)-20e-3));
                %}
%{
        highestVoltagesArray=sort(unique(voltageValues));
        meanPeakTime=0;
        for voltage=highestVoltagesArray(end-20:end)
            overshootTimeArray=timeValues(voltageValues==voltage);
            meanPeakTime=(mean(overshootTimeArray)+meanPeakTime)/2;
        end
%}
%{
        decimationFactor=70;
        tiempos=decimate(timeValues,decimationFactor);
        tensiones=decimate(voltageValues,decimationFactor);
        [xData, yData] = prepareCurveData( tiempos, tensiones );

        ft = 'splineinterp';

        [fitresult, ~] = fit( xData, yData, ft, 'Normalize', 'on' );
        
        fittedYData=feval(fitresult,timeValues);
        overshootTimeArray=timeValues(fittedYData>=max(fittedYData)-10e-3);
        
 %}

        [xData, yData] = prepareCurveData( timeValues, voltageValues );
        % Set up fittype and options.
        ft = fittype( 'smoothingspline' );
        opts = fitoptions( 'Method', 'SmoothingSpline' );
        opts.Normalize = 'on';
%         opts.SmoothingParam = 0.998410273359396;
        opts.SmoothingParam = 0.8746377451362785;
        opts.SmoothingParam = 0.898069292936176;
        % Fit model to data.
        [fitresult, ~] = fit( xData, yData, ft, opts );
        fittedYData=feval(fitresult,timeValues);
        overshootTimeArray=timeValues(fittedYData>=max(fittedYData)-0e-3);

        peakTimeStart=overshootTimeArray(1);
        peakTimeEnd=overshootTimeArray(end);
        meanPeakTime=mean(overshootTimeArray);
    end

    function overshootPercent=calculateOvershootPercent(initialState,endState,peakVoltage)
        overshootPercent=(peakVoltage-endState)/(endState-initialState)*100;
    end

end
