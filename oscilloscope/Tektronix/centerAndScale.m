function centerAndScale
goToVoltage(2.5,1,1);
goToVoltage(2.5,2,1);
[~,ych1,~,ych2]=getOscilloscopeCapture;
scale1=(max(ych1)-min(ych1))/8*1.1;
scale2=(max(ych2)-min(ych2))/8*1.1;
offset1=(max(ych1)+min(ych1))/2;
offset2=(max(ych2)+min(ych2))/2;
goToVoltage(offset1,1,scale1);
goToVoltage(offset2,2,scale2);
end