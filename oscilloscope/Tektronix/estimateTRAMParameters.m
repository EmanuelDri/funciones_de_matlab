function [initialState,endState,osVoltage,tp]=estimateTRAMParameters(xTimes,yVoltages)
[initialState,endState]=getBistateLevels(yVoltages);
osVoltage=max(yVoltages);
tp=mean(xTimes(yVoltages==osVoltage));
end
