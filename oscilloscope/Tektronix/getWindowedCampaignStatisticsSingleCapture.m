function [statisticsTable,goodnessOfFit]=getWindowedCampaignStatisticsSingleCapture(waveFormsCellArray)

tableHeaders={'Sobreimpulso','Tiempo de pico medio','Ganancia','qp','wn','Fc','Mr'};
emptyRows=cellfun(@isempty,waveFormsCellArray);
if (~isempty(find(emptyRows, 1)))
    emptyRows=emptyRows(:,1);
    capturesNumber=find(emptyRows,1,'first')-1;
else
    [capturesNumber,~]=size(waveFormsCellArray);
end

statisticsTable(1,:)=tableHeaders;
goodnessOfFit=cell(capturesNumber,2);

parfor captureIndex=1:capturesNumber
    
    outputTimeValues=waveFormsCellArray{captureIndex,1}; outputTimeValues=outputTimeValues(1:end-10);
    outputVoltageValues=waveFormsCellArray{captureIndex,2}; outputVoltageValues=outputVoltageValues(1:end-10);
    
    inputTimeValues=waveFormsCellArray{captureIndex,3}; inputTimeValues=inputTimeValues(1:end-10);
    inputVoltageValues=waveFormsCellArray{captureIndex,4}; inputVoltageValues=inputVoltageValues(1:end-10);
    
    smoothingFactor=0.9999536367214412; %este era, por cierto
    [fittedYData,gof,fitOutput]=generateSpline(outputTimeValues,outputVoltageValues,smoothingFactor);
    
    [overshootPercent,meanPeakTime,gain,wn,qp,f0,mr]=calculateTPs(outputTimeValues,fittedYData,inputTimeValues,inputVoltageValues);
    
    tableRow={overshootPercent;meanPeakTime;gain;qp;wn;f0;mr};
    
    statisticsTable(captureIndex+1,:)=tableRow;
    goodnessOfFit(captureIndex,:)={gof,fitOutput};
end
end

function [overshootPercent,meanPeakTime,gain,wn,qp,f0,mr]=calculateTPs(timeValues,voltageValues,inputTimeValues,inputVoltageValues)
[initialStateVoltage,endStateVoltage]=getBistateLevels(voltageValues,'Histogram mode',199);
[inputLowState,inputHighState]=getBistateLevels(inputVoltageValues,'Histogram mode',299);

overshootVoltage=max(voltageValues);
overshootTimeArray=timeValues(voltageValues>=max(voltageValues));
tin=midcross(inputVoltageValues,inputTimeValues);
%% Vamos a probar c�mo responde el algoritmo si obtenemos el tiempo de pico desde step en dsp.transitionMetrics
meanPeakTime=mean(overshootTimeArray)-tin;
% meanPeakTime=getPeakTimeFromStepFunction(voltageValues,timeValues,initialStateVoltage,endStateVoltage)-tin;

overshootPercent=calculateOvershootPercent(initialStateVoltage,endStateVoltage,overshootVoltage);

responseAmplitude=endStateVoltage-initialStateVoltage;
inputStepAmplitude=inputHighState-inputLowState;

%gain=responseAmplitude/1.3;%inputStepAmplitude;
 gain=responseAmplitude/inputStepAmplitude;

tau=calculateDumpingFactorFromOS(overshootPercent);
qp=1/2/tau;
wn=calculateNaturalFrequencyFromTpAndOS(meanPeakTime,overshootPercent);

respuesta=tf(wn^2,[1 2*wn*tau wn^2]);
f0 = getGainCrossover(respuesta,db2mag(-3))/2/pi;
wr=wn*sqrt(1-2*tau^2);
%     mr=20*log10(getPeakGain(respuesta,1e-9));
mr=20*log10(norm(freqresp(respuesta,wr)));
end

function peakTime=getPeakTimeFromStepFunction(ych1,xch1,ini,fin)
transitionMetricsObject = dsp.TransitionMetrics('TimeInputPort',true, ...
'StateLevels',[ini fin], ...
'PostshootOutputPort',true, ...
'SettlingOutputPort',true, ...
'SettlingSeekDuration',2e-4);

xch1=reshape(xch1,[],1);ych1=reshape(ych1,[],1);
[~,transitionMetrics]=step(transitionMetricsObject,ych1,xch1);
peakTime=transitionMetrics.OvershootInstant;
end

function overshootPercent=calculateOvershootPercent(initialState,endState,peakVoltage)
overshootPercent=(peakVoltage-endState)/(endState-initialState)*100;
end