function setupCursorMeasurement(characteristic)

% Create a VISA-USB object.
interfaceObj = instrfind('Type', 'visa-usb', 'RsrcName', 'USB0::0x0699::0x0362::C010188::0::INSTR', 'Tag', '');

% Create the VISA-USB object if it does not exist
% otherwise use the object that was found.
if isempty(interfaceObj)
    interfaceObj = visa('TEK', 'USB0::0x0699::0x0362::C010188::0::INSTR');
else
    fclose(interfaceObj);
    interfaceObj = interfaceObj(1);
end

% Create a device object. 
deviceObj = icdevice('tektronix_tds1002.mdd', interfaceObj);

% Connect device object to hardware.
connect(deviceObj);

switch characteristic
    case 'ini'
        set(deviceObj.Acquisition(1), 'State', 'run');
        set(deviceObj.Acquisition(1), 'Timebase', 0.01);
        set(deviceObj.Acquisition(1), 'Delay', -0.0375);
        set(deviceObj.Channel(1), 'Position', -72.5);
        set(deviceObj.Acquisition(1), 'Mode', 'average');
        set(deviceObj.Acquisition(1), 'NumberOfAverages', 128.0);
        set(deviceObj.Acquisition(1), 'Control', 'single');
    case 'end'
        set(deviceObj.Acquisition(1), 'State', 'run');
        set(deviceObj.Acquisition(1), 'Timebase', 0.01);
        set(deviceObj.Acquisition(1), 'Delay', 0.0324);
        set(deviceObj.Channel(1), 'Position', -186);
        set(deviceObj.Acquisition(1), 'Mode', 'average');
        set(deviceObj.Acquisition(1), 'NumberOfAverages', 128.0);
        set(deviceObj.Acquisition(1), 'Control', 'single');
    case 'peak'
        set(deviceObj.Acquisition(1), 'State', 'run');
        set(deviceObj.Acquisition(1), 'Timebase', 10e-6);
        set(deviceObj.Acquisition(1), 'Delay', 284e-6);
        set(deviceObj.Channel(1), 'Position', -207);
        set(deviceObj.Acquisition(1), 'Mode', 'average');
        set(deviceObj.Acquisition(1), 'NumberOfAverages', 128.0);
        set(deviceObj.Acquisition(1), 'Control', 'single');
end

disconnect(deviceObj);

end