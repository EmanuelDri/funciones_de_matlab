function [resultados]=barridoEnFrecuenciaManual(nombreArchivo)
resultados=cell(0,5);
delete(instrfind);
oscilloscopeHandler=connectTektronixOscilloscope();
deviceObj=oscilloscopeHandler;
% set(deviceObj.Measurement(2), 'MeasurementType', 'frequency');
% set(deviceObj.Measurement(2), 'Source', 'channel2');
% measuredFrequency = get(deviceObj.Measurement(2), 'Value');
adjustChannel(2,oscilloscopeHandler);
adjustChannel(1,oscilloscopeHandler);

frecuencia=input('frecuencia: ');
while ~isempty(frecuencia)
    period=1/frecuencia;
    timeBase=3.2*period/10;
    set(deviceObj.Acquisition, 'Timebase', timeBase);
    adjustChannel(2,oscilloscopeHandler);
    adjustChannel(1,oscilloscopeHandler);
    set(oscilloscopeHandler.Acquisition(1), 'Control', 'run-stop');
    set(oscilloscopeHandler.Acquisition(1), 'State', 'run');
    set(oscilloscopeHandler.Acquisition(1), 'Control', 'single');
    while strcmp(get(oscilloscopeHandler.Acquisition(1), 'State'),'run')
    end
    [xch1,ych1]=readOscilloscopeChannel(1,oscilloscopeHandler);
    [xch2,ych2]=readOscilloscopeChannel(2,oscilloscopeHandler);
    disp('Datos capturados ?');
    resultados(end+1,:)={xch1;ych1;xch2;ych2;frecuencia};
    set(oscilloscopeHandler.Acquisition(1), 'Control', 'run-stop');
    set(oscilloscopeHandler.Acquisition(1), 'State', 'run');
    sound(rand(1,200));
    frecuencia=input('frecuencia: ');
end
% nombreArchivo=input('Nombre del archivo: ','s');
save([nombreArchivo '.mat'],'resultados');
disp(['Datos guardados en archivo ' nombreArchivo '.mat']);
sound(rand(1,512));
set(oscilloscopeHandler.Acquisition(1), 'Control', 'run-stop');
set(oscilloscopeHandler.Acquisition(1), 'State', 'run');

    function adjustChannel(channelNumber,oscilloscopeHandler)
        goToVoltage(0,channelNumber,1);
        [~,amplitudes]=readOscilloscopeChannel(1,oscilloscopeHandler);
        setAmplitude=max(amplitudes)-min(amplitudes);
        if setAmplitude<1
            goToVoltage(0,channelNumber,.13);
            [~,amplitudes]=readOscilloscopeChannel(1,oscilloscopeHandler);
            setAmplitude=max(amplitudes)-min(amplitudes);
        end
        setOffset=(max(amplitudes)+min(amplitudes))/2;
        goToVoltage(setOffset,channelNumber,setAmplitude/7.5);
    end

end
