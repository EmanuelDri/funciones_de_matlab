% Create a VISA-USB object.
interfaceObj = instrfind('Type', 'visa-usb', 'RsrcName', 'USB::0x0699::0x0362::C010188::INSTR', 'Tag', '');

% Create the VISA-USB object if it does not exist
% otherwise use the object that was found.
if isempty(interfaceObj)
    interfaceObj = visa('TEK', 'USB::0x0699::0x0362::C010188::INSTR');
else
    fclose(interfaceObj);
    interfaceObj = interfaceObj(1);
end

% Create a device object. 
deviceObj = icdevice('tektronix_tds1002.mdd', interfaceObj);

% Connect device object to hardware.
connect(deviceObj);

% Query property value(s).
get1 = get(deviceObj.Cursor(1), 'HorizontalBarPosition1');
get2 = get(deviceObj.Cursor(1), 'HorizontalBarPosition2');

disp(strcat('Cursor 1: ',num2str(get1)));
disp(strcat('Cursor 2: ',num2str(get2)));
cursores={get1,get2};