function [resultados]=obtenerCapturas(nroMuestras,nombreArchivo)
%% SECTION TITLE
% 
%  This function is the one we use to collect measurements from
%  oscilloscopes
%  
% 
% DESCRIPTIVE TEXT

resultados=cell(nroMuestras,4);
delete(instrfind);
oscilloscopeHandler=connectTektronixOscilloscope();
% adjustChannel(2,oscilloscopeHandler);
% adjustChannel(1,oscilloscopeHandler);

for index=1:nroMuestras
    disp(index);
    set(oscilloscopeHandler.Acquisition(1), 'Control', 'run-stop');
    set(oscilloscopeHandler.Acquisition(1), 'State', 'run');
    set(oscilloscopeHandler.Acquisition(1), 'Control', 'single');
    while strcmp(get(oscilloscopeHandler.Acquisition(1), 'State'),'run')
    end
    [xch1,ych1]=readOscilloscopeChannel(1,oscilloscopeHandler);
    [xch2,ych2]=readOscilloscopeChannel(2,oscilloscopeHandler);
    resultados(index,:)={xch1;ych1;xch2;ych2};
end
% nombreArchivo=input('Nombre del archivo: ','s');
save([nombreArchivo '.mat'],'resultados');
sound(rand(1,512));
set(oscilloscopeHandler.Acquisition(1), 'Control', 'run-stop');
set(oscilloscopeHandler.Acquisition(1), 'State', 'run');
sound(rand(1,200));

    function adjustChannel(channelNumber,oscilloscopeHandler)
        [setOffset,setAmplitude]=determineSignalAttr(channelNumber,2.5,2,oscilloscopeHandler);
        if setAmplitude<8
            [setOffset,setAmplitude]=determineSignalAttr(channelNumber,2.5,1,oscilloscopeHandler);
        end
        if setAmplitude<4
            [setOffset,setAmplitude]=determineSignalAttr(channelNumber,2.5,0.5,oscilloscopeHandler);
        end
        if setAmplitude<0.2
            [setOffset,setAmplitude]=determineSignalAttr(channelNumber,setOffset,0.1,oscilloscopeHandler);
        end
        if setAmplitude<0.05
            [setOffset,setAmplitude]=determineSignalAttr(channelNumber,setOffset,0.05,oscilloscopeHandler);
            setAmplitude=setAmplitude*7.6;
        end
        goToVoltage(setOffset/1.03,channelNumber,setAmplitude/7.6);
    end

    function [setOffset,setAmplitude]=determineSignalAttr(chN,offset,voltsDiv,oscObj)
        amplitudes=makeAnAcquisition(chN,offset,voltsDiv,oscObj);
        setAmplitude=max(amplitudes)-min(amplitudes);
        setOffset=(max(amplitudes)+min(amplitudes))/2;
    end
        
    function amplitudes=makeAnAcquisition(chNumber,offset,voltsDiv,oscObj)
        set(oscObj.Acquisition(1), 'Control', 'run-stop');
        set(oscObj.Acquisition(1), 'State', 'run');
        goToVoltage(offset,chNumber,voltsDiv);
        [~,amplitudes]=readOscilloscopeChannel(1,oscObj);
    end

end
