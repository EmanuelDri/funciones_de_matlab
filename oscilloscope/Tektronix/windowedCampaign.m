function waveFormsArray=windowedCampaign(iterations)
%% windowedCampaign
% This function gets OS ans Tp statistics from a single filter showed on
% the oscilloscope screen. Channel 1 must contain filter's output wave form
% and Channel 2 the instrument trigger wave form

% tableHeaders={'Estado inicial','Estado final','Tensi�n de pico','Sobreimpulso','Tiempo de pico (inicio)','Tiempo de pico (fin)','Tiempo de pico medio'};
waveFormsArray=cell(iterations,2*4); %Four waveforms are saved, the one
%used in initial and end state measurements, peak value measurement and
%peak time measurement

oscilloscope=connectTektronixOscilloscope();
pause on;
for execution=1:iterations
    disp(strcat('Ini ',num2str(execution)));
    [xch1Ini,ych1Ini]=captureInitialState(oscilloscope);
    disp(strcat('End ',num2str(execution)));
    [xch1End,ych1End]=captureEndState(oscilloscope);
    disp(strcat('Peak ',num2str(execution)));
    [xch1Pk,ych1Pk]=capturePeakVoltage(oscilloscope);
    disp(strcat('PeakTime ',num2str(execution)));
    [xch1Tp,ych1Tp]=capturePeakTime(oscilloscope);
    waveFormsArray(execution,:)={xch1Ini;ych1Ini;xch1End;ych1End;xch1Pk;ych1Pk;xch1Tp;ych1Tp};
    
end
pause off;

    function [xch1,ych1]=captureInitialState(deviceObj)
        %% Start Oscilloscope Adquisition
%         set(deviceObj.Acquisition(1), 'Mode', 'sample');
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 0.005);
        set(deviceObj.Acquisition(1), 'Delay', -20e-3);
%         set(deviceObj.Channel(1), 'Scale', 1);
%         
%         %% Measure channel 1 min value
%         tektronixGotoVoltageLevel(2.5,1);
%         channel1Min=getChannelMinValue(1,deviceObj);
%         
%         %% Measure channel 1 mean value
        set(deviceObj.Channel(1), 'Scale', 20e-3);
%         tektronixGotoVoltageLevel(channel1Min,1);
%         channe1Mean=getChannelMeanValue(1,deviceObj);
        tektronixGotoVoltageLevel(1.14,1,20e-3,deviceObj);
        
        
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end

    function [xch1,ych1]=captureEndState(deviceObj)
        %% Start Oscilloscope Adquisition
%         set(deviceObj.Acquisition(1), 'Mode', 'sample');
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 0.005);
        set(deviceObj.Acquisition(1), 'Delay', 23.4e-3);
%         set(deviceObj.Channel(1), 'Scale', 1);
%         
%         %% Measure channel 1 min value
%         tektronixGotoVoltageLevel(2.5,1);
%         channel1Max=getChannelMaxValue(1,deviceObj);
%         
%         %% Measure channel 1 mean value
        set(deviceObj.Channel(1), 'Scale', 20e-3);
        tektronixGotoVoltageLevel(2.41,1,20e-3,deviceObj);
%         channe1Mean=getChannelMeanValue(1,deviceObj);
%         tektronixGotoVoltageLevel(channe1Mean,1);
%         channel1Min=getChannelMinValue(1,deviceObj);
%         tektronixGotoVoltageLevel(channel1Min,1);
        
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end

    function [xch1,ych1]=capturePeakVoltage(deviceObj)
        %% Start Oscilloscope Adquisition
%         set(deviceObj.Acquisition(1), 'Mode', 'sample');
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 50e-6);
        set(deviceObj.Acquisition(1), 'Delay', 225e-6);
%         set(deviceObj.Channel(1), 'Scale', 1);
        
        %% Measure channel 1 min value
%         tektronixGotoVoltageLevel(2.5,1);
%         channel1Max=getChannelMaxValue(1,deviceObj);
        
        %% Measure channel 1 mean value
        set(deviceObj.Channel(1), 'Scale', 20e-6);
%         tektronixGotoVoltageLevel(channel1Max,1);
%         channe1Max=getChannelMaxValue(1,deviceObj);
        tektronixGotoVoltageLevel(2.6,1,20e-3,deviceObj);
        
        set(deviceObj.Acquisition(1), 'Timebase', 10e-6);
        set(deviceObj.Acquisition(1), 'Delay', 280e-6);
        
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end

    function [xch1,ych1]=capturePeakTime(deviceObj)
        %% Start Oscilloscope Adquisition
%         set(deviceObj.Acquisition(1), 'Mode', 'sample');
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 50e-6);
        set(deviceObj.Acquisition(1), 'Delay', 280e-6);
        set(deviceObj.Channel(1), 'Scale', 0.2);
        
        tektronixGotoVoltageLevel(2.5,1,0.2,deviceObj);

        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end

    function tektronixGotoVoltageLevel(voltage,channel,scale,deviceObj)
        position=-voltage/scale;
        set(deviceObj.Channel(channel), 'Position', position);
    end

    function channelMinValue=getChannelMinValue(channel,deviceObj)
        set(deviceObj.Measurement(3), 'MeasurementType', 'minimum');
        set(deviceObj.Measurement(3), 'Source', strcat('channel',num2str(channel)));
                        pause(.5);
        channelMinValue = get(deviceObj.Measurement(3), 'Value');

    end

    function channelMinValue=getChannelMaxValue(channel,deviceObj)
        set(deviceObj.Measurement(3), 'MeasurementType', 'maximum');
        set(deviceObj.Measurement(3), 'Source', strcat('channel',num2str(channel)));
                pause(.5);
        channelMinValue = get(deviceObj.Measurement(3), 'Value');
    end

    function channelMeanValue=getChannelMeanValue(channel,deviceObj)

        set(deviceObj.Measurement(3), 'MeasurementType', 'mean');
        set(deviceObj.Measurement(3), 'Source', strcat('channel',num2str(channel)));
                pause(.5);
        channelMeanValue = get(deviceObj.Measurement(3), 'Value');
    end

    function [xch1,ych1]=getAnAveragedSample(channel,deviceObj)
        set(deviceObj.Acquisition(1), 'Mode', 'average');
        set(deviceObj.Acquisition(1), 'NumberOfAverages', 128.0);
        set(deviceObj.Acquisition(1), 'Control', 'single');
        
        pause (18);
        
        [xch1,ych1]= readOscilloscopeChannel(channel,deviceObj);
    end

end