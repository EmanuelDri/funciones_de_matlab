function [xch1,ych1,xch2,ych2]=getOscilloscopeCapture
deviceObj=connectTektronixOscilloscope();
[xch1,ych1]= readOscilloscopeChannel(1,deviceObj);
[xch2,ych2]= readOscilloscopeChannel(2,deviceObj);
end