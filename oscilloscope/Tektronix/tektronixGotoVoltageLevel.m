function tektronixGotoVoltageLevel(voltage,channel)

interfaceObj = instrfind('Type', 'visa-usb', 'RsrcName', 'USB0::0x0699::0x0362::C010188::0::INSTR', 'Tag', '');

% Create the VISA-USB object if it does not exist
% otherwise use the object that was found.
if isempty(interfaceObj)
    interfaceObj = visa('TEK', 'USB0::0x0699::0x0362::C010188::0::INSTR');
else
    fclose(interfaceObj);
    interfaceObj = interfaceObj(1);
end

% Create a device object. 
deviceObj = icdevice('tektronix_tds1002.mdd', interfaceObj);

% Connect device object to hardware.
connect(deviceObj);

% Query property value(s).
scale = get(deviceObj.Channel(channel), 'Scale');

position=-voltage/scale;

set(deviceObj.Channel(channel), 'Position', position);

% disconnect(deviceObj);
end