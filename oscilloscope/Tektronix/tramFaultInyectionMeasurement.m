function [waveFormsArray,estimationWaveForm]=tramFaultInyectionMeasurement(iterations)
tic
global waveBKPFormsArray;
global giterations;
deviceObj=connectTektronixOscilloscope();
waveFormsArray=cell(iterations,2*4);
[initialState,finalState,osVoltage,tp,xch1,ych1]=getScreenCapturesSetup(deviceObj);
estimationWaveForm={xch1,ych1};
for iteration=1:iterations
    giterations=iteration;
    disp(iteration);
    [xIni,yIni]=captureInitialState(initialState,deviceObj);
    [xEnd,yEnd]=captureEndState(finalState,deviceObj);
    [xTp,yTp]=capturePeakTime(osVoltage,tp,deviceObj);
    refinedOsVoltage=max(yTp);
    refinedTp=mean(xTp(yTp==refinedOsVoltage));
    [xPeak,yPeak]=capturePeakVoltage(refinedOsVoltage,refinedTp,deviceObj);
    arrayRow={xIni;yIni;xEnd;yEnd;xPeak;yPeak;xTp;yTp};
%     arrayRow={xTp;yTp};    
    waveFormsArray(iteration,:)=arrayRow;
    waveBKPFormsArray=waveFormsArray;
end
toc

    function [initialState,finalState,osVoltage,tp,xch1,ych1]=getScreenCapturesSetup(deviceObj)
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 500e-6);
        set(deviceObj.Acquisition(1), 'Delay', 500e-6);
        
        set(deviceObj.Channel(1), 'Scale', 500e-3);
        tektronixGotoVoltageLevel(2.5,1,500e-3,deviceObj);
        
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
        [initialState,finalState,osVoltage,tp]=estimateTRAMParameters(xch1,ych1);
    end

    function [xch1,ych1]=captureInitialState(initialStateVoltage,deviceObj)
        %% Start Oscilloscope Adquisition
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 0.0005);
        set(deviceObj.Acquisition(1), 'Delay', -20e-3);
        set(deviceObj.Channel(1), 'Scale', 20e-3);

        tektronixGotoVoltageLevel(initialStateVoltage,1,20e-3,deviceObj);
        
        
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end

    function [xch1,ych1]=captureEndState(finalStateVoltage,deviceObj)
        %% Start Oscilloscope Adquisition
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 0.0005);
        set(deviceObj.Acquisition(1), 'Delay', 4e-3);
        set(deviceObj.Channel(1), 'Scale', 20e-3);
        tektronixGotoVoltageLevel(finalStateVoltage,1,20e-3,deviceObj);
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end

    function [xch1,ych1]=capturePeakVoltage(osVoltage,tp,deviceObj)
        %% Start Oscilloscope Adquisition
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Channel(1), 'Scale', 20e-6);

        tektronixGotoVoltageLevel(osVoltage,1,20e-3,deviceObj);
        
        set(deviceObj.Acquisition(1), 'Timebase', 10e-6);
        set(deviceObj.Acquisition(1), 'Delay', tp);
        
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end

    function [xch1,ych1]=capturePeakTime(osVoltage,tp,deviceObj)
        
        %% Start Oscilloscope Adquisition
        set(deviceObj.Acquisition(1), 'Control', 'run-stop');
        set(deviceObj.Acquisition(1), 'State', 'run');
        
        %% Setup Horizontal delay and scale
        set(deviceObj.Acquisition(1), 'Timebase', 50e-6);
        set(deviceObj.Acquisition(1), 'Delay', tp);
        set(deviceObj.Channel(1), 'Scale', 0.2);
        
        tektronixGotoVoltageLevel(osVoltage,1,0.2,deviceObj);
        %{

        set(deviceObj.Acquisition(1), 'Timebase', 250e-6);
        set(deviceObj.Acquisition(1), 'Delay', 200e-6);
        set(deviceObj.Channel(1), 'Scale', 0.2);
        
        tektronixGotoVoltageLevel(2.06,1,0.2,deviceObj);
        %}
        [xch1,ych1]=getAnAveragedSample(1,deviceObj);
        
    end


    function [xch1,ych1]=getAnAveragedSample(channel,deviceObj)
        set(deviceObj.Acquisition(1), 'Mode', 'average');
        set(deviceObj.Acquisition(1), 'NumberOfAverages', 128.0);
        set(deviceObj.Acquisition(1), 'Control', 'single');
        
        pause (2.2);
        
        [xch1,ych1]= readOscilloscopeChannel(channel,deviceObj);
    end

     function tektronixGotoVoltageLevel(voltage,channel,scale,deviceObj)
        position=-voltage/scale;
        set(deviceObj.Channel(channel), 'Position', position);
    end


end