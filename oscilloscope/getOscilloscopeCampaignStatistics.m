function [tpStatistics,osStatistics,offsetStatistics,osVoltageErrorStatistics,tpArray,osArray,offsetArray]=getOscilloscopeCampaignStatistics(waveFormsCellArray)
[capturesNumber,~]=size(waveFormsCellArray);
headers={'Promedio','Mediana','Moda','M�nimo','M�ximo','Desviaci�n Est�ndar'};

tpArray=zeros(1,capturesNumber);
osArray=zeros(1,capturesNumber);
offsetArray=zeros(1,capturesNumber);
osVoltageErrorArray=zeros(1,capturesNumber);
tll=zeros(1,capturesNumber);
thl=zeros(1,capturesNumber);
osv=zeros(1,capturesNumber);

theoreticalOS=15.6870623264232;

% figure;
% hold on;

for captureIndex=1:capturesNumber
    %% On channel 1 the transient responsewas measured , on channel 2 the input stimulus
    
    xChannel1=double(waveFormsCellArray{captureIndex,1});
    yChannel1=double(waveFormsCellArray{captureIndex,2});
    xChannel2=double(waveFormsCellArray{captureIndex,3});
    yChannel2=double(waveFormsCellArray{captureIndex,4});
    disp(length(unique(yChannel1)));
%     plot(xChannel1,yChannel1,xChannel2,yChannel2);
    
    [peakTime,overshootPercent,~]=determineOvershootAndPeakTime(xChannel1,yChannel1);
    overshootPercent=overshootPercent*100;
    [transitionLowStateVoltage,transitionHighStateVoltage]=getBistateLevels(yChannel1);
    if transitionLowStateVoltage<2
        offsetVoltage=transitionLowStateVoltage-1.2;
    else
        offsetVoltage=transitionLowStateVoltage-2.5;
    end
    [thresholdTime,~]=middleCross(xChannel2,yChannel2);
    
    [overshootPercent,~,peakTime]=overshoot(yChannel1,xChannel1);
%     peakTime=peakTime-thresholdTime;
%     disp(overshootVoltage);
    
    tpArray(captureIndex)=peakTime*1e6;
%     osArray(captureIndex)=overshootPercent*100;
    
    osArray(captureIndex)=overshootPercent;
    offsetArray(captureIndex)=offsetVoltage*1000;
    
    tll(captureIndex)=transitionLowStateVoltage;
    thl(captureIndex)=transitionHighStateVoltage;
    
    
    measuredStepInputSize=transitionHighStateVoltage-transitionLowStateVoltage;
    theoreticalOSAmplitude=measuredStepInputSize*(theoreticalOS/100);
    osVoltage=max(yChannel1); osAmplitude=osVoltage-transitionHighStateVoltage;
%     disp(transitionHighStateVoltage);
    osVoltageErrorArray(captureIndex)=errorR(osAmplitude,theoreticalOSAmplitude);
    
    osv(captureIndex)=osVoltage;
end

figure,hist(tll,1000),title('low state');
figure,hist(thl,1000),title('high state');
figure,hist(osv,1000),title('overshoot voltage');

tpStatistics=cell(1,length(headers));
osStatistics=cell(1,length(headers));
osVoltageErrorStatistics=cell(1,length(headers));
offsetStatistics=cell(1,length(headers));

tpMean=mean(tpArray); tpStatistics{1,1}=tpMean;
tpMedian=median(tpArray); tpStatistics{1,2}=tpMedian;
tpMode=mode(tpArray); tpStatistics{1,3}=tpMode;
tpMin=min(tpArray); tpStatistics{1,4}=tpMin;
tpMax=max(tpArray); tpStatistics{1,5}=tpMax;
tpSD=std(tpArray); tpStatistics{1,6}=tpSD;
tpSDPercent=tpSD/tpMean*100; tpStatistics{1,7}=tpSDPercent;

osMean=mean(osArray); osStatistics{1,1}=osMean;
osMedian=median(osArray); osStatistics{1,2}=osMedian;
osMode=mode(osArray); osStatistics{1,3}=osMode;
osMin=min(osArray); osStatistics{1,4}=osMin;
osMax=max(osArray); osStatistics{1,5}=osMax;
osSD=std(osArray); osStatistics{1,6}=osSD;
osSDPercent=osSD/osMean*100; osStatistics{1,7}=osSDPercent;

osVoltageErrorMean=mean(osVoltageErrorArray); osVoltageErrorStatistics{1,1}=osVoltageErrorMean;
osVoltageErrorMedian=median(osVoltageErrorArray); osVoltageErrorStatistics{1,2}=osVoltageErrorMedian;
osVoltageErrorMode=mode(osVoltageErrorArray); osVoltageErrorStatistics{1,3}=osVoltageErrorMode;
osVoltageErrorMin=min(osVoltageErrorArray); osVoltageErrorStatistics{1,4}=osVoltageErrorMin;
osVoltageErrorMax=max(osVoltageErrorArray); osVoltageErrorStatistics{1,5}=osVoltageErrorMax;
osVoltageErrorSD=std(osVoltageErrorArray); osVoltageErrorStatistics{1,6}=osVoltageErrorSD;
osVoltageErrorSDPercent=osVoltageErrorSD/osVoltageErrorMean*100; osVoltageErrorStatistics{1,7}=osVoltageErrorSDPercent;

offsetMean=mean(offsetArray); offsetStatistics{1,1}=offsetMean;
offsetMedian=median(offsetArray); offsetStatistics{1,2}=offsetMedian;
offsetMode=mode(offsetArray); offsetStatistics{1,3}=offsetMode;
offsetMin=min(offsetArray); offsetStatistics{1,4}=offsetMin;
offsetMax=max(offsetArray); offsetStatistics{1,5}=offsetMax;
offsetSD=std(offsetArray); offsetStatistics{1,6}=offsetSD;
offsetSDPercent=offsetSD/offsetMean*100; offsetStatistics{1,7}=offsetSDPercent;

end