function [tpStatistics,osStatistics,offsetStatistics,osVoltageErrorStatistics]=formatOscilloscopeCampaigns()

filePath=uigetdir();
tic
if filePath~=0
    oldFolder=cd(filePath);
    matFiles=dir('*.mat');
    [filesNumber,~]=size(matFiles);
    
    disp(filesNumber);
    
    headers={'Promedio','Mediana','Moda','M�nimo','M�ximo','Desviaci�n Est�ndar','Desviaci�n Est�ndar %','Filtro'};
    tpStatistics=cell(filesNumber,length(headers));
    tpStatistics(1,:)=headers;
    osStatistics=cell(filesNumber,length(headers));
    osStatistics(1,:)=headers;
    offsetStatistics=cell(filesNumber,length(headers));
    offsetStatistics(1,:)=headers;
    
    osVoltageErrorStatistics=cell(filesNumber,length(headers));
    osVoltageErrorStatistics(1,:)=headers;
    figure;
    
    for fileIndex=1:filesNumber
        disp(fileIndex);
        %% Load data
        fileName=matFiles(fileIndex).name;
        currentCampaignWaveForms=loadDataAndConfigureStorage(filePath,fileName);
        
        %% Generate statistics tables
        [currentTpStatistics,currentOsStatistics,currentOffsetStatistics,currentOsVoltageErrorStatistics,tpArray,osArray,offsetArray]=getOscilloscopeCampaignStatistics(currentCampaignWaveForms);
        currentTpStatistics{end+1}=fileName;
        currentOsStatistics{end+1}=fileName;
        currentOffsetStatistics{end+1}=fileName;
        currentOsVoltageErrorStatistics{end+1}=fileName;
        tpStatistics(fileIndex+1,:)=currentTpStatistics;
        osStatistics(fileIndex+1,:)=currentOsStatistics;
        offsetStatistics(fileIndex+1,:)=currentOffsetStatistics; 
        
        osVoltageErrorStatistics(fileIndex+1,:)=currentOsVoltageErrorStatistics; 
        
        %% Plot histograms
        plotAndSaveHistograms(tpArray,osArray,offsetArray);
    end

    mkdir(getCurrentFolderName);
    save(fullfile(pwd,getCurrentFolderName,getCurrentFolderName),'tpStatistics','osStatistics','offsetStatistics','osVoltageErrorStatistics');
    cd(oldFolder);

end
toc

    function waveFormsArray=loadDataAndConfigureStorage(filePath,fileName)
        
        %% Variables definition
        waveFormsArray=[];
        
        %% Get campaign data from file
        
        load(fullfile(filePath,fileName));
        newFolderName=fileName(1:end-4);
        mkdir(fullfile(filePath,newFolderName));
    end

    function plotAndSaveHistograms(peakTimeArray,osArray,vOffsetArray)
        %% Generate peak time histogram
        
        hist(peakTimeArray,22);
        xlabel('Tiempo de pico [microsegundos]'); 
        ylabel('Frecuencia absoluta');
        title('Tiempo de pico vs frecuencia absoluta');
        saveFigure(filePath,fileName,'Tp');
        
        %% Generate overshoot percent histogram
        
        hist(osArray,22);
        xlabel('Porcentaje de sobreimpulso'); 
        ylabel('Frecuencia absoluta');
        title('Porcentaje de sobreimpulso vs frecuencia absoluta');
        saveFigure(filePath,fileName,'Os');
        
        %% Generate offset voltage histogram
        
        hist(vOffsetArray,22);
        xlabel('Tensi�n de offset [milivolts]'); 
        ylabel('Frecuencia absoluta');
        title('Tensi�n de offset de salida vs frecuencia absoluta');
        saveFigure(filePath,fileName,'vOff');
        
    end

    function saveFigure(filePath,fileName,suffix)
        saveFolder=fileName(1:end-4);
        saveas(gcf,fullfile(filePath,saveFolder,strcat(fileName(1:end-4),suffix,'.fig')));
        set(gcf,'units','points','position',[10,10,13660,7680]);
        print(gcf,'-djpeg',fullfile(filePath,saveFolder,strcat(fileName(1:end-4),suffix)));
    end

    function currentFolderName=getCurrentFolderName()
        [~, currentFolderName, ~] = fileparts(pwd);
    end

end