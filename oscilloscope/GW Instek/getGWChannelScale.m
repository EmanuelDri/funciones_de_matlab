function scale=getGWChannelScale(channelNumber,serialPortHandler)
scaleRequestCommand=strcat(':CHAN',num2str(channelNumber),':SCAL?');
    fprintf(serialPortHandler, scaleRequestCommand); scale  = fscanf(serialPortHandler,'%f');
end