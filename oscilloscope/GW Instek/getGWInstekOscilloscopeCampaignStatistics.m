function [tpStatistics,osStatistics,offsetStatistics]=getGWInstekOscilloscopeCampaignStatistics(waveFormsCellArray)
[capturesNumber,~]=size(waveFormsCellArray);
headers={'Promedio','Mediana','Moda','M�nimo','M�ximo','Desviaci�n Est�ndar'};

tpArray=zeros(1,capturesNumber);
osArray=zeros(1,capturesNumber);
offsetArray=zeros(1,capturesNumber);

figure;
hold on;

for captureIndex=1:capturesNumber
    %% On channel 1 the transient responsewas measured , on channel 2 the input stimulus
    
    xChannel1=double(waveFormsCellArray{captureIndex,1});
    yChannel1=double(waveFormsCellArray{captureIndex,2});
    xChannel2=double(waveFormsCellArray{captureIndex,3});
    yChannel2=double(waveFormsCellArray{captureIndex,4});
    
    plot(xChannel1,yChannel1,xChannel2,yChannel2);
    
    [peakTime,overshootPercent,~]=determineOvershootAndPeakTime(xChannel1,yChannel1);
    [transitionLowStateVoltage,~]=getBistateLevels(yChannel1);
    offsetVoltage=transitionLowStateVoltage-1.2;
    [thresholdTime,~]=middleCross(xChannel2,yChannel2);
    peakTime=peakTime-thresholdTime;
    
    tpArray(captureIndex)=peakTime*1e6;
    osArray(captureIndex)=overshootPercent;
    offsetArray(captureIndex)=offsetVoltage;
    
end

tpStatistics=cell(2,length(headers));
osStatistics=cell(2,length(headers));
offsetStatistics=cell(2,length(headers));

tpStatistics(1,:)=headers;
osStatistics(1,:)=headers;
offsetStatistics(1,:)=headers;

tpMean=mean(tpArray); tpStatistics{2,1}=tpMean;
tpMedian=median(tpArray); tpStatistics{2,2}=tpMedian;
tpMode=mode(tpArray); tpStatistics{2,3}=tpMode;
tpMin=min(tpArray); tpStatistics{2,4}=tpMin;
tpMax=max(tpArray); tpStatistics{2,5}=tpMax;
tpSD=std(tpArray); tpStatistics{2,6}=tpSD;

osMean=mean(osArray); osStatistics{2,1}=osMean;
osMedian=median(osArray); osStatistics{2,2}=osMedian;
osMode=mode(osArray); osStatistics{2,3}=osMode;
osMin=min(osArray); osStatistics{2,4}=osMin;
osMax=max(osArray); osStatistics{2,5}=osMax;
osSD=std(osArray); osStatistics{2,6}=osSD;

offsetMean=mean(offsetArray); offsetStatistics{2,1}=offsetMean;
offsetMedian=median(offsetArray); offsetStatistics{2,2}=offsetMedian;
offsetMode=mode(offsetArray); offsetStatistics{2,3}=offsetMode;
offsetMin=min(offsetArray); offsetStatistics{2,4}=offsetMin;
offsetMax=max(offsetArray); offsetStatistics{2,5}=offsetMax;
offsetSD=std(offsetArray); offsetStatistics{2,6}=offsetSD;

figure; hist(tpArray); title('Tiempo de pico [microsegundos]');
figure; hist(osArray); title('Porcentaje de sobreimpulso');
figure; hist(offsetArray); title('Tensi�n de offset de salida [volts]');

end