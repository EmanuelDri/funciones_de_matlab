function [xTimeValues,yVoltageValues]= getGWINSTEKWaveform(channelNumber,varargin)
%DSOPLOT Collect fast scan waveform data from an Instek GDS-1062A 
%
% To get started, see info in openInstek1000Serial.m
%

% Copyright 2011 MathWorks, Inc.

% Check to see that the serial port object is open and ready

if ~((channelNumber==1)||(channelNumber==2))
    error('Requested channel number does not match oscillocope features');
end

if nargin==1
    serialPortHandler=openSerialPort('COM8');
    scale=nan;
else
    serialPortHandler=varargin{1};
    if nargin==3
        scale=varargin{2};
    else
        scale=NaN;
    end
end
% serialPortHandler=openSerialPort('COM10');

%% Check connectivity
serialOK = false; 
if exist('serialPortHandler','var')
    if isa(serialPortHandler,'serial')
        if strcmp(serialPortHandler.Status,'open') && serialPortHandler.InputBufferSize > 8016
            serialOK = true;
        end
    end
end
if ~serialOK
    error('Serial port is not set up, open, or of sufficient input buffer');
end

% First, empty the serial input buffer so we don't read old stuff
b = serialPortHandler.BytesAvailable;
if b > 0
  fread(serialPortHandler,b,'char');
end

% Get count scaling info once (speedier loops this way)
if isnan(scale)
    scaleRequestCommand=strcat(':CHAN',num2str(channelNumber),':SCAL?');
    fprintf(serialPortHandler, scaleRequestCommand); scale  = fscanf(serialPortHandler,'%f');
end
% offsetRequestCommand=strcat(':CHAN',num2str(channelNumber),':OFFS?');
% fprintf(serialPortHandler, offsetRequestCommand); offset = fscanf(serialPortHandler,'%f');
% disp(offset);
% offset=0;


%% Request waveform

    adquisitionCommand=strcat(':ACQ',num2str(channelNumber),':lMEM?');
    %--- Send the acquisition command
    fprintf(serialPortHandler, adquisitionCommand);
    
    %% --- Read the header for the acquired data
    
    % read #4 header start
    h  = char(fread(serialPortHandler,1,'char'));
    f  = char(fread(serialPortHandler,1,'char'));

    % number of remaining bytes
    n  = char(fread(serialPortHandler,4,'char'));
    nn = str2num(char(n'));
    
    % todo:  assert that h is #, f is 4, and nn is 8008 for fast scan
    % read the 4-byte IEEE 754 sample time as big endian bytes
    tb = uint8(fread(serialPortHandler,4,'uint8')); % reverse the endianness
    % convert sampling period to host byte order
    % todo: check host byte order first...
    t = typecast(tb(4:-1:1),'single'); % reverse the byte order
    
    % read the channel byte character
    ch = fread(serialPortHandler,1,'char');
    
    % read the 3 reserved bytes just to get them off the buffer
    r = fread(serialPortHandler,3,'char');
    
    
    %--- Finally, read the waveform data
    
    % Raw integer ADC counts:
    %   signed 16-bit integer but LSB is always 0!
    
    if (channelNumber==1)
        adcBehavior='short';
    else
        adcBehavior='uint16';
    end
    
    oscilloscopeADCRead = fread(serialPortHandler, 4000008, adcBehavior)/256; % really 8-bit values on a GDS-1062A
    
    if (strcmp(adcBehavior,'short'))
        adcTrickedIndex=find(oscilloscopeADCRead<0,1);
%         disp(oscilloscopeADCRead(adcTrickedIndex));
        trickedAddition=zeros(1,4000008);
%         previousValue=oscilloscopeADCRead(adcTrickedIndex-1);
        trickedAddition(adcTrickedIndex+14:end)=255;
%         trickedAddition(adcTrickedIndex)=127;
        oscilloscopeADCRead=oscilloscopeADCRead'+trickedAddition;
        adcTrickedIndex=find(oscilloscopeADCRead<0,1);
        while ~isempty(adcTrickedIndex)
            oscilloscopeADCRead(adcTrickedIndex)=oscilloscopeADCRead(adcTrickedIndex)+255;
            adcTrickedIndex=find(oscilloscopeADCRead<0,1);
        end
    end

    % Convert ADC counts to display volts
    %   is ADCgain the ADC mapping of 10 volts range onto 256 8-bit values
    %   ... or is it really just a magic constant of 1/25?
    ADCgain = 10.0/250;  %todo: why not 256?  data matches for 1/25
%     v = offset + c*scale*ADCgain;
    yVoltageValues = oscilloscopeADCRead*scale*ADCgain;
    % todo: plot data format range or 'eps' size for comparison
    % todo: add offset time and time resolution
    % todo: match scope's colors
    xTimeValues=t*(1:4000008);
%     plot(t*(1:4000008),yVoltageValues); grid
%     title(sprintf('Waveform %02d from GDS-1062A, Vpp = %0.3f Volts', ...
%         k, (max(yVoltageValues)-min(yVoltageValues))));
%     drawnow
    
% fclose(serialPortHandler);

%[EOF]
    function s=openSerialPort(portName)
        delete(instrfind);
%         fclose(instrfind);
        s = serial(portName);
        %         s.BaudRate = 921600;
        s.BaudRate = 153600;
        s.InputBufferSize = 2.1e8; % expect lots of bytes coming in
        fopen(s);
    end
        
end