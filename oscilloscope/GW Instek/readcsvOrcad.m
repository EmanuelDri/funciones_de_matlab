cd(uigetdir);
folders=listFolders(pwd);
if isempty(folders)
    folders={pwd};
end
for folderIndex=1:length(folders)
    folderName=folders{folderIndex};
    ficheros=dir(fullfile(folderName, '*.csv'));
    simulations={};
    for fileIndex=1:length(ficheros)
        fileName=ficheros(fileIndex).name;
        disp(fileName);
        fileAddress=fullfile(folderName,fileName);
        opts=detectImportOptions(fileAddress);
        auxiliarTable=readtable(fileAddress,opts);
        variables=auxiliarTable.Properties.VariableNames;
        simulation=auxiliarTable;
        if contains(upper(variables{2}),'VOUT')
            simulation.(2)=auxiliarTable.(3); simulation.(3)=auxiliarTable.(2);
            simulation.Properties.VariableNames= {'Time';'VStep';'VVout'};
        end
        save([fileAddress(1:end-4) '.mat'],'simulation');
    end
end