function waveForms=gwInstekCampaign(iterations)
tic
waveForms=cell(iterations,4);

fclose(instrfind);
oscilloscopeSerialPort=openInstek1000Serial();
pause on;
channel1Scale=getGWChannelScale(1,oscilloscopeSerialPort);
channel2Scale=getGWChannelScale(2,oscilloscopeSerialPort);

close;figure;hold on;

for execution=1:iterations
    disp(execution);
    pause(2/75*1);
    [channel1TimeValues,channel1VoltageValues]= getGWINSTEKWaveform(1,oscilloscopeSerialPort,channel1Scale);
    [channel2TimeValues,channel2VoltageValues]= getGWINSTEKWaveform(2,oscilloscopeSerialPort,channel2Scale);
    waveForms(execution,:)={channel1TimeValues,channel1VoltageValues,channel2TimeValues,channel2VoltageValues};
    plot(channel1TimeValues,channel1VoltageValues,channel2TimeValues,channel2VoltageValues);       
    
end
pause off;
disp(toc)
end