cd(uigetdir);
ficheros=dir('*.csv');
figure; hold on;
if ~isempty(ficheros)
    for fileIndex=1:length(ficheros)
        fileName=ficheros(fileIndex).name;
        disp(fileName);
%         amplitudEntradaPap=input('tensi�n');
        vadc=csvread(fileName,16,0);
        vadc=vadc(:,1);
        verticalScale=csvread(fileName,5,1,[5,1,5,1]);
        verticalOffset=csvread(fileName,6,1,[6,1,6,1]);
        verticalOffset=0;
        samplingTime=csvread(fileName,11,1,[11,1,11,1]);
        horizontalDelay=csvread(fileName,9,1,[9,1,9,1]);
        %tensiones=10/256*vadc*verticalScale+verticalOffset;
        tensiones=10/255*vadc*verticalScale+verticalOffset;
        Vout=tensiones;
        tiempos=0:samplingTime:samplingTime*length (tensiones)-samplingTime;
        tiempos=tiempos-samplingTime*length(tensiones)/2+horizontalDelay;
        Time=tiempos;
%         Fs=1/samplingTime;
%         coefFourier=fft(tensiones);
%         n=length(coefFourier);
%         amplitudesFourier=abs(coefFourier(1:n/2)/n);
%         amplitudesFourierUnilateral=amplitudesFourier;
%         amplitudesFourierUnilateral(2:end)=2*amplitudesFourierUnilateral(2:end);
%         frecuencias=0:(Fs/n):(Fs/2-Fs/n);
        
%         plot(frecuencias,amplitudesFourierUnilateral);
        
%         save([fileName(1:end-4) '.mat'],'verticalScale','verticalOffset','samplingTime',...
%             'horizontalDelay','tensiones','tiempos',...
%             'amplitudesFourierUnilateral','frecuencias','amplitudEntradaPap');
        save([fileName(1:end-4) '.mat'],'Time','Vout');
    end
end