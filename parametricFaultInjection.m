function results = parametricFaultInjection( c1,c2,c3,c4,ca,cb,fs,parametricSweepPercentage)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

stepOptions = stepDataOptions('StepAmplitude',1.3);
sweepFactorArray=.1:parametricSweepPercentage/100:1.9;
% sweepFactorArray(1)=[];
headers={'Tp';'OS%';'K';'wn';'Qp';'C1';'C2';'C3';'C4';'CA';'CB'};
results=cell(1,length(headers));
results(1,:)=headers;
figure; %figure(2);
%close(2);
for sweepFactor=sweepFactorArray
    parameterFailValue=(sweepFactor*c1)+0;
    [K,qp,wn,os,tp]=generateAndAnalyseTransferFunction(parameterFailValue,c2,c3,c4,ca,cb,fs,stepOptions);
    results(end+1,:)={tp;os;K;wn;qp;parameterFailValue;c2;c3;c4;ca;cb};
end
figure; %figure(2);
%close(2);
for sweepFactor=sweepFactorArray
    parameterFailValue=(sweepFactor*c2)+0;
    [K,qp,wn,os,tp]=generateAndAnalyseTransferFunction(c1,parameterFailValue,c3,c4,ca,cb,fs,stepOptions);
    results(end+1,:)={tp;os;K;wn;qp;c1;parameterFailValue;c3;c4;ca;cb};
end
figure; %figure(2);
%close(2);
for sweepFactor=sweepFactorArray
    parameterFailValue=(sweepFactor*c3)+0;
    [K,qp,wn,os,tp]=generateAndAnalyseTransferFunction(c1,c2,parameterFailValue,c4,ca,cb,fs,stepOptions);
    results(end+1,:)={tp;os;K;wn;qp;c1;c2;parameterFailValue;c4;ca;cb};
end
figure; %figure(2);
%close(2);
for sweepFactor=sweepFactorArray
    parameterFailValue=(sweepFactor*c4)+0;
    [K,qp,wn,os,tp]=generateAndAnalyseTransferFunction(c1,c2,c3,parameterFailValue,ca,cb,fs,stepOptions);
    results(end+1,:)={tp;os;K;wn;qp;c1;c2;c3;parameterFailValue;ca;cb};
end
figure; %figure(2);
%close(2);
for sweepFactor=sweepFactorArray
    parameterFailValue=(sweepFactor*ca)+0;
    [K,qp,wn,os,tp]=generateAndAnalyseTransferFunction(c1,c2,c3,c4,parameterFailValue,cb,fs,stepOptions);
    results(end+1,:)={tp;os;K;wn;qp;c1;c2;c3;c4;parameterFailValue;cb};
end
figure; %figure(2);
%close(2);
for sweepFactor=sweepFactorArray
    parameterFailValue=(sweepFactor*cb)+0;
    [K,qp,wn,os,tp]=generateAndAnalyseTransferFunction(c1,c2,c3,c4,ca,parameterFailValue,fs,stepOptions);
    results(end+1,:)={tp;os;K;wn;qp;c1;c2;c3;c4;ca;parameterFailValue};
end

    function [gain,qp,wn,overshootPercent,meanPeakTime]=generateAndAnalyseTransferFunction(c1,c2,c3,c4,ca,cb,fs,stepOptions)
        gain=[];
        wn=[];
        qp=[];
        transferFunction=scFilterZTF(c1,c2,c3,c4,ca,cb,fs);
        times=-10e-3:1/fs:11.4e-3;
%         stimulus=ones(size(timeValues))*2.5;
%         stimulus(timeValues>-5e-3&timeValues<1.5e-3)=1.2;
%         stimulus(timeValues>1.5e-3)=3.8;
%         timeValues=timeValues+10e-3;
%         voltageValues=lsim(transferFunction,stimulus,timeValues)+2.5;
        [voltageValues,timeValues]=step(transferFunction,1.7e-3,stepOptions);
        voltageValues=voltageValues+2.5;
        voltages=voltageValues;
        [xData, yData] = prepareCurveData( timeValues, voltageValues );
                        % Set up fittype and options.
                        ft = fittype( 'smoothingspline' );
                        opts = fitoptions( 'Method', 'SmoothingSpline' );
                        opts.Normalize = 'on';
                        opts.SmoothingParam =0.9999536367214412; %este era, por cierto
                        [fitModel, ~] = fit( xData, yData, ft, opts );
                        fittedVoltages=feval(fitModel,timeValues);
                        figure(1);hold on;plot(timeValues,fittedVoltages);
                        voltageValues=fittedVoltages;
%         voltageValues(voltageValues>5)=5;
%         voltageValues(voltageValues<0)=0;
        hold on;plot(timeValues,voltageValues);
        %         fittedVoltages=generateFitSpline(timeValues,voltageValues);
        [initialStateVoltage,endStateVoltage]=getBistateLevels(voltageValues,2000,'Histogram mean');
        %         endStateVoltage=3.8;
        %         initialStateVoltage=1.2;
        initialStateVoltage=voltageValues(1);
                endStateVoltage=voltageValues(end);
        overshootVoltage=max(voltageValues);
        voltageRange=overshootVoltage-initialStateVoltage;
        overshootTimeArray=timeValues(voltageValues>=max(voltageValues)-voltageRange*.0);
        overshootPercent=(overshootVoltage-endStateVoltage)/(endStateVoltage-initialStateVoltage)*100;
        if isnan(overshootPercent)||overshootPercent==0
            meanPeakTime=inf;
        else
            meanPeakTime=mean(overshootTimeArray)+1/fs/2;
        end
        if (~isempty(find(voltages>5, 1))||~isempty(find(voltages<0, 1)))
            meanPeakTime='satura';
            overshootPercent='';
        else
            stepVoltage=endStateVoltage-initialStateVoltage;
            gain=stepVoltage/1.3;
            if ~isempty(findpeaks(voltages))
                
                
                tau=1/sqrt((pi/log(overshootPercent/100))^2+1);
                qp=1/2/tau;
                wn=pi/meanPeakTime/sqrt(1-tau^2);
                meanPeakTime=meanPeakTime*1e6;
            else
%                 gain=(voltageValues(end)-voltageValues(1))/2.6;
                qp='No OS';
                wn='No OS';
                overshootPercent=0;
                meanPeakTime=inf;
            end
        end
    end

    function fittedVoltages=generateFitSpline(timeValues,voltageValues)
        [xData, yData] = prepareCurveData( timeValues, voltageValues );
        % Set up fittype and options.
        ft = fittype( 'smoothingspline' );
        opts = fitoptions( 'Method', 'SmoothingSpline' );
        opts.Normalize = 'on';
        opts.SmoothingParam =0.9999536367214412; %este era, por cierto
        [fitModel, ~] = fit( xData, yData, ft, opts );
        fittedVoltages=feval(fitModel,timeValues);
    end

end

