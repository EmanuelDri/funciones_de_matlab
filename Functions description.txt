- calculateOSFromVoltages(vpeak,vfinal,vini) calculates the overshoot percentage from its characteristic voltages
- [getTableColumnsByName.m] obtains the columns of a table which contain a substring passed as an input parameter
- [getOrcadSimTp] calculates the tps of simulated filter responses
- [detectFaultsFromTps.m] this function gathers tps from measurements and compares them in simulations to  determie the fault coverage

general
-boxplotDiffSizeArrays(arrays,labels,varargin) This function creates a boxplot of the vectors contained in 
the cell array <arrays> and assignes them the labels contained in the cell array  
<labels>, also admits Name,Value arguments to boxplot passed in the  varargin input

