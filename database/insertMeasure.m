function insertMeasure(os,tp,strategy,filter,position,description,varargin)
if nargin<7
    connection=connectODBCPostgreSQLDatabase('postgres','a');
else
    connection=varargin{1};
end
collumnNames={'fecha','sobreimpulso','tiempo_pico','ancho_de_pulso',...
    'estrategia','filtro','posicion','descripcion'};
rowData={datestr(now),os,tp,strategy,filter,position,description};
insert(connection,'mediciones_bist',collumnNames,rowData);
close(connection);
end